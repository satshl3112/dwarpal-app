import React from 'react';
import { StyleSheet, Text,View,ScrollView,AsyncStorage,Image,FlatList,TouchableOpacity } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import { Ionicons } from '@expo/vector-icons';

export default class MyFlats extends React.Component {

    constructor(props){
        super(props);
        this.state={ spinner:false,myflats:[],
        }
      }

      async componentDidMount(){
        var userid = await AsyncStorage.getItem('userid');
        const url1='http://agro-vision.in/dpal/Rest/myflats/'+userid;
        fetch(url1).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({myflats:responseJson})
        }).catch((error)=>{ console.log(error)} );
      }

    render() {
      return (
        <ScrollView style={styles.container}>
           <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
          <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View style={{flexDirection:'row',padding:2,backgroundColor:'#D1F2EB',margin:5,borderRadius:5,borderWidth:2,borderColor:'#25be9f'}}>
              <View style={{flex:1}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/myflat.png')}/>
              </View>
              <View style={{flex:5,paddingLeft:10}}>
              <Text style={{fontSize:18,color:'orange'}}>{item.soc_name}</Text>
                <Text style={{fontSize:16,color:'#148F77'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
            </View>}/>
        </ScrollView>
        );
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    input:{width:'100%', borderRadius:2, borderWidth:1, height:40, padding:5,fontSize:16,backgroundColor:'#fff'},
    chooseFlat:{margin:5,backgroundColor:'#E8F8F5',padding:10,borderWidth:1,borderColor:'#25be9f'},
    pickers:{borderWidth:1, borderColor:'grey',backgroundColor:'#fff'},
    btn:{padding:10, backgroundColor:'#25be9f',marginTop:10},
    btnText:{fontSize:18, textAlign:'center', color:'#fff'}
  })