
import React from 'react';
import { StyleSheet, Text,AsyncStorage, TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { Container,Content,Header, Left, Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';


export default class Deposite extends React.Component {
  constructor(props){
    super(props);
    this.state={ damt:'',rect:'',banks:[],
    }
  }

  componentDidMount(){
            const wurlb='http://iglobal.in/home/banks_api';
            fetch(wurlb).then((response)=>response.json())
            .then((responseJson)=>{
              console.log(responseJson)
               this.setState({isLoading: false,banks:responseJson})
            }).catch((error)=>{
              console.log(error)
            })
  }

  async depositSubmit(){
    if(this.state.damt=='' || this.state.rect==''){
      alert("Both Enter Amount and Reference Number.");return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    fetch('http://iglobal.in/home/deposit_submit_api/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                damt:this.state.damt,
                rect:this.state.rect
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              //this.setState({dataSource:responseJson});
              //console.log(responseJson);
              alert(responseJson.msg);
    });
  }

    render() {
      return (
        <Container>
          <Content>
          <Card>
          <Card>
            <CardItem header style={{backgroundColor:'#F5B712'}}><Text style={{fontSize:18,color:'#fff'}}>New Deposit</Text></CardItem>
            <Body style={{padding:5}}>
              <Item regular style={{marginBottom:5}}>
                <Input keyboardType = "number-pad"
    maxLength={8} onChangeText={(damt)=>this.setState({damt:damt})}  placeholder='Enter Amount' />
              </Item>
              <Item regular  style={{marginBottom:5}}>
                <Input  onChangeText={(rect)=>this.setState({rect:rect})}  placeholder='Bank Receipt/Reference Number' />
              </Item>
              <Button title="Submit Request" onPress={()=>this.depositSubmit()}/>
            </Body>
            </Card>
          </Card>
          <FlatList data={this.state.banks}    keyExtractor={(item, index) => index.toString()} 
                renderItem={({item})=>
                <Card>
                    
                <CardItem header bordered style={{backgroundColor:'#2998E5'}}>
                  <Text style={{color:'#fff',fontSize:16}}>{item.bank_name}</Text>
                </CardItem>
                
                <CardItem bordered>
                  <Body>
                    <Text>ACCOUNT HOLDER</Text>
                    <Text style={styles.title}>{item.acc_name}</Text>
                    <Text>ACCOUNT No.</Text>
                    <Text style={styles.title}>{item.acc_number}</Text>
                    <Text>IFSC CODE</Text>
                    <Text style={styles.title}>{item.ifsc_code}</Text>
                    <Text>ACCOUNT TYPE</Text>
                    <Text style={styles.title}>{item.acc_type}</Text>
                  </Body>
                </CardItem>

                </Card>
              }/> 
          
         </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
  
  title:{fontSize:16,color:'green'},btn:{width:'100%', padding:5, backgroundColor:'#1b65d3', height:20},
  btnText:{fontSize:20, textAlign:'center', color:'#fff'}
});
