import React from 'react';
import {StyleSheet,BackHandler,Text,View,Image,AsyncStorage,CheckBox,Share,
  Picker,TouchableOpacity,TextInput,SafeAreaView,FlatList,ActivityIndicator} from 'react-native';
import * as Permissions from 'expo-permissions';
import * as Contacts from 'expo-contacts'
import DatePicker from 'react-native-datepicker';
import { Container,Content, Header, Left, Card,Form,CardItem,Badge,Tabs,Tab, Button} from 'native-base';
import { HeaderBackButton } from 'react-navigation-stack';
import ActionButton from 'react-native-action-button';
import Modal from "react-native-modal";
import AnimatedLoader from "react-native-animated-loader";
import { Ionicons } from '@expo/vector-icons';

export default class UserContacts extends React.Component {
  
  static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Select Guest to Invite',
  })

  constructor() {
    super();
    this.state = {
      isLoading: false,guest_entry:false, contacts: [],guests:[],check: {},start_time:'',start_date:'',end_date:'',validity:'',
      recent_invited:[],mname:'',mphone:'',mentries:[],invite_msg:'',qrmodal:false,gtype:0,invite_code:''
    };
  }

  loadContacts = async () => {
    this.setState({spinner: !this.state.spinner});
    const permission = await Permissions.getAsync(
      Permissions.CONTACTS
    );

    if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(Permissions.CONTACTS);
    }
    
    const { data } = await Contacts.getContactsAsync({
      fields: [Contacts.Fields.PhoneNumbers, Contacts.Fields.Emails],sort:Contacts.SortTypes.FirstName
    });

    //console.log(data);
    this.setState({ contacts: data, inMemoryContacts: data, isLoading: false,spinner:false,});
  };

  handleBackPress = async () => {
      this.props.navigation.goBack()
      return true;
  }

  async componentDidMount() {
    this.setState({gtype:this.props.navigation.getParam('gtype', '')})
    var flatid = await AsyncStorage.getItem('flat');
    BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
    this.setState({ isLoading: true });
    this.loadContacts();
    const url2='http://agro-vision.in/dpal/Rest/recent_invited_api/'+flatid;
              fetch(url2).then((response)=>response.json())
              .then((responseJson)=>{
                this.setState({recent_invited:responseJson})
              }).catch((error)=>{ console.log(error)} );
  }

  renderItem = ({ item }) => (
    <View style={{ minHeight: 70, padding: 5 }}>
      <Text style={{ color: '#bada55', fontWeight: 'bold', fontSize: 26 }}>
        {item.firstName + ' '}
        {item.lastName}
      </Text>
      <Text style={{ color: 'white', fontWeight: 'bold' }}>
      {item.phoneNumbers && item.phoneNumbers[0] && item.phoneNumbers[0].number}
      </Text>
    </View>
  );

  searchContacts = value => {
    const filteredContacts = this.state.inMemoryContacts.filter(contact => {
      let contactLowercase = (
        contact.firstName +
        ' ' +
        contact.lastName
      ).toLowerCase();

      let searchTermLowercase = value.toLowerCase();

      return contactLowercase.indexOf(searchTermLowercase) > -1;
    });
    this.setState({ contacts: filteredContacts });
  };


  checkBox_Test = (id,fname,lname) => {
    var gues=id+' '+'|'+fname+' '+lname;
    if(this.state.guests.includes(gues)){
      var index = this.state.guests.indexOf(gues);
      if (index > -1) {
        this.state.guests.splice(index, 1);
      }
      //alert("hey i am here")
    }else{
      //alert("gone in")
      this.state.guests.push(gues);
    }
    const checkCopy = {...this.state.check}
    if (checkCopy[id]) checkCopy[id] = false;
    else checkCopy[id] = true;
    this.setState({ check: checkCopy });
  }  

  async allowGuestFreq(){
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowGuestFreq/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            society: await AsyncStorage.getItem('society'),block_id: await AsyncStorage.getItem('block'),
            guests: this.state.guests,mentries:this.state.mentries,
            start_date:this.state.start_date,end_date:this.state.end_date,
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({guest_entry:false});
        alert(responseJson.msg);
        this.props.navigation.navigate('Activity')
      });
  }

  invite_manual(){
    if(this.state.mname!="" && this.state.mphone!=""){
    this.setState({spinner: true});
    //var newm=[{"name":this.state.mname,"phone":this.state.mphone}];
    var entt=this.state.mname+'|'+this.state.mname;
      this.state.mentries.push({"name":this.state.mname,"phone":this.state.mphone});
      console.log(this.state.mentries)
      this.setState({mentries:this.state.mentries});
      this.setState({mname:''});
      this.setState({mphone:''});
      this.setState({spinner: false});
    }else{alert("Please Enter Name & Contact")}
  }

  async allowGuestOnce(){
    if(this.state.validity==''){
      alert("Please Select Validity")
    }
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowGuestOnce/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            society: await AsyncStorage.getItem('society'),block_id: await AsyncStorage.getItem('block'),
            validity: this.state.validity,guests: this.state.guests,mentries:this.state.mentries,
            start_date:this.state.start_date,start_time:this.state.start_time,
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({guest_entry:false});
        if(responseJson.status==1){
        alert(responseJson.msg);
      }else{
        this.setState({invite_code:responseJson.invite_code});
        this.setState({invite_msg:responseJson.imsg});
        this.setState({qrmodal:true});
        //this.props.navigation.navigate('Activity');
      }
      });
  }

  onShare = async () => {
    try {
        const result = await Share.share({
        message:this.state.invite_msg
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
        <Container>
        <Content>
        <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
        <Modal style={{padding:10}} isVisible={this.state.qrmodal}>
          <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({qrmodal:false})}} size={26}/>
          <Card style={{backgroundColor:'#fff'}}>
          <Image style={{width:'100%', height:300}} source={{uri: 'http://agro-vision.in/dpal/assets/qrcodes/user_invite_'+this.state.invite_code+'.png'}}/>
          <Text style={{alignSelf:'center',fontSize:20}}>{this.state.invite_code}</Text>
          
          <TouchableOpacity style={{backgroundColor:'#17A589',padding:10,borderRadius:20,alignItems:'center',margin:10}}
           onPress={() => this.onShare()} >
              <Text style={{fontSize:16,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-share" color='#fff' size={16}/> Share Invite Code</Text></TouchableOpacity>
          </Card>
        </Modal>          
           
        <Modal isVisible={this.state.guest_entry}>
           <View style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/guest.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10}}>Allow My Guest Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <TouchableOpacity onPress = {() => {this.setState({guest_entry:false})}}><Ionicons name="md-close" color='#fff' size={22}/></TouchableOpacity> 
              </View>
           </View>
           <View style={{flex:1}}>
                    <Tabs initialPage={this.state.gtype}>
                    <Tab heading="Once" style={{padding:10,height:200}}>
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5}}>Select Date & Time</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      </View>
                      
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Valid for Next</Text>
                      </View>
                      
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
                      <Picker
                      selectedValue={this.state.validity}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ validity: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowGuestOnce()}>
                      <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>                      
                      <Tab heading="Frequent" style={{padding:10,height:200}}>
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5}}>Select Dates</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_date) => {this.setState({end_date: end_date})}}/>
                      </View>
                      </View>
                      
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowGuestFreq()}>
                      <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                    </Tabs>
                  </View>
                <Card>                  
                </Card>
           </Modal>
           
          <Tabs>
            <Tab heading="Phone Contact" textStyle={{color: '#fff'}} tabStyle={{backgroundColor: '#25be9f'}} activeTabStyle={{backgroundColor: '#1e2939'}}>
            <TextInput
          placeholder="Search for a contact"
          placeholderTextColor="#dddddd"
          style={{backgroundColor: '#f3f3f3',height: 40,fontFamily:'custom-fonts',fontSize: 18,padding: 10,borderRadius:20,
          borderWidth:0.5,borderColor:'grey',margin:5}} 
            onChangeText={value => this.searchContacts(value)}/>
            <FlatList data={this.state.contacts} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            
            <Card>
              {item.phoneNumbers && item.phoneNumbers[0] && item.phoneNumbers[0].number &&
              <CardItem  style={{flexDirection:'row'}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/user.png')}/>
              </View>
              <View style={{flex:8,padding:5}}>                
                <Text style={{fontSize:16,color:'#5D6D7E'}}>{item.firstName} {item.lastName}</Text>
                <Text style={{fontSize:16,color:'#76D7C4'}}>
                {item.phoneNumbers && item.phoneNumbers[0] && item.phoneNumbers[0].number}</Text>
              </View>
              
              <CheckBox value = { this.state.check[item.phoneNumbers[0].number]} 
              onChange = {() => this.checkBox_Test(item.phoneNumbers[0].number,item.firstName,item.lastName)}/>
              </CardItem>}
            </Card>}/>
            </Tab>
            <Tab heading="Recent Contact" textStyle={{color: '#fff'}} tabStyle={{backgroundColor: '#25be9f'}} activeTabStyle={{backgroundColor: '#1e2939'}}>
            <FlatList data={this.state.recent_invited} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            
            <Card>
              <CardItem  style={{flexDirection:'row'}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/user.png')}/>
              </View>
              <View style={{flex:8,padding:5}}>                
                <Text style={{fontSize:16,color:'#5D6D7E'}}>{item.gname}</Text>
                <Text style={{fontSize:16,color:'#76D7C4'}}>{item.gphone}</Text>
              </View>
              <CheckBox onChange = {() => this.checkBox_Test(item.gname,item.gphone)}/>
              </CardItem>
            </Card>}/> 
            </Tab>
            <Tab heading="Enter Manual" textStyle={{color: '#fff'}} tabStyle={{backgroundColor: '#25be9f'}} activeTabStyle={{backgroundColor: '#1e2939'}}>
              <Card style={{padding:10}}>
                <TextInput value={this.state.mname} style={styles.input} placeholder="Name" onChangeText={(mname)=>this.setState({mname:mname})}/>
                <TextInput value={this.state.mphone} keyboardType = "number-pad" style={styles.input} placeholder="Phone Number" onChangeText={(mphone)=>this.setState({mphone:mphone})} />
                <TouchableOpacity style={styles.btn} onPress={()=>this.invite_manual()}>
                      <Text style={styles.btnText}>Add</Text></TouchableOpacity>  
              </Card>
              <FlatList data={this.state.mentries} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            
              <Card>
                <CardItem  style={{flexDirection:'row'}}>
                <View style={{flex:2}}>
                <Image style={{width:50, height:50,margin:5}} source={require('.././assets/user.png')}/>
                </View>
                <View style={{flex:8,padding:5}}>                
                  <Text style={{fontSize:16,color:'#5D6D7E'}}>{item.name}</Text>
                  <Text style={{fontSize:16,color:'#5D6D7E'}}>{item.phone}</Text>
                </View>
                </CardItem>
              </Card>}/>
            </Tab>
          </Tabs>
        </Content>
        <ActionButton  buttonColor="rgba(231,76,60,1)" onPress={() =>this.setState({guest_entry:true})} ></ActionButton>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },btn:{width:'100%',marginTop:5, padding:8, backgroundColor:'#25be9f',borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff',},
  btnText2:{fontSize:16, textAlign:'center', color:'#000',},
  input:{borderRadius:20,width:'100%', borderWidth:0.5, height:40, marginBottom:10,padding:10,fontSize:16,backgroundColor:'#fff'},
});