import React from 'react';
import { StyleSheet,StatusBar,AsyncStorage,KeyboardAvoidingView,ScrollView,TextInput,FlatList, Text, View,Image,TouchableOpacity  } from 'react-native';
import { Container,Content, Header,Item,Label,Input, Left, Card,Tab, Tabs , CardItem,Button, Body,Icon, Right, Title } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import Swiper from 'react-native-swiper';

export default class Home extends React.Component {
 
  constructor(props){
    super(props);
     this.state={
       cats:[{'name':'category1'},{'name':'category2'},{'name':'category3'},{'name':'category3'},{'name':'category3'}],
       spinner: false,username:'',password:''
     }
  }

  loginSubmit=()=>{
      if(this.state.username=="" ||this.state.password==""){
          alert("Please Enter Username & Password.");
         // this.setState({errorMsg:'Please Enter Username & Password'});
        }else{
          this.setState({spinner: !this.state.spinner});
          fetch('http://agro-vision.in/dpal/Rest/login_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  username: this.state.username,
                  password: this.state.password,
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({spinner: !this.state.spinner});
            if(responseJson.status==0){
              alert("Hi, "+responseJson.name+responseJson.msg);
              AsyncStorage.setItem("LoggedIn",'1');
              AsyncStorage.setItem("name",responseJson.name);
              AsyncStorage.setItem("userid",responseJson.userid);
              AsyncStorage.setItem("mobile",responseJson.mobile);
              AsyncStorage.setItem("email",responseJson.email);
              console.log(responseJson);
             // AsyncStorage.setItem("userdata",JSON.stringify(responseJson));
              this.props.navigation.navigate("Dashboard");
            }else{
              alert(responseJson.msg);
            }
            });
        }
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <StatusBar backgroundColor="blue" barStyle="dark-content" />
      <ScrollView style={styles.container}>
      <Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
      <View style={{ height:300,marginTop:22 }}>
      <Swiper showsButtons={true}>
        <Image style={{height:300,width:'100%'}} source={require('.././assets/slide1.jpg')}/>
        <Image style={{height:300,width:'100%'}} source={require('.././assets/slide2.jpg')}/>
      </Swiper>
      <Container>
        <Content style={{padding:20}}>
        <Tabs>
            <Tab heading="Login">
              <Item floatingLabel>
                <Label>Mobile</Label>
                <Input />
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input />
              </Item>
            </Tab>
            <Tab heading="Register">
            
            </Tab>
        </Tabs>
        </Content>
      </Container>
      </View>
      <View style={{alignItems:'center'}}>
            <Text style={{fontSize:16,marginBottom:5}}>User Login</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={10} autoCapitalize = 'none' onChangeText={(username)=>this.setState({username})}  style={styles.input} placeholder="Enter Mobile No"/>
            <TextInput onChangeText={(password)=>this.setState({password})}  style={styles.input} placeholder="Enter Password" secureTextEntry/>
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>this.loginSubmit()}><Text style={styles.btnText}>Login</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn2}  
                onPress={()=>this.props.navigation.navigate('Register')}><Text style={styles.btnText}>Register</Text></TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.btn3}  
                onPress={()=>this.props.navigation.navigate('ForgotPassword')}><Text style={styles.btnText}>Forgot Password?</Text></TouchableOpacity>
      </View>
      </ScrollView></KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input:{width:'80%', borderRadius:2, borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
  btn:{width:'48%',borderRadius:2, padding:8, backgroundColor:'#25be9f'},
  btn2:{width:'48%',borderRadius:2, padding:8, backgroundColor:'#1d2839'},
  btn3:{width:'80%',borderRadius:2, padding:8, marginTop:10, backgroundColor:'orange'},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  points:{color:'#6C3483',fontSize:20},
  points2:{color:'#F39C12',fontSize:18,marginBottom:5,borderBottomWidth:0.5,borderBottomColor:'grey'},
  points3:{color:'#34495E',fontSize:16,marginBottom:5,fontWeight:'600'},
})
