import React from 'react';
import { StyleSheet,ScrollView,Text,Image,ActivityIndicator, BackHandler,Alert,
  AsyncStorage,TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { Container,Content,Header, Left, Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';
import { HeaderBackButton } from 'react-navigation-stack';
import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font';

let deviceWidth = Dimensions.get('window').width

export default class AskApproval extends React.Component {
  constructor(props){
    super(props);
    this.state={entry_type:'',entry_msg:'',entry_name:'',entry_phone:'',entry_img:'',type_img:'',assetsLoaded: false,entry_id:'',gid:''
    }
  }

  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#1e2939'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Awaiting Approval',
  })
  
  async componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
          return true;
        });
        await Font.loadAsync({
          'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
        });
        this.setState({ assetsLoaded: true });
       
        var notid = this.props.navigation.getParam('nid');
        console.log(notid)
        
        const url2='http://agro-vision.in/dpal/Rest/ask_approval_api/'+notid;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({entry_type:responseJson.entry_type});
            this.setState({type_img:responseJson.type_img});
            this.setState({entry_msg:responseJson.entry_msg});
            this.setState({entry_name:responseJson.entry_name});
            this.setState({entry_phone:responseJson.entry_phone});
            this.setState({entry_img:responseJson.entry_img});
            this.setState({gid:responseJson.ngid});
            this.setState({entry_id:responseJson.ngid});
        }).catch((error)=>{ console.log(error)});
  }

  async approveEntry(id,st){
        var userid = await AsyncStorage.getItem('userid');
        this.setState({spinner: !this.state.spinner});
        const url6='http://agro-vision.in/dpal/Rest/approveEntry/'+id+'/'+st+'/'+userid+'/'+this.state.gid;
        fetch(url6).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: !this.state.spinner}); 
            this.notifyUser(responseJson.token,responseJson.title,responseJson.msg,responseJson.entid)
            this.props.navigation.navigate("Activity")
        }).catch((error)=>{
          console.log(error)
        });
  }

  notifyUser = async (tkn,title,msg,entid) => {
    
    const message = {
      to: tkn,//this.state.exp_token,
      sound: 'default',
      title: title,
      body:msg,
      data: { type:'ureply',eid:entid},
      android: {
        "channelId": "chat-messages" //and this
      }

    };
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
    const data = response._bodyInit;
    console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
  };

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <Container style={styles.container}>
            <Content>
              <Card style={{marginTop:100}}>
                <CardItem header bordered style={{alignItems:'center',justifyContent:'center'}}>
                <Image style={{width:50,height:50,marginTop:-40,borderRadius:50}} source={{ uri:'http://agro-vision.in/dpal/assets/'+this.state.type_img+'.jpg'}}/>
                
                </CardItem>
                <CardItem style={{alignItems:'center',justifyContent:'center'}}>
                <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>{this.state.entry_type} waiting at the Gate</Text>
                </CardItem>
                <CardItem style={{flexDirection:'row',justifyContent:'space-around',padding:10}}>
                    <Image style={{width:40,height:40,borderRadius:50}} source={{ uri:'http://agro-vision.in/dpal/assets/images/guests/'+this.state.entry_img}}/> 
                    <Text style={{fontSize:20,marginLeft:10,fontFamily:'custom-fonts'}}>{this.state.entry_name}</Text>
                    <Ionicons style={{marginLeft:10}} onPress={()=>{Linking.openURL('tel:'+this.state.entry_phone);}} name="md-call" color='#117A65' size={20}/>
                </CardItem>
                <CardItem style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:20}}>
                    <Ionicons onPress={()=>this.approveEntry(this.state.entry_id,'Denied')} name="md-close-circle-outline" color='orange' size={50}/>
                    <Ionicons onPress={()=>this.approveEntry(this.state.entry_id,'Approved')} name="md-checkmark-circle-outline" color='green' size={50}/>
                </CardItem>
              </Card>
            </Content>
        </Container>
    );}else {
      return (
          <View style={styles.container}>
              <ActivityIndicator />
          </View>
      );
  }
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,backgroundColor: '#1e2939',
        alignItems: 'center',
    },
});
