import React from 'react';
import {StyleSheet,View,ActivityIndicator, AsyncStorage,StatusBar } from 'react-native';


export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
      super(props);
      this._bootstrapAsync();
    }
  
    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
      if(await AsyncStorage.getItem('flat')==true){
        this.props.navigation.navigate('Household');
      }else{
        const userToken = await AsyncStorage.getItem('userid');
        this.props.navigation.navigate(userToken ? 'Dashboard' : 'Login');
      }
    };
  
    // Render any loading content that you like here
    render() {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#1e2939" />
          <StatusBar barStyle="dark" />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,justifyContent:'center',
      backgroundColor: '#fff',
    },
  });