import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,Linking,TextInput,
    RefreshControl,Picker,Button,Alert,FlatList,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { HeaderBackButton } from 'react-navigation-stack';
import Spinner from 'react-native-loading-spinner-overlay';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';


export default class Residents extends React.Component {
  
    constructor(props){
        super(props);
        this.state={ users:[],spinner:false,society:'',allusers:[],blocks:[],block:'',is_secr:null,}
    }

    static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'All Residents',
    })

    searchContacts = value => {
        if(value!=""){
        const filteredContacts = this.state.users.filter(contact => {
          let contactLowercase = (
            contact.name 
          ).toLowerCase();
    
          let searchTermLowercase = value.toLowerCase();
    
          return contactLowercase.indexOf(searchTermLowercase) > -1;
        });
        this.setState({ users: filteredContacts });
        }else{
            this.setState({users:this.state.allusers})
        }
      };

    async componentDidMount(){
      this.setState({is_secr:await AsyncStorage.getItem('is_secr')})
        this.setState({spinner: !this.state.spinner});
        this.setState({society:await AsyncStorage.getItem('society')});
        const url2='http://agro-vision.in/dpal/Rest/residents_api/'+this.state.society;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({users:responseJson});this.setState({allusers:responseJson});
        }).catch((error)=>{ console.log(error)} );
        const url77='http://agro-vision.in/dpal/Rest/blocks_api/'+this.state.society;
                  fetch(url77).then((response)=>response.json())
                  .then((responseJson)=>{
                      this.setState({blocks:responseJson})
                  }).catch((error)=>{ console.log(error)});
    }

    async checkb(bl){
        this.setState({block:bl})
        this.setState({spinner: !this.state.spinner});
        const url2='http://agro-vision.in/dpal/Rest/residents_api/'+this.state.society+'/'+bl;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({users:responseJson});
        }).catch((error)=>{ console.log(error)} );
      }

    render() {
      return (
          <Container style={{padding:5}}>
            <Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <Content>
            <Card>
            <CardItem style={{flexDirection:'row',justifyContent:'space-between'}}>
            <TextInput placeholder="Search User" style={{flex:2,backgroundColor: '#f3f3f3', height: 35,fontSize: 16,
            padding: 5, borderWidth: 0.5,borderColor:'grey'}}
            onChangeText={value => this.searchContacts(value)}/>
                <View style={{borderWidth:0.5, flex:2,marginLeft:10, borderColor:'grey'}}>
                <Picker selectedValue={this.state.block}
                      style={{ width: '100%',height:35 }} onValueChange={(itemValue)=>this.checkb(itemValue)}>
                      <Picker.Item label="Filter Block" value="" />
                      {this.state.blocks.map((facility, i) => {
                        return <Picker.Item key={i} value={facility.block_id} label={facility.block_name} />
                      })}
                </Picker>
                      </View>
              </CardItem>
            
            </Card>
            <FlatList data={this.state.users} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
              <CardItem style={{flexDirection:'row'}}>
                <View style={{flex:1,marginRight:10,}}>
                  <Image style={{width:50,height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
                </View>
                <View style={{flex:4}}>
                  <Text style={{fontSize:18,color:'#17A589'}}>{item.name}</Text>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>{item.block_name} Flat No {item.flat_no}</Text>
                  {item.is_secr!="" &&
                  <Text style={{color:'orange'}}>Secretary</Text>}
                  </View>
                </View>
              </CardItem>{this.state.is_secr!=null &&
              <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                 <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.mobile);}}>
                     <Text style={{fontSize:16,color:'#1E8449'}}>
                        <Ionicons name="md-call" color='#1E8449'size={18}/> Call</Text></TouchableOpacity>
                  <TouchableOpacity>
                    <Text style={{fontSize:16,color:'#EC7063'}}>
                        <Ionicons name="md-trash" color='#EC7063'size={18}/> Remove</Text></TouchableOpacity>
                        <TouchableOpacity>
                    <Text style={{fontSize:16,color:'green'}}>
                        <Ionicons name="md-eye" color='green'size={18}/> View</Text></TouchableOpacity>
              </CardItem>}
            </Card>
          }/>
            </Content></ScrollView>
          </Container>
        );
  }
}

const styles = StyleSheet.create({
  badge:{fontSize:14,color:'#0E6251'}
})
