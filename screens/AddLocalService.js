import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,RefreshControl,KeyboardAvoidingView,
  TextInput,Picker,Button,Alert,FlatList,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import { Container,Content, Header, Left, Card,Textarea, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import { HeaderBackButton } from 'react-navigation-stack';
import MultiSelect from 'react-native-multiple-select';

export default class AddLocalService extends React.Component {
  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Add New Helper'})
  

    constructor(props){
        super(props);
        this.state={name:'',gender:'',phone1:'',age:'',aadhar:'',services:[],address:'',gender:'',
        selectedItems:[],new_user_image:'help.png'}
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
    };

    _pickImage = async () => {
        this.setState({spinner: !this.state.spinner});
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 4],
        });
    
        console.log(result);
    
        if (!result.cancelled) {
          this.setState({ image: result.uri });
          let localUri = result.uri;
          let filename = localUri.split('/').pop();
    
          let match = /\.(\w+)$/.exec(filename);
          let type = match ? `image/${match[1]}` : `image`;
    
          let formData = new FormData();
          formData.append('photo', { uri: localUri, name: filename, type });
    
          return await fetch('http://agro-vision.in/dpal/Rest/upload_imageh/', {
            method: 'POST',
            body: formData,
            header: {
              'content-type': 'multipart/form-data',
            },
          }).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            this.setState({new_user_image:responseJson.img});
          });
        }else{
          this.setState({spinner: !this.state.spinner});
        }
      };

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }
   
    async componentDidMount(){
        this.setState({spinner: !this.state.spinner}); 
        const url2='http://agro-vision.in/dpal/Rest/all_services';
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: !this.state.spinner}); 
            this.setState({services:responseJson})
        }).catch((error)=>{
            console.log(error)
        });
    }

    async add_helper_api(){
      var s = await AsyncStorage.getItem('cityid');
      if(this.state.name=="" || this.state.selectedItems=="" || this.state.phone1=="" || this.state.address==""
      || this.state.gender==""){
        alert("Please Enter Mandatory Details.");return false;}
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      const { navigation } = this.props;
          fetch('http://agro-vision.in/dpal/Rest/add_helper_api', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                name:this.state.name,gender:this.state.gender,zzz:this.state.selectedItems,phone1:this.state.phone1,
                address:this.state.address,new_user_image:this.state.new_user_image,society:await AsyncStorage.getItem('society'),
                city:await AsyncStorage.getItem('city'),
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({spinner: !this.state.spinner});
              console.log(responseJson);
              alert(responseJson.msg);
              this.props.navigation.navigate('Community')
            });
    }

    render() {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <Container>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Content>
                <Card>
                  <CardItem>
                  <View>
                  <MultiSelect items={this.state.services} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Services" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      
                  <TextInput style={styles.input}  
                  onChangeText={(name)=>this.setState({name})} placeholder="Enter Full Name"/>
                  
                  <View style={{flexDirection:'row'}}>
                  <TextInput keyboardType = "number-pad" maxLength={10} style={{width:'50%',borderRadius:20,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16}}  
                  onChangeText={(phone1)=>this.setState({phone1})} placeholder="Primary Mobile No."/>
                  <TextInput keyboardType = "number-pad" maxLength={10} style={{width:'50%',borderRadius:20,marginLeft:5,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16}}  
                  onChangeText={(phone1)=>this.setState({phone1})} placeholder="Alternate No."/>
                  
                  </View>
                  <View style={{flexDirection:'row'}}>
                    <View style={{flex:3}}>
                      <View style={{borderWidth:0.5, borderColor:'grey',borderRadius:20}}>
                      <Picker
                            selectedValue={this.state.gender}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ gender: itemValue})} >
                              <Picker.Item label="Select Gender" value="" />
                              <Picker.Item label="Male" value="Male" />
                              <Picker.Item label="Female" value="Female" />
                      </Picker> 
                      </View>
                      <Textarea value={this.state.address}  onChangeText={(address)=>this.setState({address})}  
                    style={{width:'100%', borderRadius:20,marginBottom:10,marginTop:10,padding:3,borderWidth:0.5}} rowSpan={3} bordered placeholder="Address" />
                    </View>
                    <View style={{flex:2,padding:10,alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this._pickImage()}>
                    <Image style={{width:100,height:100,borderColor:'grey',borderWidth:0.5,borderRadius:50}} 
                    source={{ uri:'http://agro-vision.in/dpal/assets/images/helpers/'+this.state.new_user_image }}/>
                    </TouchableOpacity>
                    </View>
                  </View>
                  <TouchableOpacity style={{backgroundColor:'#17A589',padding:10,borderRadius:20}} onPress={()=>this.add_helper_api()}>
                    <Text style={{color:'#fff',fontSize:16,textAlign:'center'}}>Add Helper</Text>
                  </TouchableOpacity>

                  </View>
                  </CardItem>
                </Card>
              </Content>
            </ScrollView>
          </Container></KeyboardAvoidingView>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',justifyContent:'center'
  },input:{width:'100%',borderRadius:20,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
})
