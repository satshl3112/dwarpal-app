import React from 'react';
import { Linking,StyleSheet,Share,BackHandler,ActivityIndicator,WebView,Text,Alert,ScrollView,RefreshControl,CheckBox,Image, 
  View,Picker,KeyboardAvoidingViewNotifications,TextInput,FlatList,Button,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container,Content, Header, Left, Card,Form,CardItem,List,ListItem, Body,Icon, Right, Title,Fab,Tabs,Tab } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import DatePicker from 'react-native-datepicker';
import AnimatedLoader from "react-native-animated-loader";
import Modal from "react-native-modal";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import ActionButton from 'react-native-action-button';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import MultiSelect from 'react-native-multiple-select';
import * as Font from 'expo-font';
import * as Contacts from 'expo-contacts'

import { NavigationActions } from 'react-navigation';

import { Notifications } from 'expo';

const day1 = new Date();
const day15=new Date(new Date().getTime()+(15*24*60*60*1000));
const day30=new Date(new Date().getTime()+(30*24*60*60*1000));
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = dd + '-' + mm + '-' + yyyy;
var cabtypes=[
  {label:'Once', value:'Once'},
  {label:'Frequent', value:'Frequent'}
]
var items = [{
  id: 'Sunday',
  name: 'Sunday',
}, {
  id: 'Monday',
  name: 'Monday',
}, {
  id: 'Tuesday',
  name: 'Tuesday',
}, {
  id: 'Wednesday',
  name: 'Wednesday',
}, {
  id: 'Thursday',
  name: 'Thursday',
}, {
  id: 'Friday',
  name: 'Friday',
}, {
  id: 'Saturday',
  name: 'Saturday',
}];

export default class Household extends React.Component {
    constructor(props){
      super(props);
      this.state={myfam:[],assetsLoaded: false,new_name:'',gender:'',new_email:'',nofam:true,new_mobile:'',name:'',mobile:'',flat_no:'',myflats:[],
      email:'',userid:'',age_group:'',active:false,helps:[],nohelp:true,isVisible: false,new_user_image:'add_family.jpg',usercode:'',
      isVisible2:false,token: '',error: '',quick_actions:false,quick_actions2:false,cab_tabs:false,cabtype:'Once',cabtime:'',vcomp:'',vnumber:'',
      selectedItems : [],validity:'',start_time:'',end_time:'',delivery_tabs:false,service_list:false,help_type:'',cab1advance:false,
      cab2advance:false,del1advance:false,del2advance:false,ocomp:'',start_date:today,optionname:'Advance',qr_img:'',qrmodal:false,
      vModal:false,noveh:true,vehicles:[],veh_type:'',veh_number:'',vmake:'',saddress:'',is_secr:'',freq_ent:[],noentry:true,
      freq_guest:[],noguest:true,is_admin:'',loc_url:'',uimg:'',view_user:false,edit_mid:'',is_edit:true,
      vucode:'',vuimg:'',vuname:'',vumob:'',vutype:'',vutoken:'',add_fam_title:'Add Member',metype:''}

      
    };


    onShare = async () => {
      try {
          const result = await Share.share({
          message:'My Address is: Flat No '+this.state.flat_no+' '+this.state.saddress+' Google Coordinates : '+this.state.loc_url
        });
  
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    };

    onShare2 = async () => {
      try {
          const result = await Share.share({
          message:this.state.name+ ' has invited you to download the MyGate for : Flat No '+this.state.flat_no+' '+' Get the app here : https://m628y.app.goo.gl/JdE5'
        });
  
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    };
   
    onSelectedItemsChange = selectedItems => {
      this.setState({ selectedItems });
    };
   
    _pickImage = async () => {
      this.setState({spinner: !this.state.spinner});
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 4],
      });
  
     // console.log(result);
  
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
  
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
  
        let formData = new FormData();
        formData.append('photo', { uri: localUri, name: filename, type });
  
        return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
          method: 'POST',
          body: formData,
          header: {
            'content-type': 'multipart/form-data',
          },
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({new_user_image:responseJson.img});
        });
      }else{
        this.setState({spinner: !this.state.spinner});
      }
    };

    async removeVehicle(vid){
      //alert(vid)
      var userid = await AsyncStorage.getItem('userid');
      Alert.alert(
        'Confirm Action',
        'Sure you want to Remove Vehicle?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
              this.setState({spinner: !this.state.spinner});
              const url2='http://agro-vision.in/dpal/Rest/removeVehicle_api/'+vid;
              fetch(url2).then((response)=>response.json())
              .then((responseJson)=>{
                this.setState({spinner: !this.state.spinner});
                alert(responseJson.msg);this.vehRefreshapi();
              }).catch((error)=>{ console.log(error)} );
            }
          },
        ],
        {cancelable: false},
      );
    }
    
    async pageRefresh(){
	  
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      var slat=await AsyncStorage.getItem('slat');
      var slong=await AsyncStorage.getItem('slong');
      var loc_url='http://www.google.com/maps/place/'+slat+','+slong;
	  
	    const url11='http://agro-vision.in/dpal/Rest/myflats/'+userid;
      fetch(url11).then((response)=>response.json())
      .then((responseJson)=>{
        //this.setState({spinner: !this.state.spinner});
        this.setState({myflats:responseJson.result})
      }).catch((error)=>{ console.log(error)} );
      //alert(loc_url)
      this.setState({loc_url:loc_url});
      this.setState({userid:await AsyncStorage.getItem('userid')});
      this.setState({name:await AsyncStorage.getItem('name')});
      this.setState({mobile:await AsyncStorage.getItem('mobile')});
      this.setState({email:await AsyncStorage.getItem('email')});
      this.setState({qr_img:await AsyncStorage.getItem('qr_img')});
      this.setState({usercode:await AsyncStorage.getItem('ucode')});
      this.setState({saddress:await AsyncStorage.getItem('saddress')});
      this.setState({is_secr:await AsyncStorage.getItem('is_secr')});
      this.setState({is_admin:await AsyncStorage.getItem('flatAdmin')});
      this.setState({uimg:await AsyncStorage.getItem('uimg')});
      this.setState({flat_no:await AsyncStorage.getItem('flat_no')+' '+await AsyncStorage.getItem('blockname')+' '+await AsyncStorage.getItem('societyname')});
      //Get Family Members
      const url2='http://agro-vision.in/dpal/Rest/myfamily_api/'+flatid+'/'+userid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        if(responseJson.status==1){
          this.setState({nofam:false})
          this.setState({myfam:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)} );
      //Get Vehicles List
      const urlv='http://agro-vision.in/dpal/Rest/myVehicles_api/'+flatid;
      fetch(urlv).then((response)=>response.json())
      .then((responseJson)=>{
        if(responseJson.status==1){
          this.setState({noveh:false})
          this.setState({vehicles:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)} );
      //Get Daily Helps
      const url1='http://agro-vision.in/dpal/Rest/daily_helper_api/'+flatid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson);
        if(responseJson.status==1){
          this.setState({nohelp:false})
          this.setState({helps:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)});
      //Get Guests Helps
      const url12='http://agro-vision.in/dpal/Rest/frequent_guests_api/'+flatid;
      fetch(url12).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson);
        if(responseJson.status==1){
          this.setState({noguest:false})
          this.setState({freq_guest:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)});
      //Get Daily Helps
      const url112='http://agro-vision.in/dpal/Rest/frequent_entries_api/'+flatid;
      fetch(url112).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson);
        if(responseJson.status==1){
          this.setState({noentry:false})
          this.setState({freq_ent:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)});
    }

    async removeFromFlat(){
      var flatid = await AsyncStorage.getItem('flat');
      Alert.alert(
        'Confirm Action',
        'Sure you want to Remove this user from Flat?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
              this.setState({spinner: !this.state.spinner});
              const urlr='http://agro-vision.in/dpal/Rest/removefromflat_api/'+flatid+'/'+this.state.edit_mid;
              fetch(urlr).then((response)=>response.json())
              .then((responseJson)=>{
                this.setState({spinner: !this.state.spinner});
                this.setState({isVisible: false});
                this.pageRefresh();
              }).catch((error)=>{ console.log(error)} );
                    }
          },
        ],
        {cancelable: false},
      );
      
    }

    async componentDidMount(){
      var flatid = await AsyncStorage.getItem('flat');
      Notifications.addListener(notification => {
        //console.log(notification)
        console.log(notification);
        //this.props.navigation.navigate('Activity');
        if(notification.data.type=='Approval'){
          this.props.navigation.navigate("AskApproval",{nid:notification.data.nid})
        }
        if(notification.data.type=="Flat" && notification.data.type!=flatid){
          alert("You have notification from different Flat. Please Switch Flat.")
          this.props.navigation.navigate("Dashboard");return false;
        }
      });
      const { navigation } = this.props;
      this.focusListener = navigation.addListener('didFocus', () => {
        this.pageRefresh();
      });
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
      });
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      console.log(this.state.assetsLoaded)
      this.pageRefresh();
    }

    componentWillUnmount() {
      // Remove the event listener
      this.focusListener.remove();
    }

    async goGuest(){
      this.setState({quick_actions:false})
      this.props.navigation.navigate("UserContacts")
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    async changeFlat(){
      var userid = await AsyncStorage.getItem('userid');
      this.setState({ isVisible2:!this.state.isVisible2})
      //this.setState({spinner: !this.state.spinner});
      
    }

    gotoSociety(s,b,f,fno,c,blck,so,st,qr,ucode,admin_type,slat,slong){
      //alert(f)
      if(st!="Approved"){
        alert("Your Request is Pending");return false;
      }
      var usercode='#00'+ucode+s;
      AsyncStorage.setItem("ucode",usercode);
      AsyncStorage.setItem("blockname",blck);
      AsyncStorage.setItem("society",s);
      AsyncStorage.setItem("societyname",so);
      AsyncStorage.setItem("block",b);
      AsyncStorage.setItem("flat",f);
      AsyncStorage.setItem("flat_no",fno);
      AsyncStorage.setItem("city",c);
      AsyncStorage.setItem("qr_img",qr);
      AsyncStorage.setItem("slat",slat);
      AsyncStorage.setItem("slong",slong);
      this.setState({ isVisible2:!this.state.isVisible2})
      this.setState({nohelp:true})
      this.setState({nofam:true})
      this.pageRefresh();
    }

    addNewFlat(){
      this.setState({ isVisible2:!this.state.isVisible2})
      this.props.navigation.navigate("AddFlat")      
    }

    selectedAction(type,metype){
      if(type=="cab_tabs"){this.setState({cab_tabs:true});}
      if(type=="delivery_tabs"){this.setState({delivery_tabs:true});this.setState({help_type:'Delivery'});}
      this.setState({metype:metype});
      this.setState({quick_actions:false});
      this.setState({quick_actions2:false});
    }

    open_help(type){
      this.setState({delivery_tabs:true});
      this.setState({service_list:false});
      this.setState({help_type:type});
      this.setState({quick_actions:false});
    }

    async vehRefreshapi(){
      var flatid = await AsyncStorage.getItem('flat');
      const urlv='http://agro-vision.in/dpal/Rest/myVehicles_api/'+flatid;
      fetch(urlv).then((response)=>response.json())
      .then((responseJson)=>{
        if(responseJson.status==1){
          this.setState({noveh:false})
          this.setState({vehicles:responseJson.result})
        }else{this.setState({noveh:true})}
      }).catch((error)=>{ console.log(error)} );
    }

    async addVehicle(){
      if(this.state.veh_type=="" || this.state.veh_number==""  || this.state.vmake==""){
        alert("Please Enter All The Fields.");
      }else{
        var userid = await AsyncStorage.getItem('userid');
        this.setState({spinner: !this.state.spinner});
        fetch('http://agro-vision.in/dpal/Rest/add_vehicle/'+userid, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                veh_type: this.state.veh_type,
                veh_number: this.state.veh_number,
                veh_name: this.state.vmake,
                flat:await AsyncStorage.getItem('flat'),
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            this.setState({vModal: !this.state.vModal});
            alert(responseJson.msg);
            this.vehRefreshapi();
          });
      }
    }

    async addnewMember(){
      if(this.state.new_name=="" || this.state.age_group==""  || this.state.gender=="" || this.state.new_mobile==""){
          alert("Please Enter All The Fields.");
        }else{
          var userid = await AsyncStorage.getItem('userid');
          this.setState({spinner: !this.state.spinner});
          fetch('http://agro-vision.in/dpal/Rest/add_new_member/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  mid:this.state.edit_mid,
                  name: this.state.new_name,gender:this.state.gender,age_group:this.state.age_group,
                  mobile: this.state.new_mobile,
                  email: this.state.new_email,img:this.state.new_user_image,
                  society:await AsyncStorage.getItem('society'),
                  block:await AsyncStorage.getItem('block'),
                  flat:await AsyncStorage.getItem('flat'),
                  city:await AsyncStorage.getItem('city'),
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({spinner: !this.state.spinner});
              if(responseJson.status==1){
                alert(responseJson.msg)
              }else{
                this.setState({ isVisible:!this.state.isVisible})
                this.pageRefresh();
              }
            });
        }
    }

    async allowCabOnce(){
      //if(this.state.cab1advance==true && this.state.vnumber=="" || this.state.vnumber.length<4){alert("Please Enter Last 4 Digit of Vehicle Number");return false;}
      //12if(this.state.cabtime=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowCabOnce/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              cabtime: this.state.cabtime,vnumber: this.state.vnumber,vcomp: this.state.vcomp,
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              flat:await AsyncStorage.getItem('flat'),start_time:this.state.start_date+' '+this.state.start_time,
              ocomp:this.state.ocomp
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false});
          alert(responseJson.msg);
        });
    }
    
    async allowDeliveryOnce(){
      if(this.state.validity=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowDeliveryOnce/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              start_time: this.state.start_date+' '+this.state.start_time,society:await AsyncStorage.getItem('society'),
              block_id:await AsyncStorage.getItem('block'),
              validity: this.state.validity,vcomp: this.state.vcomp,help_type:this.state.help_type
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({delivery_tabs:false});
          alert(responseJson.msg);
        });
    }

    async allowDeliveryFreq(){
      if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){
      alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowDeliveryFreq/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
              vnumber: this.state.vnumber,start_time:this.state.start_time,end_time:this.state.end_time,help_type:this.state.help_type
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({delivery_tabs:false});
          alert(responseJson.msg);
          this.props.navigation.navigate("Activity")
        });
    }

    advanceOptions(){
      this.setState({cab1advance:!this.state.cab1advance});
      if(this.state.optionname=="Advance"){
        this.setState({optionname:"Less"});
      }else{
        this.setState({optionname:"Advance"});
      }
    }

    async allowCabFreq(){
      ///alert(this.state.selectedItems);return false;
      if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){alert("Please Select All Fields.");
      return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowCabFreq/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
              start_time:this.state.start_time,end_time:this.state.end_time,ocomp:this.state.ocomp
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson);
          this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false});
          alert(responseJson.msg);
          this.props.navigation.navigate("Activity")
        });
    }

    addNewOpen(){
      this.setState({ isVisible:!this.state.isVisible})  
      this.setState({new_user_image:'add_family.jpg'});
      this.setState({new_name:''});
      this.setState({new_mobile:''});
      this.setState({gender:''});
      this.setState({new_email:''});
      this.setState({age_group:''});
      this.setState({edit_mid:''});
      this.setState({is_edit:true});this.setState({add_fam_title:'Add Member'});
    }

    viewuser(code1,code2,img,utype,name,mob,vtoken,mid,gender,email,age){
      var uc=code1+''+code2;
      this.setState({vucode:uc});
      this.setState({vutoken:vtoken});
      this.setState({vuimg:img});
      if(utype==''){this.setState({vutype:'Member'});}else{this.setState({vutype:utype});}
      this.setState({vuname:name});
      this.setState({vumob:mob});
      if(this.state.is_secr=='yes' || this.state.is_admin=='Admin'){
        this.setState({isVisible:true});
        this.setState({edit_mid:mid});
        this.setState({is_edit:false});
        this.setState({add_fam_title:'Update Details'});
        this.setState({new_user_image:img});
        this.setState({new_name:name});
        this.setState({new_mobile:mob});
        this.setState({gender:gender});
        this.setState({new_email:email});
        this.setState({age_group:age});
      }else{
        this.setState({view_user:true});
      }
    }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
          <Container>
          <View style={{backgroundColor:'#1e2939',padding:10,flexDirection:'row'}}>
            <Text onPress = {() => this.changeFlat()} style={{color:'#fff',fontSize:16,fontFamily:'custom-fonts'}}>{this.state.flat_no}</Text>
            <Ionicons name="md-arrow-dropdown" color='#fff' style={{marginLeft:20}} size={25}/>
          </View>
          <View  style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
           
          <Modal isVisible={this.state.isVisible} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Add/Edit Family Member</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <TouchableOpacity style={{alignSelf:'center',marginBottom:5}} onPress={()=>this._pickImage()}>
                    <Image style={{width:100,height:100}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.new_user_image }}/>
                  </TouchableOpacity>
                  <TextInput value={this.state.new_name} onChangeText={(new_name)=>this.setState({new_name})} style={styles.input} placeholder="Full Name"/>
                  <View style={{flexDirection:'row',width:'100%',marginBottom:10}}>
                    <View style={{flex:1}}> 
                      <View style={{borderWidth:1, borderColor:'grey',borderRadius:20}}>
                      <Picker enabled={this.state.is_edit}  
                            selectedValue={this.state.gender}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ gender: itemValue})} >
                              <Picker.Item label="Gender" value="" />
                              <Picker.Item label="Male" value="Male" />
                              <Picker.Item label="Female" value="Female" />
                      </Picker> 
                      </View>
                    </View>
                    <View style={{flex:1,marginLeft:5}}>
                    <View style={{borderWidth:1, borderColor:'grey',borderRadius:20}}>
                      <Picker enabled={this.state.is_edit}  
                            selectedValue={this.state.age_group}
                            style={{ width: '100%',height:35}}
                            onValueChange={(itemValue) => this.setState({ age_group: itemValue})} >
                              <Picker.Item label="Age Group" value="" />
                              <Picker.Item label="18-40" value="18-35" />
                              <Picker.Item label="35-50" value="35-50" />
                              <Picker.Item label="above 50" value="above 50" />
                      </Picker> 
                      </View>
                    </View>
                  </View>
                  <TextInput editable={this.state.is_edit}   value={this.state.new_email}  autoCapitalize = 'none' onChangeText={(new_email)=>this.setState({new_email})}  style={styles.input} placeholder="Enter Email"/>
            
              <TextInput editable={this.state.is_edit}   value={this.state.new_mobile}  keyboardType = "number-pad"
      maxLength={10} onChangeText={(new_mobile)=>this.setState({new_mobile})}  style={styles.input} placeholder="Enter Mobile Number"/>
              
                </Body>
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                  <TouchableOpacity style={{backgroundColor:'grey',padding:8,borderRadius:20}} onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                  
                <TouchableOpacity style={{backgroundColor:'#117A65',padding:8,borderRadius:20}} onPress={()=>this.addnewMember()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>{this.state.add_fam_title}</Text></TouchableOpacity>
                {this.state.edit_mid!='' &&
                <TouchableOpacity style={{backgroundColor:'#EC7063',padding:8,borderRadius:20}} onPress={()=>this.removeFromFlat()}>
                <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Remove</Text></TouchableOpacity>}
              </CardItem>
            </Card>
          </Modal>

           <Modal isVisible={this.state.view_user}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({view_user:false})}} size={26}/>
              <Card  style={{backgroundColor:'#fff'}}>
                <CardItem header bordered style={{backgroundColor:'#1e2939'}}>
                <Text style={{color:'#fff',fontSize:18,fontFamily:'custom-fonts'}}>User Details</Text>
                </CardItem>
                <View style={{alignItems:'center',backgroundColor:'#fff',padding:5}}>
                <Image style={{width:100, height:100,borderRadius:200}} 
                source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+this.state.vuimg}}/>
                <Text style={{fontSize:20,fontFamily:'custom-fonts'}}>{this.state.vuname}</Text>
                <Text  style={{fontSize:18,fontFamily:'custom-fonts'}}>{this.state.vumob}</Text>
                <Text  style={{fontSize:16,fontFamily:'custom-fonts'}}>{this.state.vutype} #{this.state.vucode}</Text>
                </View>
                <CardItem style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#1e2939'}}>
                  <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+this.state.vumob);}}>
                    <Text style={{fontSize:16,fontFamily:'custom-fonts',color:'#fff'}}>
                    <Ionicons name="md-call" color='#fff' size={18}/> Call</Text></TouchableOpacity>
                    {this.state.vutoken=="" &&
                    <Text onPress={this.onShare2} style={{fontSize:16,color:'#fff',fontFamily:'custom-fonts'}}>
                      <Ionicons name="md-share" color='#fff' size={18}/> Invite</Text>}
                </CardItem>
              </Card>
           </Modal>
           <Modal style={{padding:10}} isVisible={this.state.qrmodal}>
            <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({qrmodal:false})}} size={26}/>
            <Image style={{width:'100%', height:300}} source={{uri: 'http://agro-vision.in/dpal/assets/qrcodes/'+this.state.qr_img}}/>
           </Modal>          
           
           <Modal isVisible={this.state.isVisible2}>
           <Card>
            <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
              <Text style={{color:'#fff',fontSize:18,fontFamily:'custom-fonts'}}>Select Flats</Text>
            </CardItem>
            <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <TouchableOpacity onPress={()=>this.gotoSociety(item.soc_id,item.block_id,item.flat_id,item.flat_no,item.city_id,item.block_name,
              item.soc_name,item.req_status,item.soc_qrcode,item.rid,item.ouser_type,item.slat,item.slong)} style={item.req_status=="Approved"?{shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#76D7C4',margin:5,borderRadius:5}:
            {shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#F1948A',margin:5,borderRadius:5}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/myflat.png')}/>
              </View>
              <View style={{flex:7,paddingLeft:10}}>
              <Text  style={{fontSize:18,color:'#1e2939',fontFamily:'custom-fonts'}}>{item.soc_name}</Text>
                <Text style={{fontSize:16,color:'#fff',fontFamily:'custom-fonts'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              <View style={{flex:1,alignContent:'center'}}><Ionicons color="#fff" name="md-arrow-dropright" size={55}/></View>
            </TouchableOpacity>}/>
            <CardItem footer bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
            <TouchableOpacity style={{backgroundColor:'grey',padding:10,borderRadius:20}} 
            onPress={()=>{this.setState({ isVisible2:!this.state.isVisible2})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                  
                <TouchableOpacity style={{backgroundColor:'#117A65',padding:10,borderRadius:20}} onPress={()=>this.addNewFlat()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Add New Flat</Text></TouchableOpacity>
             
            </CardItem>
          
          </Card>
           </Modal>          
           
           <Modal isVisible={this.state.cab_tabs}>
           
           <Card>
           <CardItem style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10,fontFamily:'custom-fonts'}}>Allow Cab Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({cab_tabs:false})}} size={22}/>
              </View>
           </CardItem>
             <View style={{padding:10}}>
             <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Days"/>
                          <Picker.Item value="30 Days" label="30 Days"  />
                        </Picker>
                      </View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                      <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                      </View>
                    
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
             </View>
           </Card>
           </Modal>
           <Modal isVisible={this.state.delivery_tabs}>
           <Card>
           <CardItem style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10,fontFamily:'custom-fonts'}}>Allow {this.state.help_type} Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({delivery_tabs:false})}} size={22}/>
              </View>
           </CardItem>
           <View style={{padding:10}}>
           <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Day"/>
                          <Picker.Item value="30 Days" label="30 Day"  />
                        </Picker>
                      </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                    <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                    <DatePicker style={{borderRadius:20}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                    <DatePicker style={{borderRadius:20}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                    </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                    <TextInput  style={styles.input2} placeholder="Company" onChangeText={(vcomp)=>this.setState({vcomp})}/>
                    <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>    
           </View>
           </Card>
          
           </Modal>
           <Modal isVisible={this.state.quick_actions2} style={{borderRadius:50}}>
              <Card>
                <CardItem header bordered style={{backgroundColor:'#1d2839',flexDirection:'row'}}>
                  <Text style={{color:'#fff',fontSize:18,fontFamily:'custom-fonts'}}>Allow Frequent Entry</Text>
                  <View style={{flex:1,alignItems:'flex-end'}}>
                  <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({quick_actions2:false})}} size={22}/>
                  </View>
                </CardItem>
                <CardItem bordered>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.selectedAction('cab_tabs','freq')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Cab</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs','freq')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/delivery.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Delivery</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({ service_list:!this.state.service_list})} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/helper.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Help</Text>
                    </TouchableOpacity>
                  </View>
                </CardItem>

                {this.state.service_list==true &&
                <List>
                    <ListItem onPress={() =>this.open_help('Home Repair','freq')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Home Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Appliances Repair','freq')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Appliances Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Internet Repair','freq')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Internet Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Beautician','freq')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Beautician</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Others','freq')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Others</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                  </List>}
              
                </Card>
           </Modal>
           
           <Modal isVisible={this.state.quick_actions} style={{borderRadius:50}}>
              <Card>
                <CardItem header bordered style={{backgroundColor:'#1d2839',flexDirection:'row'}}>
                  <Text style={{color:'#fff',fontSize:18,fontFamily:'custom-fonts'}}>Allow Future Entry</Text>
                  <View style={{flex:1,alignItems:'flex-end'}}>
                  <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({quick_actions:false})}} size={22}/>
                  </View>
                </CardItem>
                <CardItem bordered>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.selectedAction('cab_tabs','once')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Cab</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs','once')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/delivery.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Delivery</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.goGuest()} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={require('.././assets/guest.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Guest</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({ service_list:!this.state.service_list})} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/helper.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Help</Text>
                    </TouchableOpacity>
                  </View>
                </CardItem>

                {this.state.service_list==true &&
                <List>
                    <ListItem onPress={() =>this.open_help('Home Repair','once')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Home Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Appliances Repair','once')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Appliances Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Internet Repair','once')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Internet Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Beautician','once')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Beautician</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Others','once')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Others</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                  </List>}
              
                <CardItem bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/kid_exit.png')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Kid Exit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:2,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/alert.png')}/>
                      <Text style={{textAlign:'center',fontFamily:'custom-fonts'}}>Security Alert</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:2,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/message.jpg')}/>
                      <Text style={{textAlign:'center',fontFamily:'custom-fonts'}}>Message to Guard</Text>
                    </TouchableOpacity>
                </CardItem>
              </Card>
           </Modal>
           <Modal isVisible={this.state.vModal} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Add Vehicle Details</Text>
              </CardItem>
              <CardItem>
                  <View style={{borderWidth:1,borderColor:'grey',flex:1,borderRadius:20,}}>
                    <Picker
                          selectedValue={this.state.veh_type}
                          style={{ width: '100%',height:40 }}
                          onValueChange={(itemValue) => this.setState({ veh_type: itemValue})} >
                            <Picker.Item label="Select Type" value="" />
                            <Picker.Item label="Two Wheeler" value="Two Wheeler" />
                            <Picker.Item label="Four Wheeler" value="Four Wheeler" />
                    </Picker> 
                  </View>
              </CardItem>
              <CardItem bordered>
                <Body>
                    
                  <TextInput onChangeText={(veh_number)=>this.setState({veh_number})} style={styles.input} placeholder="Vehicle Number"/>
                 
                  <TextInput onChangeText={(vmake)=>this.setState({vmake})}  style={styles.input} placeholder="Make/Model"/>
            
                </Body>
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                  <TouchableOpacity style={{backgroundColor:'grey',padding:10,borderRadius:20}} onPress = {() => { this.setState({ vModal:!this.state.vModal})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:'#117A65',padding:10,borderRadius:20}} onPress={()=>this.addVehicle()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Add Vehicle</Text></TouchableOpacity>
              </CardItem>
            </Card>
          </Modal>
         
         
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
          <Card>
            <CardItem style={{flexDirection:'row'}}>
                <View style={{flex:2}}>
                <Image style={{width:60, height:60,borderRadius:50}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.uimg }}/>
                </View>
                <View style={{flex:6, margin:5}}>
                  <Text style={{fontSize:18,color:'#0E6655',alignItems:'center',fontWeight:'600',fontFamily:'custom-fonts'}}>Hi, {this.state.name}</Text>
                  <Text style={{fontSize:14,color:'#5D6D7E',alignItems:'center',fontFamily:'custom-fonts'}}>{this.state.mobile}</Text>
                </View>
                <TouchableOpacity onPress = {() => { this.setState({ qrmodal:!this.state.qrmodal})}}  style={{flex:2}}>
                <Image style={{width:60, height:60}} source={{uri: 'http://agro-vision.in/dpal/assets/qrcodes/'+this.state.qr_img}}/>
                </TouchableOpacity>
            </CardItem>
            <CardItem style={{flexDirection:'row',backgroundColor:'#F7F9F9',justifyContent:'space-between'}}>
                  <TouchableOpacity onPress={this.onShare}>
                  <Text style={{fontSize:16,color:'#5D6D7E',fontFamily:'custom-fonts'}}><Ionicons name="md-share" size={18} color='#A6ACAF'/> Share Address</Text>
                    </TouchableOpacity>
                  <Text style={{fontSize:16,color:'#59A900',fontFamily:'custom-fonts'}}>{this.state.usercode}</Text>
                  {this.state.is_secr=='yes' &&
                  <Text style={{color:'#FFBE33',fontFamily:'custom-fonts'}}><Ionicons name="md-star" color='#FFBE33' size={16}/> Secretary</Text>}
                  {this.state.is_secr!='yes' && this.state.is_admin=='Admin' &&
                  <Text style={{color:'#FFBE33',fontFamily:'custom-fonts'}}><Ionicons name="md-star" color='#FFBE33' size={16}/> Flat Admin</Text>}
            </CardItem>
          </Card>

          <View style={{flexDirection:'row',padding:5,justifyContent:'space-between'}}>
            <View style={{flex:3}}>
              <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>Flat Members</Text>
            </View>
            {this.state.is_admin=='Admin' && 
            <View style={{flex:1}}>
            <TouchableOpacity style={styles.addbtn} onPress = {() =>  this.addNewOpen()}>
              <Text style={{fontSize:14,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-add" color='#fff' size={14}/> ADD</Text></TouchableOpacity>
            </View>}
          </View>
          {this.state.nofam &&
          <View style={{flexDirection:'row',marginTop:5,padding:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:1,borderRadius:4,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:90, height:90}} source={require('.././assets/addnew.png')}/> 
              {this.state.is_admin=='Admin' &&
              <Text onPress = {() => addNewOpen()} style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts'}}>Add Member</Text>}
            </View>
            <View style={{flex:2}}></View>
          </View>}
          {!this.state.nofam &&
            <View style={{flexDirection:'row',marginTop:5}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.myfam}
            renderItem={({item}) =>
            <Card style={{padding:5,marginRight:5}}>
              <TouchableOpacity onPress={()=>this.viewuser(item.rid,item.socid,item.uimg,item.ouser_type,item.name,item.mobile,item.expo_token,item.uid,item.gender,item.email,item.age_group)} bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flex:1}}>
                <Image style={{width:60, height:60,borderRadius:200}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
                </View>
                <View style={{flex:2,padding:4}}>
                  <Text style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts',textAlign:'center'}}>{item.name}</Text>
                  {item.ouser_type!="" &&
                  <Text style={{fontSize:14,color:'orange'}}>{item.ouser_type}</Text>}{item.ouser_type=="" &&
                  <Text style={{fontSize:12,color:'grey'}}>Member</Text>}
                  <Text style={{fontSize:12,color:'grey'}}>#00{item.rid}{item.socid}</Text>
                </View>
              </TouchableOpacity>
              <CardItem style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.mobile);}}>
                    <Text style={{fontSize:16,fontFamily:'custom-fonts'}}>
                    <Ionicons name="md-call" color='#117A65' size={18}/> Call</Text></TouchableOpacity>
                    {item.expo_token=="" &&
                    <Text onPress={this.onShare2} style={{fontSize:16,fontFamily:'custom-fonts'}}><Ionicons name="md-share" color='grey' size={18}/> Invite</Text>}
              </CardItem>
            </Card>
            }/></View>}

            <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:3}}>
              <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>My Vehicles</Text>
            </View><View style={{flex:1}}>
              <TouchableOpacity style={styles.addbtn} onPress = {() => { this.setState({ vModal:!this.state.vModal})}}>
              <Text style={{fontSize:14,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-add" color='#fff' size={14}/> ADD</Text></TouchableOpacity>
            </View>
          </View>
          {this.state.noveh &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:10,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/car.jpg')}/> 
              <Text onPress = {() => { this.setState({ vModal:!this.state.vModal})}} style={{fontSize:16,color:'#1ABC9C'}}> Add Vehicle</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify',fontFamily:'custom-fonts'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.noveh &&
          <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.vehicles}
            renderItem={({item}) =>
            <Card style={{padding:5,marginRight:5}}>
            <TouchableOpacity bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
              <View style={{flex:1}}>
              <Image style={{width:70, height:70,borderRadius:200}} source={{uri: 'http://agro-vision.in/dpal/assets/'+item.veh_type+'.png'}}/>
              </View>
              <View style={{flex:2,padding:5}}>
              <Text style={{fontSize:14,color:'#1ABC9C',fontFamily:'custom-fonts',textAlign:'center'}}>{item.veh_number}</Text>
              <Text style={{fontSize:14,color:'grey'}}>{item.veh_title}</Text>
              <Ionicons onPress={()=>this.removeVehicle(item.veh_id)} name="md-trash" color='red' size={20}/>
              </View>
            </TouchableOpacity>
          </Card>}/></View>}
          
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:3}}>
              <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>My Staff</Text>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity style={styles.addbtn} onPress={() => this.props.navigation.navigate('ListServices')} >
              <Text style={{fontSize:14,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-add" color='#fff' size={14}/> ADD</Text></TouchableOpacity>
            </View>
          </View>
          {this.state.nohelp &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:10,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/help.png')}/> 
              <Text onPress={() => this.props.navigation.navigate('ListServices')}  style={{fontSize:16,color:'#1ABC9C'}}> Add Daily Help</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify',fontFamily:'custom-fonts'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.nohelp &&
          <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.helps}
            renderItem={({item}) =>
            <Card style={{marginRight:5}}>
              <TouchableOpacity  onPress={() => this.props.navigation.navigate('WorkProfile',{hid:item.hid,htype:item.ser_type,serid:item.serid})} bordered style={{flexDirection:'row',justifyContent:'space-between',padding:5}}>
                <View style={{flex:1}}>
                <Image style={{width:60, height:60,borderRadius:200}} source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
                </View>
                <View style={{flex:2,padding:5}}>
                <Text style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts',textAlign:'center'}}>{item.hname}</Text>
                <Text style={{fontSize:14,color:'grey'}}>{item.ser_type}</Text>
                </View>
              </TouchableOpacity>
              <CardItem style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#F8F9F9'}}>
                  <Ionicons onPress={()=>{Linking.openURL('tel:'+item.hphone);}} name="md-call" color='#117A65' size={20}/>
                  <Ionicons name="md-notifications" color='grey' size={22}/>
                  <Ionicons name="md-star" color='orange' size={22}/>
              </CardItem>
            </Card>}/></View>}
         
         <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:3}}>
              <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>Frequent Guests</Text>
            </View><View style={{flex:1}}>
              
              <TouchableOpacity style={styles.addbtn} onPress={() => this.props.navigation.navigate('UserContacts')} >
              <Text style={{fontSize:14,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-add" color='#fff' size={14}/> ADD</Text></TouchableOpacity>
            </View>
          </View>
          {this.state.noguest &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/addnew.png')}/> 
              <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserContacts")}>
                <Text style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts'}}> Add Entry</Text></TouchableOpacity>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify',fontFamily:'custom-fonts'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.noguest &&
          <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.freq_guest}
            renderItem={({item}) =>
            <Card style={{marginRight:5}}>
              <TouchableOpacity bordered style={{flexDirection:'row',justifyContent:'space-between',padding:5}}>
              <View style={{flex:1}}>
              <Image style={{width:50, height:50,borderRadius:200,borderWidth:0.5,borderColor:'#E5E7E9'}} 
              source={require('.././assets/guest.jpg')}/>
              </View>
              <View style={{flex:2,padding:5}}>
              <Text style={{fontSize:14,color:'#1ABC9C',fontFamily:'custom-fonts',textAlign:'center'}}>{item.gname}</Text>
              <Text style={{fontSize:12,color:'grey'}}>{item.gphone}</Text>
              
              </View>
            </TouchableOpacity>
            <CardItem style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#F8F9F9'}}>
                <Ionicons onPress={()=>{Linking.openURL('tel:'+item.hphone);}} name="md-call" color='#117A65' size={20}/>
                <Ionicons name="md-create" color='orange' size={20}/>
                <Ionicons name="md-trash" color='red' size={20}/>
            </CardItem>
          </Card>}/></View>}

          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:3}}>
              <Text style={{fontSize:18,fontFamily:'custom-fonts'}}>Frequent Entries</Text>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity style={styles.addbtn} onPress={() =>this.setState({quick_actions2:true})}>
              <Text style={{fontSize:14,fontFamily:'custom-fonts',color:'#fff'}}>
              <Ionicons name="md-add" color='#fff' size={14}/> ADD</Text></TouchableOpacity>
            </View>
          </View>
          {this.state.noentry &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/calendar.png')}/> 
              <TouchableOpacity onPress={() =>this.setState({quick_actions2:true})}>
                <Text style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts'}}> Add Entry</Text></TouchableOpacity>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify',fontFamily:'custom-fonts'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.noentry &&
          <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.freq_ent}
            renderItem={({item}) =>
            <Card style={{padding:5,marginRight:5}}>
            <TouchableOpacity bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
              <View style={{flex:1}}>
              <Image style={{width:70, height:70,borderRadius:200,borderWidth:0.5,borderColor:'#E5E7E9'}}
                source={{uri: 'http://agro-vision.in/dpal/assets/'+item.entry_img}}/>
              </View>
              <View style={{flex:2,padding:5}}>
              <Text style={{fontSize:16,color:'#1ABC9C',fontFamily:'custom-fonts',textAlign:'center'}}>{item.entry_type}</Text>
              <Text style={{fontSize:14,color:'grey',fontFamily:'custom-fonts'}}>{item.start_time}-{item.end_time}</Text>
              <View style={{flexDirection:'row',borderTopColor:'grey',borderTopWidth:0.5,margin:5,padding:5,justifyContent:'space-between'}}>
                <Ionicons name="md-create" color='orange' size={20}/>
                <Ionicons name="md-list" color='green' size={20}/>
                <Ionicons name="md-trash" color='red' size={20}/>
              </View>
              </View>
            </TouchableOpacity>
          </Card>}/></View>}
        </ScrollView>
        {/* <ActionButton style={{alignItems:'center'}}  buttonColor="rgba(231,76,60,1)" onPress={() =>this.setState({quick_actions:true})}></ActionButton> */}
        </View></Container>
      );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
  }
}

const styles = StyleSheet.create({
  cfont:{fontFamily: 'roboto-medium'},
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:20},
  addbtn:{backgroundColor:'#17A589',padding:5,borderRadius:20,alignItems:'center'},
  btnText:{fontSize:18, textAlign:'center', color:'#fff',fontFamily:'custom-fonts'},btnText2:{fontSize:16, textAlign:'center', color:'#000'},
  container: {
    flex: 1,padding:5,
  },input:{width:'100%',borderRadius:20, borderWidth:0.5,fontFamily:'custom-fonts', height:40, marginBottom:10,padding:10,fontSize:16,backgroundColor:'#fff'},
  input2:{borderRadius:20,width:'100%', borderWidth:0.5,fontFamily:'custom-fonts', height:40,padding:10,fontSize:16,backgroundColor:'#fff'},
});
