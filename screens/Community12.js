import React from 'react';
import { Text, View, Button,Image,StyleSheet,AsyncStorage,TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container,Content, Header, Left, Card,Form,CardItem,Body} from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";

export default class Community extends React.Component {

  constructor(props){
    super(props);
    this.state={pending_apprs:0,is_secr:'',pending_fapprs:0}
  }
  
  async pageRefresh(){
      
      var userid = await AsyncStorage.getItem('userid');
      const url5='http://agro-vision.in/dpal/Rest/total_pending_req/'+userid;
      fetch(url5).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({pending_apprs:responseJson.reqs})
      }).catch((error)=>{ console.log(error)});
      var flat = await AsyncStorage.getItem('flat');
      const url6='http://agro-vision.in/dpal/Rest/total_pending_req2/'+flat;
      fetch(url6).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({pending_fapprs:responseJson.reqs})
      }).catch((error)=>{ console.log(error)});
  }

  async componentDidMount(){
    const message = {
      to: 'ExponentPushToken[WLeglSKZ3YaeIHR6FFotQe]',
      sound: 'default',
      title: 'Original Title',
      body: 'And here is the body!',
      data: { data: 'goes here note' },
    };
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });

    console.log(await AsyncStorage.getItem('secr'))
    this.setState({is_secr:await AsyncStorage.getItem('is_secr')})
    this.setState({flatAdmin:await AsyncStorage.getItem('flatAdmin')})
    console.log(this.state.is_secr)
    this.pageRefresh();
  }

  render() {
    return (
      <Container style={{margin:5}}>
        <Content>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        {this.state.is_secr!=null &&
        <Card style={{flex:1}}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Approvals',{type:'Society'})}  style={{flexDirection:'row',justifyContent:'space-between',padding:5,backgroundColor:'#F2F3F4'}}>
            <View style={{flexDirection:'row'}}>
            <Image style={{width:20, height:20,margin:2}} source={require('.././assets/myflat.png')}/>
            <Text style={{fontSize:14,margin:2}}>{this.state.pending_apprs} Society Requests</Text>
            </View>
            <Ionicons color="#AEB6BF" name="md-arrow-dropright" style={{padding:2}} size={20}/>
          </TouchableOpacity>
        </Card>}
        {this.state.flatAdmin!=null &&
        <Card style={{flex:1}}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Approvals',{type:'Flat'})}  style={{flexDirection:'row',justifyContent:'space-between',padding:5,backgroundColor:'#F2F3F4'}}>
            <View style={{flexDirection:'row'}}>
            <Image style={{width:20, height:20,margin:2}} source={require('.././assets/new_helper.png')}/>
            <Text style={{fontSize:14,margin:2}}>{this.state.pending_fapprs} Flat Requests</Text>
            </View>
            <Ionicons color="#AEB6BF" name="md-arrow-dropright" style={{padding:2}} size={20}/>
          </TouchableOpacity>
        </Card>}
        </View>
        <Grid>
            {this.state.is_secr!=null &&
            <Col>
              <Card>
              <CardItem style={{justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                <TouchableOpacity  onPress={() => this.props.navigation.navigate('AddLocalService')}>
                <Image style={styles.image_style} source={require('.././assets/new_helper.png')}/>
                <Text style={styles.title}>Add Local Service</Text>
                </TouchableOpacity>
              </CardItem>
              </Card>
            </Col>} 
            {this.state.is_secr!=null &&
            <Col>
              <Card>
              <CardItem style={{justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                <TouchableOpacity  onPress={() => this.props.navigation.navigate('HelperApprovals')}>
                <Image style={styles.image_style} source={require('.././assets/approve_helper.jpg')}/>
                <Text style={styles.title}>Approve Flat Helper</Text>
                </TouchableOpacity>
              </CardItem>
              </Card>
            </Col>}
        </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image_style:{
    width:100,height:100,borderRadius:50,alignSelf:'center',borderWidth:0.5,borderColor:'#E5E8E8',
  },title:{color:'grey',fontSize:14,marginTop:10,textAlign:'center'}
});
