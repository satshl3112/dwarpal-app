import React from 'react';
import { StyleSheet,AsyncStorage,StatusBar,View,Text,TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class UNIcon extends React.Component {
 
  constructor(props){
    super(props);
     this.state={totnot:0}
  }

  async componentDidMount(){
    var flatid = await AsyncStorage.getItem('flat');
    //console.log('flat '+ flatid);
    const url11='http://agro-vision.in/dpal/Rest/user_notif_api/'+flatid;
      fetch(url11).then((response)=>response.json())
      .then((responseJson)=>{
          console.log(responseJson)
        this.setState({totnot:responseJson.note})
    }).catch((error)=>{ console.log(error)} );
  }

  render() {
    return (
      <View>
          <Text style={{color:'orange',marginRight:10}}> {this.state.totnot}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1e2939',
  },
});
