import React from 'react';
import {StatusBar,StyleSheet,BackHandler,TouchableHighlight,ActivityIndicator,
  RefreshControl,Text,View,ScrollView,KeyboardAvoidingView,AsyncStorage,Image,Picker,FlatList,TouchableOpacity,TextInput } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import { Ionicons } from '@expo/vector-icons';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Container,Header, Content,Left,Right,Body,CardItem,Card,Title,ListItem,List} from 'native-base';
import * as Font from 'expo-font';
import { Notifications } from 'expo';
var owner_types=[
  {label:'Owner', value:'Owner'},
  {label:'Tenant', value:'Tenant'},
  {label:'Shared Tenant', value:'Shared Tenant'},
  {label:'Builder', value:'Builder'}
]

export default class Dashboard extends React.Component {

  constructor(props){
    super(props);
     this.state={ assetsLoaded:false,cities:[],dataSource:[],cars:[],selected_city:'',societies:[],blocks:[],myblock:'',flats:[],myflat:'',myflats:[],
     society:'',start_date:'',qr_img:'',end_date:'',spinner: false,owner_type:'Owner',search_s:'',selected_soc_name:'',name:'',
     mobile:'',email:'',uimg:'user.png',noflats:false
     }
  }

  async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      const url1='http://agro-vision.in/dpal/Rest/myflats/'+userid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        console.log(responseJson.result)
        if(responseJson.status==1){
          this.setState({myflats:responseJson.result});
          this.setState({noflats:false});
        }else{this.setState({noflats:true});}
      }).catch((error)=>{ console.log(error)} );
  }

  _onRefresh(){
    this.setState({refreshing:true})
    this.pageRefresh().then(()=>{
      this.setState({refreshing:false})
    })
  }

  async componentDidMount(){
      Notifications.addListener(notification => {
          this.pageRefresh();
      });
      //console.log(await AsyncStorage.getItem('uimg'))
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
      });
      const { navigation } = this.props;
      this.focusListener = navigation.addListener('didFocus', () => {
        this.pageRefresh();
      });
      console.log(await AsyncStorage.getItem('flat'));
      var userid = await AsyncStorage.getItem('userid');
      this.setState({name:await AsyncStorage.getItem('name')});
      this.setState({qr_img:await AsyncStorage.getItem('qr_img')});
      this.setState({mobile:await AsyncStorage.getItem('mobile')});
      this.setState({email:await AsyncStorage.getItem('email')});
      this.setState({uimg:await AsyncStorage.getItem('uimg')});
      //this.setState({spinner: !this.state.spinner});  
      
      this.pageRefresh();
  }

  async addMyFlat(){
    if(this.state.selected_city=="" ||  this.state.society=="" ||  this.state.myblock=="" ||  this.state.myflat==""){
      alert("Please Select All the Fields");return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    this.setState({spinner: !this.state.spinner});  
    fetch('http://agro-vision.in/dpal/Rest/addflat_api/'+userid, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            city: this.state.selected_city,
            society: this.state.society,
            block:this.state.myblock,
            flat:this.state.myflat,
            owner:this.state.owner_type
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});  
        //console.log(responseJson);
        alert(responseJson.msg);
        //this.pageRefresh();
        this.setState({selected_city:''})
        this.setState({society:''})
      });
  }

  async logout(){
    await AsyncStorage.clear();
    alert("You are logout successfully.")
    this.props.navigation.navigate("Login");
  }

  getSociety(){
    if(this.state.search_s.length>3){
    fetch('http://agro-vision.in/dpal/Rest/society_api/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            city: this.state.selected_city,
            soc: this.state.search_s,
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        //this.setState({myblock:''})
        this.setState({societies:responseJson})
      });
    }
  }

  getBlocks(socid,socname){
    this.setState({spinner: !this.state.spinner})
    this.setState({ search_s: socname})  
    this.setState({society:socid})
    //if(socid==""){return false;}
    const url5='http://agro-vision.in/dpal/Rest/blocks_api/'+socid;
        fetch(url5).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({spinner: !this.state.spinner});  
           this.setState({blocks:responseJson})
           this.setState({myblock:'Select Block'})
           //this.setState({societies:''})
           //console.log(responseJson)
        }).catch((error)=>{
          console.log(error)
        });
  }
  
  gotoSociety(s,b,f,fno,c,blck,so,st,ot,qr,ucode,saddress,is_secr,admin_type,slat,slong){
    //alert(is_secr)
    if(st!="Approved"){
      alert("Your Request is Pending");return false;
    }
    //alert(slat+','+slong);return false;
    var usercode='#00'+ucode+s;
    AsyncStorage.setItem("ucode",usercode);
    AsyncStorage.setItem("blockname",blck);
    AsyncStorage.setItem("society",s);
    AsyncStorage.setItem("societyname",so);
    AsyncStorage.setItem("block",b);
    AsyncStorage.setItem("flat",f);
    AsyncStorage.setItem("flat_no",fno);
    AsyncStorage.setItem("city",c);
    AsyncStorage.setItem("owner_type",ot);
    AsyncStorage.setItem("qr_img",qr);
    AsyncStorage.setItem("saddress",saddress);
    AsyncStorage.setItem("is_secr",is_secr);
    AsyncStorage.setItem("flatAdmin",admin_type);
    AsyncStorage.setItem("slat",slat);
    AsyncStorage.setItem("slong",slong);
    this.props.navigation.navigate("Household");
  }

  getFlats(){ 
    if(this.state.myblock!=""){
    //this.setState({spinner: !this.state.spinner}); 
    const url6='http://agro-vision.in/dpal/Rest/flats_api/'+this.state.myblock;
        fetch(url6).then((response)=>response.json())
        .then((responseJson)=>{
           //this.setState({spinner: !this.state.spinner}); 
           this.setState({flats:responseJson})
        }).catch((error)=>{
          console.log(error)
        });
      }
  }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <KeyboardAvoidingView behavior="padding" enabled>
        <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
        <Container>
        <Content>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <StatusBar backgroundColor="#1d2839" barStyle="light-content" />
            <View style={{shadowColor: '#1d2839',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,
    shadowRadius: 2,elevation: 5,flexDirection:'row',padding:5,margin:5,borderRadius:5,backgroundColor:'#fff'}}>
            <View style={{flex:2}}>
              <Image style={{width:70,height:70,margin:2}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.uimg }}/>
            </View>
            <View style={{flex:8,marginLeft:5,padding:5}}>
              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={{fontSize:18,color:'#FFA200',fontFamily:'custom-fonts'}}>Hi, {this.state.name}</Text>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserProfile")}>
                <Ionicons style={{color:'red'}} name="md-create" size={18}/></TouchableOpacity>
              </View>
              <Text style={{fontSize:14,color:'#7F8C8D'}}><Ionicons style={{marginRight:10,color:'#148F77',fontFamily:'custom-fonts'}} name="md-call" size={14}/>  {this.state.mobile}</Text>
              <Text style={{fontSize:14,color:'#7F8C8D'}}><Ionicons style={{marginRight:10,color:'#148F77',fontFamily:'custom-fonts'}} name="md-at" size={14}/> {this.state.email}</Text>
            </View>
            
          </View>
          {this.state.selected_city=='' &&
          <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <TouchableOpacity  onPress={()=>this.gotoSociety(item.socid,item.block_id,item.flat_id,item.flat_no,
              item.city_id,item.block_name,item.soc_name,item.req_status,item.owner_type,item.soc_qrcode,item.rid,item.soc_address,item.secratary,item.ouser_type,item.slat,item.slong)} style={item.req_status=="Approved"?{shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#76D7C4',margin:5,borderRadius:5}:
            {shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#F1948A',margin:5,borderRadius:5}}>
              <View style={{flex:3}}>
              <Image style={{width:60, height:60,margin:5}} source={{ uri:'http://agro-vision.in/dpal/assets/images/helpers/'+item.soc_img}}/>
              </View>
              <View style={{flex:7,padding:5}}>
              <Text style={{fontSize:18,color:'#117A65',fontFamily:'custom-fonts'}}>{item.soc_name}-{item.soc_code}</Text>
                <Text style={{fontSize:16,color:'#fff',fontFamily:'custom-fonts'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              <View style={{flex:1,alignContent:'center',marginTop:15}}>
                  <Text style={{color:'grey',fontSize:22,fontFamily:'custom-fonts'}}> {item.notes}</Text></View>
              <View style={{flex:1,alignContent:'center',marginTop:10}}>
                  <Ionicons color="#E5E7E9" name="md-arrow-dropright" size={40}/></View>
              
            </TouchableOpacity>}/>}
          {this.state.noflats==true && <View><Text style={{color:'red',textAlign:'center',fontSize:20,margin:20}}>No Flats Added</Text></View>}
         <List style={{backgroundColor:'#F2F3F4',marginTop:50}}>
            <ListItem  onPress={()=>this.props.navigation.navigate("AddFlat")} style={{flexDirection:'row',justifyContent:'space-between'}}>
             <Text style={{fontSize:16,fontFamily:'custom-fonts'}}><Ionicons name="md-home" color='grey' size={16}/> Add Your Flat</Text>
              <Ionicons name="md-arrow-dropright" color='grey' size={22}/>
            </ListItem>
            <ListItem style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={{fontSize:16,fontFamily:'custom-fonts'}}><Ionicons name="md-chatboxes" color='grey' size={16}/> Support & Feedback</Text>
              <Ionicons name="md-arrow-dropright" color='grey' size={22}/>
            </ListItem>
            <ListItem  onPress={()=>this.logout()} style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Text style={{fontSize:16,fontFamily:'custom-fonts'}}><Ionicons name="md-log-out" color='grey' size={16}/> Logout</Text>
              <Ionicons name="md-arrow-dropright" color='grey' size={22}/>
            </ListItem>
          </List>
          </Content>
        </Container>
          </ScrollView>
                  
          </KeyboardAvoidingView>
        );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
      }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input:{width:'100%',borderRadius:5,borderColor:'grey', borderWidth:0.5, height:40,marginTop:2,padding:10,fontSize:16,
  backgroundColor:'#fff',borderRadius:20},
  chooseFlat:{shadowColor: '#000',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,
  shadowRadius: 2,elevation: 5,borderRadius:5,margin:5,padding:10,backgroundColor:'#fff'},
  pickers:{marginTop:2,borderWidth:0.5, borderColor:'grey',backgroundColor:'#fff',borderRadius:20},
  btn:{padding:10, backgroundColor:'#148F77',marginTop:10,borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'}
})

 