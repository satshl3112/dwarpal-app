import React from 'react';
import { StyleSheet,StatusBar,AsyncStorage,KeyboardAvoidingView,ScrollView,Button,TextInput,FlatList, Text, View,Image,TouchableOpacity  } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import Spinner from 'react-native-loading-spinner-overlay';

export default class ForgotPassword extends React.Component {

    static navigationOptions =  ({ navigation }) => ({
        headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle: 'Forgot Password',
      })
 
  constructor(props){
    super(props);
     this.state={
        username:'',spinner: false
     }
  }

  forgotSubmit=()=>{
      if(this.state.username=="" || this.state.username.length!=10){
          alert("Please Enter Valid and Registered 10 Digit Mobile Number.");
         // this.setState({errorMsg:'Please Enter Username & Password'});
        }else{
          this.setState({spinner: !this.state.spinner});
          fetch('http://iglobal.in/home/forgot_password_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                mobile: this.state.username,
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            if(responseJson.status==0){console.log(responseJson);
              alert(responseJson.msg);
              //AsyncStorage.setItem("forgotmob",responseJson.mob);
              //AsyncStorage.setItem("userdata",JSON.stringify(responseJson));
              this.props.navigation.navigate("VerifyForgotOTP",{mobile:responseJson.mob,votp:responseJson.otp});
            }else{
              alert(responseJson.msg);
            }
            });
        }
  }


  

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <Spinner size='large' visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
            <Image style={{width:100, height:100,margin:10}} source={require('.././assets/logo.png')}/>
            <Text style={{fontSize:16,marginBottom:5}}>Enter Registered Mobile Number</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={10} autoCapitalize = 'none' onChangeText={(username)=>this.setState({username})}  style={styles.input} placeholder="Enter Mobile No"/>
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>this.forgotSubmit()}><Text style={styles.btnText}>Request OTP</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn2}  
                onPress={()=>this.props.navigation.navigate('Login')}><Text style={styles.btnText}>Back to Login</Text></TouchableOpacity>
              
            </View></KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent:'center'
  },
  input:{width:'80%',borderRadius:20, borderWidth:0.5, height:40, marginBottom:10,padding:10,fontSize:16},
  btn:{width:'45%', padding:10, backgroundColor:'#25be9f',borderRadius:20},
  btn2:{width:'45%', padding:10, backgroundColor:'#1d2839',borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  points:{color:'#6C3483',fontSize:20},
  points2:{color:'#F39C12',fontSize:18,marginBottom:5,borderBottomWidth:0.5,borderBottomColor:'grey'},
  points3:{color:'#34495E',fontSize:16,marginBottom:5,fontWeight:'600'},
})
