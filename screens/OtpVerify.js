import React from 'react';
import { StyleSheet,KeyboardAvoidingView,AsyncStorage, Text,Image, View, TextInput ,TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

export default class OtpVerify extends React.Component {
    static navigationOptions = {
        header: null
    }

    constructor(props){
        super(props);
        this.state={
            otp:'',spinner: false
        }
    }

    otpSubmit=()=>{
        if(this.state.otp=="" || this.state.otp.length<6){
            alert("Please Enter Valid 6 Digit OTP.");
           // this.setState({errorMsg:'Please Enter Username & Password'});
          }else{
           // this.setState({spinner: true});
            fetch('http://agro-vision.in/dpal/Rest/verify_otp_api/', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    otp: this.state.otp,
                }),
              }).then((response)=>response.json())
              .then((responseJson)=>{
                ///this.setState({spinner: !this.state.spinner});
              if(responseJson.status==0){
                alert("Hi, "+responseJson.name+responseJson.msg);
                AsyncStorage.setItem("LoggedIn",'1');
                AsyncStorage.setItem("name",responseJson.name);
                AsyncStorage.setItem("userid",responseJson.userid);
                AsyncStorage.setItem("mobile",responseJson.mobile);
                AsyncStorage.setItem("email",responseJson.email);
                AsyncStorage.setItem("qr_img",responseJson.qr_img);
                AsyncStorage.setItem("uimg",responseJson.uimg);
               // AsyncStorage.setItem("userdata",JSON.stringify(responseJson));
                this.props.navigation.navigate("Dashboard");
              }else{
                alert(responseJson.msg);
              }
              });
          }
    }


  render() {
    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'} size="large"
          textStyle={styles.spinnerTextStyle}
        />
            <Image style={{width:100, height:150,marginBottom:10}} source={require('.././assets/otp.png')}/>
            <Text style={{fontSize:16,marginBottom:10}}>Verify OTP sent to your Mobile/Email</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={6} onChangeText={(otp)=>this.setState({otp})}  style={styles.input} placeholder="Enter 6 Digit OTP"/>
            
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity  style={styles.btn2}><Text style={styles.btnText}>Resend OTP</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>this.otpSubmit()}><Text style={styles.btnText}>Verify OTP</Text></TouchableOpacity>
                
            </View>
            <View>
               <Text  onPress={()=>this.props.navigation.navigate('Login')} style={{fontSize:16, textAlign:'center', 
                  color:'#EC7063',marginTop:15}}>Back to Login?</Text> 
            </View>
        </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input:{width:'80%', borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,borderRadius:20,},
    btn:{width:'45%', padding:10, backgroundColor:'#25be9f',borderRadius:20,},
    btn2:{width:'45%', padding:10, backgroundColor:'#1d2839',borderRadius:20,},
    btnText:{fontSize:18, textAlign:'center', color:'#fff'}
  });