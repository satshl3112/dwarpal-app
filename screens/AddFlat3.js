import React from 'react';
import {StatusBar,StyleSheet,BackHandler,TouchableHighlight,RefreshControl,Text,View,ScrollView,KeyboardAvoidingView,AsyncStorage,Image,Picker,FlatList,TouchableOpacity,TextInput } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import { Ionicons } from '@expo/vector-icons';
import { HeaderBackButton } from 'react-navigation-stack';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Container,Header, Content,Left,Right,Body,CardItem,Card,Title,ListItem,List} from 'native-base';

var owner_types=[
  {label:'Owner', value:'Owner'},
  {label:'Family Member', value:'Family Member'},
  {label:'Tenant', value:'Tenant'},
  {label:'Shared Tenant', value:'Shared Tenant'}
]

export default class AddFlat extends React.Component {

    static navigationOptions =  ({ navigation }) => ({
        headerTitleStyle: {color:'white'},
        headerStyle: {backgroundColor:'#25be9f'},
        headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,
        headerRight:'', headerTitle:'Add Your Flats',
    })

    handleBackPress = async () => {
      this.props.navigation.goBack()
      return true;
    }
    
    constructor(props){
        super(props);
        this.state={cities:[],dataSource:[],cars:[],selected_city:'',societies:[],blocks:[],myblock:'',flats:[],myflat:'',myflats:[],
        society:'',start_date:'',qr_img:'',end_date:'',spinner: true,owner_type:'Owner',search_s:'',selected_soc_name:'',name:'',mobile:'',email:''
        }
    }

  async pageRefresh(){
      
  }

  _onRefresh(){
    this.setState({refreshing:true})
    this.pageRefresh().then(()=>{
      this.setState({refreshing:false})
    })
  }

  async componentDidMount(){  
      BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
      const url2='http://agro-vision.in/dpal/Rest/cities_api';
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});  
        this.setState({cities:responseJson})
      }).catch((error)=>{ console.log(error)} );
      this.pageRefresh();
  }

  async addMyFlat(){
    //alert("sadf");return false;
    if(this.state.selected_city=="" ||  this.state.society=="" ||  this.state.myblock=="" ||  this.state.myflat==""){
      alert("Please Select All the Fields");return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    this.setState({spinner:true});  
    fetch('http://agro-vision.in/dpal/Rest/addflat_api/'+userid, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            city: this.state.selected_city,
            society: this.state.society,
            block:this.state.myblock,
            flat:this.state.myflat,
            owner:this.state.owner_type
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner:false});  
        console.log(responseJson);
        alert(responseJson.msg);
        //this.pageRefresh();
        //if(responseJson.status==0){
        this.setState({selected_city:''})
        this.setState({society:''});
        this.props.navigation.navigate("Dashboard");
        //}
      });
  }

  async logout(){
    await AsyncStorage.clear();
    alert("You are logout successfully.")
    this.props.navigation.navigate("Login");
  }

  getSociety(){
    this.setState({myblock:''})
    this.setState({blocks:[]})
    this.setState({flats:[]})
    if(this.state.search_s.length>=3){
    fetch('http://agro-vision.in/dpal/Rest/society_api/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            city: this.state.selected_city,
            soc: this.state.search_s,
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        //this.setState({myblock:''})
        this.setState({societies:responseJson})
      });
    }else{ this.setState({societies:[]})}
  }

  getBlocks(socid,socname){
    console.log('getb');
    this.setState({spinner: !this.state.spinner})
    this.setState({ search_s: socname})  
    this.setState({society:socid})
    //if(socid==""){return false;}
    const url5='http://agro-vision.in/dpal/Rest/blocks_api/'+socid;
        fetch(url5).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({spinner: !this.state.spinner});  
           this.setState({blocks:responseJson})
           this.setState({myblock:'Select Block'})
           //this.setState({societies:''})
           //console.log(responseJson)
        }).catch((error)=>{
          console.log(error)
        });
  }
  
  
  getFlats(bl){ 
    this.setState({myblock:bl})
    //alert(bl);return false;
    //if(this.state.myblock!=""){
      //this.setState({spinner: !this.state.spinner});  
    const url6='http://agro-vision.in/dpal/Rest/flats_api/'+bl;
        fetch(url6).then((response)=>response.json())
        .then((responseJson)=>{
         // this.setState({spinner: !this.state.spinner});  
           this.setState({flats:responseJson})
           this.setState({myblock:bl})
        }).catch((error)=>{
          console.log(error)
        });
     // }
  }

    render() {
      return (
        <KeyboardAvoidingView behavior="padding" enabled>
        <ScrollView  refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
        <Container>
        <Content>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <StatusBar backgroundColor="#1d2839" barStyle="light-content" />
            
           
          <View style={styles.chooseFlat}>
          <Text style={{fontSize:14,color:'grey'}}>Select City</Text>
          <View style={styles.pickers}>
          <Picker
                selectedValue={this.state.selected_city}
                style={{ width: '100%',height:40,borderRadius:20}}
                onValueChange={(itemValue) => this.setState({ selected_city: itemValue})}>
                <Picker.Item label="Select City" value="" />
                {this.state.cities.map((facility, i) => {
                  return <Picker.Item key={i} value={facility.city_id} label={facility.city_name} />
                })}
          </Picker>
          </View>
          {this.state.selected_city!="" &&
          <View>
          <Text style={{fontSize:14,color:'grey',marginTop:10}}>Search Your Society</Text>
          <TextInput style={styles.input} value={this.state.search_s}
          onChangeText={(search_s)=>this.getSociety(this.setState({search_s}))} placeholder="Enter Society Name"/>
          {this.state.myblock=="" &&
          <FlatList data={this.state.societies} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <ListItem onPress={()=>this.getBlocks(item.soc_id,item.soc_name)} >
            <Left>
                <Text style={{color:'grey',fontSize:16}}>{item.soc_name}-{item.soc_code}</Text>
            </Left>
            <Right>
                <Ionicons name="md-arrow-dropright" size={18} />
            </Right>
            </ListItem>}/>}
              </View>}{this.state.society!="" &&
              <View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:6}}>
              <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select Block/Building</Text>
              <View style={styles.pickers}>
              <Picker
                    selectedValue={this.state.myblock}
                    style={{ width: '100%',height:40,borderRadius:20 }} 
                    onValueChange={(itemValue) => this.getFlats(itemValue)} >
                      <Picker.Item label="Select Block" value="" />
                    {this.state.blocks.map((facility, i) => {
                      return <Picker.Item key={i} value={facility.block_id} label={facility.block_name} />
                    })}
              </Picker>
              </View>
              <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select Flat No.</Text>
              <View style={styles.pickers}>
              <Picker
                    selectedValue={this.state.myflat}
                    style={{ width: '100%',height:40,borderRadius:20 }}  
                    onValueChange={(itemValue) => this.setState({ myflat: itemValue})}>
                      <Picker.Item label="Select Flat" value="" />
                    {this.state.flats.map((facility, i) => {
                      return <Picker.Item key={i} value={facility.flat_id} label={facility.flat_no} />
                    })}
              </Picker>
              </View>
            
            </View>
            <View style={{flex:4,padding:5,marginLeft:10,alignContent:'center'}}>
            <Text style={{fontSize:14,marginBottom:5,marginTop:5,color:'grey'}}>You are</Text>
            <RadioForm buttonSize={14} 
              radio_props={owner_types} buttonColor={'#25be9f'} selectedButtonColor={'#25be9f'}
              initial={0} //formHorizontal={true}  labelHorizontal={true}
              onPress={(value)=>{this.setState({owner_type:value})}}/>
            </View>
          </View>
          <TouchableOpacity style={styles.btn} onPress={()=>this.addMyFlat()}>
            <Text style={styles.btnText}>
            <Ionicons style={{marginRight:10,color:'#fff'}} name="md-business" size={20}/> Add My Flat</Text>
          </TouchableOpacity></View>}
          </View>
          
          </Content>
        </Container>
          </ScrollView>
                  
          </KeyboardAvoidingView>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input:{width:'100%',borderRadius:5,borderColor:'grey', borderWidth:0.5, height:40,marginTop:2,padding:10,fontSize:16,
  backgroundColor:'#fff',borderRadius:20},
  chooseFlat:{shadowColor: '#000',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,
  shadowRadius: 2,elevation: 5,borderRadius:5,margin:5,padding:10,backgroundColor:'#fff'},
  pickers:{marginTop:2,borderWidth:0.5, borderColor:'grey',backgroundColor:'#fff',borderRadius:20},
  btn:{padding:10, backgroundColor:'#148F77',marginTop:10,borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'}
})

 