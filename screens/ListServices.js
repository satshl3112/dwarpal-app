
import React from 'react';
import { StyleSheet,BackHandler ,ActivityIndicator, Text,Picker,AsyncStorage, TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Header, Content, List, ListItem,Left,Right } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import * as Font from 'expo-font';

export default class ListServices extends React.Component {
  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'List Services',
  })
  constructor(props){
        super(props);
        this.state={ services:[],spinner:true,
        }
    }
    
    handleBackPress = async () => {
        this.props.navigation.goBack()
        return true;
    }
    async componentDidMount(){
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
      });
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
      const { navigation } = this.props;
      const state_id = navigation.getParam('center_id', '0');
      var socid=await AsyncStorage.getItem('society');
      const url2='http://agro-vision.in/dpal/Rest/services_api/'+socid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
         this.setState({spinner: !this.state.spinner}); 
         this.setState({services:responseJson})
      }).catch((error)=>{
        console.log(error)
      });
    }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <Container>
            <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <Content>
            <List>
            <FlatList data={this.state.services} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                <ListItem onPress={() => this.props.navigation.navigate('ListWorkers',{serid:item.serid,ser_title:item.ser_type})}>
                <Left>
                    <Text style={{fontFamily:'custom-fonts',fontSize:16}}>{item.ser_type} ({item.totl})</Text>
                </Left>
                <Right>
                    <Ionicons name="md-arrow-dropright" size={18} />
                </Right>
                </ListItem>}/>
            </List>
            </Content>
        </Container>    
    );}else {
      return (
          <View>
              <ActivityIndicator />
          </View>
      );
  }
  }
}

