import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,RefreshControl,
  TextInput,Picker,Button,Alert,FlatList,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';
import { Container,Content, Header, Left, Card,Textarea, CardItem, Body,Icon, Right, Title,Badge,Tabs,Tab } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import { HeaderBackButton } from 'react-navigation-stack';

export default class UserNotifications extends React.Component {
    static navigationOptions =  ({ navigation }) => ({
        headerTitleStyle: {color:'white'},
        headerStyle: {backgroundColor:'#25be9f'},
        headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Notifications',
      })

    constructor(props){
        super(props);
        this.state={pending_apprs:[],allnotifs:[]}
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }
    
    async pageRefresh(){
      var flatid = await AsyncStorage.getItem('flat');
      this.setState({spinner: !this.state.spinner});
      const url2='http://agro-vision.in/dpal/Rest/waiting_entries/'+flatid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
          console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({pending_apprs:responseJson});
      }).catch((error)=>{ console.log(error)} );
      const url3='http://agro-vision.in/dpal/Rest/user_notif_api2/'+flatid;
      fetch(url3).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({allnotifs:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async componentDidMount(){
      this.pageRefresh();
    }

    async approveEntry(id,st){
        var userid = await AsyncStorage.getItem('userid');
        Alert.alert(
            'Confirm Action',
            'Sure you want to '+st,
            [
              {
                text: 'No',
                onPress: () => console.log('Cancel Pressed.'),
                style: 'cancel',
              },
              {text: 'Yes', 
                onPress:()=>{
                    this.setState({spinner: !this.state.spinner});
                    const url6='http://agro-vision.in/dpal/Rest/approveEntry/'+id+'/'+st+'/'+userid;
                    fetch(url6).then((response)=>response.json())
                    .then((responseJson)=>{
                        console.log(responseJson)
                       this.setState({spinner: !this.state.spinner}); 
                      alert(responseJson.msg)
                    }).catch((error)=>{
                      console.log(error)
                    });
                }
              },
            ],
            {cancelable: false},
          );
      }

    render() {
      return (
          <Container><Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
            <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
              <Content>
               <Tabs>
                 <Tab heading="Awaiting At Gate"  tabStyle={{backgroundColor: '#1e2939'}} activeTabStyle={{backgroundColor: '#25be9f'}}>
                 <FlatList data={this.state.pending_apprs} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
              <CardItem>
                <TouchableOpacity  style={{flexDirection:'row'}} onPress={()=>this.props.navigation.navigate("AskApproval",{nid:item.nentry_id})}>
                  <Image style={{width:65, height:65,borderRadius:50,marginRight:10}} source={{uri: 'http://agro-vision.in/dpal/assets/images/guests/'+item.nentry_img}}/>
                
                <View style={{flex:4}}>
                  <Text style={{fontSize:18,color:'#17A589'}}>{item.nentry_type}</Text>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>{item.block_name} Flat No {item.flat_no}</Text>
                  <Text>{item.save_time}</Text></View>
                  <View style={{flexDirection:'row'}}>
                  <Text style={{color:'orange',fontSize:16}}>{item.nentry_name} • {item.nentry_phone}</Text>
                  </View>
                </View>
                </TouchableOpacity>
              </CardItem>
              {item.nentry_status!="" &&
              <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  {item.nentry_status=="Approved" &&
                  <Text style={{fontSize:16,color:'#1E8449'}}>
                        <Ionicons name="md-log-in" color='#1E8449'size={18}/> Approved {item.nentry_time}</Text>}
                        {item.nentry_status=="Denied" &&
                    <Text style={{fontSize:16,color:'#EC7063'}}>
                        <Ionicons name="md-close" color='#EC7063'size={18}/> Denied {item.nentry_time}</Text>}
                        
              </CardItem>}
              {item.nentry_status=="" &&
              <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  <TouchableOpacity onPress={()=>this.approveEntry(item.nentry_id,'Approved')}>
                    <Text style={{fontSize:18,color:'#1E8449'}}>
                        <Ionicons name="md-checkmark" color='#1E8449'size={18}/> Approve</Text></TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.approveEntry(item.nentry_id,'Denied')}>
                    <Text style={{fontSize:18,color:'#EC7063'}}>
                        <Ionicons name="md-close" color='#EC7063'size={18}/> Deny</Text></TouchableOpacity>
              </CardItem>}
            </Card>
          }/>
                 </Tab>
                 <Tab heading="Notifications"  tabStyle={{backgroundColor: '#1e2939'}} activeTabStyle={{backgroundColor: '#25be9f'}}>
                 <FlatList data={this.state.allnotifs} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
              <CardItem style={{flexDirection:'row'}}>
                 <Image style={{width:65, height:65,borderRadius:50,marginRight:10}} 
                 source={{uri: 'https://static.vecteezy.com/system/resources/previews/000/363/879/non_2x/notification-vector-icon.jpg'}}/>
                
                <View style={{flex:4}}>
                  <Text style={{fontSize:16,color:'#17A589'}}>{item.not_title}</Text>
                  <Text style={{fontSize:14,color:'grey'}}>{item.not_desc}</Text>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>{item.not_type}</Text>
                  <Text>{item.not_datetime}</Text></View>
                </View>
              </CardItem>
              </Card>
          }/>
                 </Tab>
               </Tabs>
               
              </Content>
            </ScrollView>
          </Container>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',justifyContent:'center'
  },input:{width:'100%',borderRadius:2,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
})
