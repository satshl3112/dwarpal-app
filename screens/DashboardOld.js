import React from 'react';
import { StyleSheet, Text,View,ScrollView,Image,Picker,TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Ionicons } from '@expo/vector-icons';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var owner_types=[
  {label:'Owner of Flat', value:'Owner of Flat'},
  {label:'Renting the Flat', value:'Renting the Flat'}
]

export default class Dashboard extends React.Component {

  constructor(props){
    super(props);
     this.state={cities:[],dataSource:[],cars:[],selected_city:'',societies:[],blocks:[],myblock:'',flats:[],myflat:'',
     society:'',start_date:'',end_date:'',spinner: false,owner_type:'Owner of Flat'
     }
  }

  componentDidMount(){
      //this.setState({spinner: !this.state.spinner});  
      const url2='http://agro-vision.in/dpal/Rest/cities_api';
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        //this.setState({spinner: !this.state.spinner});  
        this.setState({cities:responseJson})
      }).catch((error)=>{ console.log(error)} );
  }

  getSociety(cityid){
    //if(cityid==""){return false;}
    //alert(cityid)
    const url4='http://agro-vision.in/dpal/Rest/society_api/'+cityid;
        fetch(url4).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({societies:responseJson})
          // console.log(responseJson)
        }).catch((error)=>{
          console.log(error)
        });
  }

  getBlocks(socid){
    //if(socid==""){return false;}
    const url5='http://agro-vision.in/dpal/Rest/blocks_api/'+socid;
        fetch(url5).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({blocks:responseJson})
           //console.log(responseJson)
        }).catch((error)=>{
          console.log(error)
        });
  }

  getFlats(block){ ///if(block==""){return false;}
    //console.log(socid)
    //alert(socid)
    const url6='http://agro-vision.in/dpal/Rest/flats_api/'+block;
        fetch(url6).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({flats:responseJson})
           //console.log(responseJson)
        }).catch((error)=>{
          console.log(error)
        });
  }


    render() {
      return (
        <ScrollView style={styles.container}>
          <Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
          <View style={{flexDirection:'row',padding:5,backgroundColor:'#D1F2EB',margin:5,borderRadius:5,borderWidth:2,borderColor:'#25be9f'}}>
            <View style={{flex:1}}>
            <Image style={{width:50, height:50,marginBottom:10,margin:5}} source={require('.././assets/myflat.png')}/>
            </View>
            <View style={{flex:5,alignItems:'center'}}>
              <Text style={{fontSize:20,color:'#148F77'}}>Add Your Flat</Text>
            </View>
          </View>
          <View style={styles.chooseFlat}>
          <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select City</Text>
          <View style={styles.pickers}>
          <Picker
                selectedValue={this.state.selected_city}
                style={{ width: '100%',height:40 }} onValueChange={this.getSociety(this.state.selected_city)} 
                onValueChange={(itemValue) => this.setState({ selected_city: itemValue})}>
                <Picker.Item label="Select City" value="" />
                {this.state.cities.map((facility, i) => {
                  return <Picker.Item key={i} value={facility.city_id} label={facility.city_name} />
                })}
          </Picker>
          </View>
          <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select Society</Text>
          <View style={styles.pickers}>
          <Picker
                selectedValue={this.state.society}
                style={{ width: '100%',height:40 }} onValueChange={this.getBlocks(this.state.society)} 
                onValueChange={(itemValue) => this.setState({ society: itemValue})}>
                  <Picker.Item label="Select Society" value="" />
                {this.state.societies.map((facility, i) => {
                  return <Picker.Item key={i} value={facility.soc_id} label={facility.soc_name} />
                })}
          </Picker>
          </View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select Block/Building</Text>
              <View style={styles.pickers}>
              <Picker
                    selectedValue={this.state.myblock}
                    style={{ width: '100%',height:40 }}  onValueChange={this.getBlocks(this.state.myblock)}
                    onValueChange={(itemValue) => this.setState({ myblock: itemValue})}>
                      <Picker.Item label="Select Block" value="" />
                    {this.state.blocks.map((facility, i) => {
                      return <Picker.Item key={i} value={facility.block_id} label={facility.block_name} />
                    })}
              </Picker>
              </View>
            </View>
            <View style={{flex:1,marginLeft:5}}>
              <Text style={{fontSize:14,color:'grey',marginTop:10}}>Select Flat No.</Text>
              <View style={styles.pickers}>
              <Picker
                    selectedValue={this.state.myflat}
                    style={{ width: '100%',height:40 }}  onValueChange={this.getFlats(this.state.myflat)} 
                    onValueChange={(itemValue) => this.setState({ myblock: itemValue})}>
                      <Picker.Item label="Select Flat" value="" />
                    {this.state.flats.map((facility, i) => {
                      return <Picker.Item key={i} value={facility.flat_id} label={facility.flat_no} />
                    })}
              </Picker>
              </View>
            </View>
          </View>

        
          <Text style={{fontSize:16,marginBottom:10,color:'grey'}}>You are</Text>
          <RadioForm 
            radio_props={owner_types} buttonColor={'#25be9f'}
            initial={0} //formHorizontal={true}  labelHorizontal={true}
            onPress={(value)=>{this.setState({owner_type:value})}}/>

          <TouchableOpacity style={styles.btn} onPress={()=>this.searchCars()}><Text style={styles.btnText}>
          <Ionicons style={{marginRight:10,color:'#fff'}} name="md-business" onPress={()=>searchCars()} size={20}/> Add My Flat</Text>
          </TouchableOpacity>
          </View>
        </ScrollView>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  chooseFlat:{margin:5,backgroundColor:'#E8F8F5',padding:10,borderWidth:1,borderColor:'#25be9f'},
  pickers:{borderWidth:1, borderColor:'grey',backgroundColor:'#fff'},
  btn:{padding:10, backgroundColor:'#25be9f',marginTop:10},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'}
})

