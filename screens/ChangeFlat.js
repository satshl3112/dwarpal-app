import React from 'react';
import { StyleSheet, Text,View,ScrollView,AsyncStorage,Image,Picker,FlatList,TouchableOpacity,TextInput } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Ionicons } from '@expo/vector-icons';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Container,Header, Content,Left,Right,Body,CardItem,Card,Title} from 'native-base';

var owner_types=[
  {label:'Owner of Flat', value:'Owner of Flat'},
  {label:'Renting the Flat', value:'Renting the Flat'},
  {label:'Builder', value:'Builder'}
]

export default class ChangeFlat extends React.Component {

  constructor(props){
    super(props);
     this.state={cities:[],dataSource:[],cars:[],selected_city:'',societies:[],blocks:[],myblock:'',flats:[],myflat:'',myflats:[],
     society:'',start_date:'',end_date:'',spinner: true,owner_type:'Owner of Flat',search_s:'',selected_soc_name:'',name:'',mobile:'',email:''
     }
  }

  async componentDidMount(){
      var userid = await AsyncStorage.getItem('userid');
      this.setState({name:await AsyncStorage.getItem('name')});
      this.setState({mobile:await AsyncStorage.getItem('mobile')});
      this.setState({email:await AsyncStorage.getItem('email')});
      //this.setState({spinner: !this.state.spinner});  
      const url2='http://agro-vision.in/dpal/Rest/cities_api';
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});  
        this.setState({cities:responseJson})
      }).catch((error)=>{ console.log(error)} );

      const url1='http://agro-vision.in/dpal/Rest/myflats/'+userid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson)
        this.setState({myflats:responseJson})
      }).catch((error)=>{ console.log(error)} );
  }
  
  gotoSociety(s,b,f,fno,c,blck,so,st){
    //alert(f)
    if(st!="Approved"){
      alert("Your Request is Pending");return false;
    }
    AsyncStorage.setItem("blockname",blck);
    AsyncStorage.setItem("society",s);
    AsyncStorage.setItem("societyname",so);
    AsyncStorage.setItem("block",b);
    AsyncStorage.setItem("flat",f);
    AsyncStorage.setItem("flat_no",fno);
    AsyncStorage.setItem("city",c);
    this.props.navigation.navigate("Household");
  }


    render() {
      return (
           <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View style={item.req_status=="Approved"?{flexDirection:'row',padding:2,backgroundColor:'#76D7C4',margin:5,borderRadius:5,borderWidth:2,borderColor:'#27AE60'}:
            {flexDirection:'row',padding:2,backgroundColor:'#F5B7B1',margin:5,borderRadius:5,borderWidth:2,borderColor:'#EC7063'}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/myflat.png')}/>
              </View>
              <View style={{flex:7,paddingLeft:10}}>
              <Text  onPress={()=>this.gotoSociety(item.soc_id,item.block_id,item.flat_id,item.flat_no,item.city_id,item.block_name,
                item.soc_name,item.req_status)} style={{fontSize:18,color:'#1e2939'}}>{item.soc_name}</Text>
                <Text style={{fontSize:16,color:'#fff'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              <View style={{flex:1,alignContent:'center'}}><Ionicons color="#fff" name="md-arrow-dropright" size={55}/></View>
            </View>}/>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input:{width:'100%', borderRadius:2, borderWidth:1, height:40, padding:5,fontSize:16,backgroundColor:'#fff'},
  chooseFlat:{margin:5,backgroundColor:'#E8F8F5',padding:5,borderWidth:2,borderColor:'#25be9f', borderRadius:2},
  pickers:{borderWidth:1, borderColor:'grey',backgroundColor:'#fff'},
  btn:{padding:10, backgroundColor:'#25be9f',marginTop:10},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'}
})

 