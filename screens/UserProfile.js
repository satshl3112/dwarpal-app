
import React from 'react';
import { StyleSheet,ScrollView, Text,Image, AsyncStorage,TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { Container,Content,Header, Left, Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';
import { HeaderBackButton } from 'react-navigation-stack';
import * as ImagePicker from 'expo-image-picker';
import Modal from "react-native-modal";
import { Ionicons } from '@expo/vector-icons';

export default class UserProfile extends React.Component {
  constructor(props){
    super(props);
    this.state={ password:'',cpassword:'',name:'',phone:'',email:'',uimg:'user.png',oldphone:'',gotp:'',update_otp_modal:false,otp:''
    }
  }

  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Update Profile',
  })
  
  async componentDidMount(){
    this.setState({name:await AsyncStorage.getItem('name')});
    this.setState({phone:await AsyncStorage.getItem('mobile')});
    this.setState({oldphone:await AsyncStorage.getItem('mobile')});
    this.setState({email:await AsyncStorage.getItem('email')});
    this.setState({uimg:await AsyncStorage.getItem('uimg')});
  }

  _pickImage = async () => {
    this.setState({spinner: !this.state.spinner});
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 4],
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
      let localUri = result.uri;
      let filename = localUri.split('/').pop();

      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;

      let formData = new FormData();
      formData.append('photo', { uri: localUri, name: filename, type });

      return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
        method: 'POST',
        body: formData,
        header: {
          'content-type': 'multipart/form-data',
        },
      }).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({uimg:responseJson.img});
      });
    }else{
      this.setState({spinner: !this.state.spinner});
    }
  };

  async changePassword_api(){
    if(this.state.password=='' || this.state.cpassword=='' || this.state.password.length<5){
      alert("Both Password must match and min 5 character long.");return false;
    }
  }

  async verify_otp_check(){
    if(this.state.otp==this.state.gotp){
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      fetch('http://agro-vision.in/dpal/Rest/update_profile_api/'+userid, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              name:this.state.name,
              phone:this.state.phone,
              email:this.state.email,uimg:this.state.uimg
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            console.log(responseJson)
            alert(responseJson.msg);
            AsyncStorage.setItem("uimg",this.state.uimg);
      });
    }else{alert("Incorrect OTP.")}
  }

  async verify_otp(){
    this.setState({spinner: !this.state.spinner});
    const url22='http://agro-vision.in/dpal/Rest/update_otp_verify/'+this.state.phone;
    fetch(url22).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        if(responseJson.status==0){
          console.log(responseJson)
          this.setState({update_otp_modal:true});
          this.setState({gotp:responseJson.gotp});
        }else{alert(responseJson.msg)}
      }).catch((error)=>{ console.log(error)} );
  }

  async update_profile(){
    //alert("Profile Updated Successfully");return false;
    if(this.state.name=='' || this.state.phone=='' || this.state.email==""){
      alert("Please Enter All the Details.");return false;
    }
    if(this.state.phone!=this.state.oldphone){
      this.verify_otp();return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    fetch('http://agro-vision.in/dpal/Rest/update_profile_api/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                name:this.state.name,
                phone:this.state.phone,
                email:this.state.email,uimg:this.state.uimg
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              console.log(responseJson)
              alert(responseJson.msg);
              AsyncStorage.setItem("uimg",this.state.uimg);
    });
  }

  async changeWpin(){
    if(this.state.wpin=='' || this.state.cwpin=='' || this.state.wpin.length<4){
      alert("Both Wallet must match and min 4 digit long.");return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    fetch('http://iglobal.in/home/change_wpin_api/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                wpin:this.state.wpin,
                cwpin:this.state.cwpin
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              //this.setState({dataSource:responseJson});
              //console.log(responseJson);
              alert(responseJson.msg);
    });
  }

    render() {
      return (
        <ScrollView>
        <Modal isVisible={this.state.update_otp_modal} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Verify OTP</Text>
              </CardItem>
              <CardItem>
                <Body style={{alignItems:'center',alignContent:'center'}}>
                <Image style={{width:100, height:150,marginBottom:10}} source={require('.././assets/otp.png')}/>
                <TextInput keyboardType = "number-pad" maxLength={6} onChangeText={(otp)=>this.setState({otp})}  
                style={styles.input} placeholder="Verify OTP"/>
                </Body>
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                  <TouchableOpacity style={{backgroundColor:'grey',padding:10,borderRadius:20}} onPress = {() => { this.setState({ update_otp_modal:!this.state.update_otp_modal})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:'#117A65',padding:10,borderRadius:20}} onPress={()=>this.verify_otp_check()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Verify OTP</Text></TouchableOpacity>
              </CardItem>
            </Card>
          </Modal>
          <Container>
          <Content>
           <Card style={{padding:10}}>
           <TouchableOpacity style={{alignSelf:'center',marginBottom:5}} onPress={()=>this._pickImage()}>
              <Image style={{width:100,height:100}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.uimg }}/>
            </TouchableOpacity>
            <TextInput style={styles.input} onChangeText={(name)=>this.setState({name:name})} value={this.state.name} />
            <TextInput keyboardType = "number-pad"  maxLength={10} style={styles.input} onChangeText={(phone)=>this.setState({phone:phone})} value={this.state.phone} />
            <TextInput style={styles.input} onChangeText={(email)=>this.setState({email:email})} value={this.state.email} />
            <TouchableOpacity style={styles.btn} onPress={()=>this.update_profile()}>
                  <Text style={styles.btnText}>Update Profile</Text></TouchableOpacity>  
           </Card>
          </Content>
        </Container>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  btn:{width:'100%',marginTop:10, padding:10, backgroundColor:'#25be9f',borderRadius:20},
  btnText:{fontSize:16, textAlign:'center', color:'#fff'},
  input:{borderRadius:20,width:'100%', borderWidth:0.5, height:40, marginBottom:10,padding:10,fontSize:16,backgroundColor:'#fff'},
});
