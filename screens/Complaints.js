import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,RefreshControl,KeyboardAvoidingView,CheckBox,BackHandler,
  TextInput,Picker,Button,Alert,FlatList,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import { Container,Content, Header, Left, Card,Textarea, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import { HeaderBackButton } from 'react-navigation-stack';
import Modal from "react-native-modal";

export default class Complaints extends React.Component {
  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Complaints'})
  
    constructor(props){
        super(props);
        this.state={subject:'',complain:'',complaints_list:[],is_secr:null,vModal:false,complainr:'',replycid:''}
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }
   
    async pageRefresh(){
        var type='one';
        this.setState({is_secr:await AsyncStorage.getItem('is_secr')})
        if(this.state.is_secr!=null){type='all'}
        var flatid = await AsyncStorage.getItem('flat');
        var soc = await AsyncStorage.getItem('society');
        this.setState({spinner: true}); 
        const url2='http://agro-vision.in/dpal/Rest/complaint_list_api/'+flatid+'/'+type+'/'+soc;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: false}); 
            this.setState({complaints_list:responseJson})
        }).catch((error)=>{
            console.log(error)
        });
    }

    handleBackPress = async () => {
        this.props.navigation.goBack()
        return true;
    }

    async componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
        this.pageRefresh();
    }

    async add_complaint_api(){
        if(this.state.subject=="" || this.state.complain==""){alert("Please Enter Subject & Complaint.");return false;}
        this.setState({spinner: !this.state.spinner});
        fetch('http://agro-vision.in/dpal/Rest/add_complaint_api/', {
            method: 'POST',
            headers: {Accept: 'application/json','Content-Type': 'application/json'},
            body: JSON.stringify({
                flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
                subject: this.state.subject,complain: this.state.complain,society:await AsyncStorage.getItem('society'),
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: !this.state.spinner});
            this.pageRefresh();
            alert(responseJson.msg);
        });
    }

    openReply(rcid){
      this.setState({vModal:true})
      this.setState({replycid:rcid})
    }

    async replyComplaint(){
      if(this.state.complainr==""){alert("Please Enter Complaint Reply.");return false;}
        this.setState({spinner: !this.state.spinner});
        fetch('http://agro-vision.in/dpal/Rest/reply_complaint_api/', {
            method: 'POST',
            headers: {Accept: 'application/json','Content-Type': 'application/json'},
            body: JSON.stringify({
                userid: await AsyncStorage.getItem('userid'),flat: await AsyncStorage.getItem('flat'),
                complainr: this.state.complainr,rcid: this.state.replycid,society:await AsyncStorage.getItem('society'),
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            console.log(responseJson);this.setState({vModal:false})
            this.setState({spinner: !this.state.spinner});
            this.pageRefresh();
            alert(responseJson.msg);
        });
    }

    render() {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <Container>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <Modal isVisible={this.state.vModal} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Reply</Text>
              </CardItem>
              <CardItem>
              <Textarea  onChangeText={(complainr)=>this.setState({complainr})}  
                        style={{width:'100%', borderRadius:20,marginBottom:10,marginTop:10,padding:3,borderWidth:0.5}}
                        rowSpan={3} bordered placeholder="Type Your Reply Here" />
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                  <TouchableOpacity style={{backgroundColor:'grey',padding:10,borderRadius:20}} onPress = {() => { this.setState({ vModal:!this.state.vModal})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:'#117A65',padding:10,borderRadius:20}} onPress={()=>this.replyComplaint()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Reply</Text></TouchableOpacity>
              </CardItem>
            </Card>
          </Modal>
         
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} 
          onRefresh={this._onRefresh.bind(this)}/>}>
              <Content>
              {this.state.is_secr==null &&
                <Card>
                  <CardItem header bordered style={{backgroundColor:'orange'}}>
                    <Text style={{color:'#fff',fontSize:16,fontFamily:'custom-fonts'}}>Raise New Complaint</Text></CardItem>
                  <View style={{padding:10}}>
                    <TextInput style={styles.input}  
                    onChangeText={(subject)=>this.setState({subject})} placeholder="Enter Subject"/>
                    <Textarea  onChangeText={(complain)=>this.setState({complain})}  
                        style={{width:'100%', borderRadius:20,marginBottom:10,marginTop:10,padding:3,borderWidth:0.5}}
                        rowSpan={3} bordered placeholder="Type Your Complaint Here" />
                    <TouchableOpacity style={{backgroundColor:'#17A589',padding:10,borderRadius:20}} 
                    onPress={()=>this.add_complaint_api()}>
                        <Text style={{color:'#fff',fontSize:16,textAlign:'center'}}>Log Complaint</Text>
                    </TouchableOpacity>
                  </View>
                </Card>}
                <FlatList data={this.state.complaints_list} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                    <Card>
                        <CardItem bordered style={{backgroundColor:'#EC7063'}}>
                          <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>{item.comp_subj}</Text></CardItem>
                        <CardItem>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.comp_desc}</Text></CardItem>
                        <CardItem style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#F2F3F4'}}>
                        <Text>{item.comp_status}</Text>
                        <Text>{item.comp_log_date}</Text>
                        {this.state.is_secr!=null && item.comp_status=='Pending' &&
                        <TouchableOpacity style={{backgroundColor:'orange',padding:5,borderRadius:5}} 
                        onPress={()=>this.openReply(item.comp_id)}>
                          <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:14}}>Reply</Text></TouchableOpacity>}
                        </CardItem>
                        {item.comp_status!='Pending' &&
                        <View>
                          <CardItem bordered style={{backgroundColor:'#52BE80'}}><Text>Replied {item.comp_reply_date}</Text></CardItem>
                          <CardItem><Text>{item.comp_reply}</Text></CardItem>
                        </View>}
                    </Card>
                }/>
                
              </Content>
            </ScrollView>
          </Container>
        </KeyboardAvoidingView>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',justifyContent:'center'
  },input:{width:'100%',borderRadius:20,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
})
