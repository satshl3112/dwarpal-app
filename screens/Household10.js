import React from 'react';
import { Linking,StyleSheet,BackHandler, WebView,Text,ScrollView,RefreshControl,CheckBox,Image, View,Picker,KeyboardAvoidingView,TextInput,FlatList,Button,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container,Content, Header, Left, Card,Form,CardItem,List,ListItem, Body,Icon, Right, Title,Fab,Tabs,Tab } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import DatePicker from 'react-native-datepicker';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from "react-native-modal";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import ActionButton from 'react-native-action-button';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import MultiSelect from 'react-native-multiple-select';

const day1 = new Date();
const day15=new Date(new Date().getTime()+(15*24*60*60*1000));
const day30=new Date(new Date().getTime()+(30*24*60*60*1000));

var cabtypes=[
  {label:'Once', value:'Once'},
  {label:'Frequent', value:'Frequent'}
]
var items = [{
  id: 'Sunday',
  name: 'Sunday',
}, {
  id: 'Monday',
  name: 'Monday',
}, {
  id: 'Tuesday',
  name: 'Tuesday',
}, {
  id: 'Wednesday',
  name: 'Wednesday',
}, {
  id: 'Thursday',
  name: 'Thursday',
}, {
  id: 'Friday',
  name: 'Friday',
}, {
  id: 'Saturday',
  name: 'Saturday',
}];


export default class Household extends React.Component {
    constructor(props){
      super(props);
      this.state={myfam:[],new_name:'',gender:'',new_email:'',nofam:true,new_mobile:'',name:'',mobile:'',flat_no:'',myflats:[],
      email:'',userid:'',age_group:'',active:false,helps:[],nohelp:true,isVisible: false,new_user_image:'add_family.jpg',
      isVisible2:false,token: '',error: '',quick_actions:false,cab_tabs:false,cabtype:'Once',cabtime:'',vcomp:'',vnumber:'',
      selectedItems : [],validity:'',start_time:'',end_time:'',delivery_tabs:false,service_list:false,help_type:'',cab1advance:false,
      cab2advance:false,del1advance:false,del2advance:false}
    }
   
    onSelectedItemsChange = selectedItems => {
      this.setState({ selectedItems });
    };
   
    _pickImage = async () => {
      this.setState({spinner: !this.state.spinner});
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 4],
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
  
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
  
        let formData = new FormData();
        formData.append('photo', { uri: localUri, name: filename, type });
  
        return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
          method: 'POST',
          body: formData,
          header: {
            'content-type': 'multipart/form-data',
          },
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({new_user_image:responseJson.img});
        });
      }else{
        this.setState({spinner: !this.state.spinner});
      }
    };

    async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      this.setState({userid:await AsyncStorage.getItem('userid')});
      this.setState({name:await AsyncStorage.getItem('name')});
      this.setState({mobile:await AsyncStorage.getItem('mobile')});
      this.setState({email:await AsyncStorage.getItem('email')});
      this.setState({flat_no:await AsyncStorage.getItem('flat_no')+' '+await AsyncStorage.getItem('blockname')+' '+await AsyncStorage.getItem('societyname')});
      //Get Family Members
      const url2='http://agro-vision.in/dpal/Rest/myfamily_api/'+flatid+'/'+userid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        if(responseJson.status==1){
          this.setState({nofam:false})
          this.setState({myfam:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)} );
      //Get Daily Helps
      const url1='http://agro-vision.in/dpal/Rest/daily_helper_api/'+flatid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson);
        if(responseJson.status==1){
          this.setState({nohelp:false})
          this.setState({helps:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)});
    }

    async componentDidMount(){
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
      });
      this.pageRefresh();
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    async changeFlat(){
      var userid = await AsyncStorage.getItem('userid');
      this.setState({ isVisible2:!this.state.isVisible2})
      this.setState({spinner: !this.state.spinner});
      const url1='http://agro-vision.in/dpal/Rest/myflats/'+userid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({myflats:responseJson})
      }).catch((error)=>{ console.log(error)} );
    }

    gotoSociety(s,b,f,fno,c,blck,so,st){
      //alert(f)
      if(st!="Approved"){
        alert("Your Request is Pending");return false;
      }
      AsyncStorage.setItem("blockname",blck);
      AsyncStorage.setItem("society",s);
      AsyncStorage.setItem("societyname",so);
      AsyncStorage.setItem("block",b);
      AsyncStorage.setItem("flat",f);
      AsyncStorage.setItem("flat_no",fno);
      AsyncStorage.setItem("city",c);
      this.setState({ isVisible2:!this.state.isVisible2})
      this.setState({nohelp:true})
      this.setState({nofam:true})
      this.pageRefresh();
    }

    addNewFlat(){
      this.setState({ isVisible2:!this.state.isVisible2})
      this.props.navigation.navigate("Dashboard")
    }

    selectedAction(type){
      if(type=="cab_tabs"){this.setState({cab_tabs:true});}
      if(type=="delivery_tabs"){this.setState({delivery_tabs:true});this.setState({help_type:'Delivery'});}
      this.setState({quick_actions:false});
    }

    open_help(type){
      this.setState({delivery_tabs:true});
      this.setState({service_list:false});
      this.setState({help_type:type});
      this.setState({quick_actions:false});
    }

    async addnewMember(){
      if(this.state.new_name=="" || this.state.age_group==""  || this.state.gender=="" || this.state.new_mobile==""){
          alert("Please Enter All The Fields.");
        }else{
          var userid = await AsyncStorage.getItem('userid');
          this.setState({spinner: !this.state.spinner});
          fetch('http://agro-vision.in/dpal/Rest/add_new_member/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  name: this.state.new_name,gender:this.state.gender,age_group:this.state.age_group,
                  mobile: this.state.new_mobile,
                  email: this.state.new_email,img:this.state.new_user_image,
                  society:await AsyncStorage.getItem('society'),
                  block:await AsyncStorage.getItem('block'),
                  flat:await AsyncStorage.getItem('flat'),
                  city:await AsyncStorage.getItem('city'),
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            this.setState({ isVisible:!this.state.isVisible})
            this.setState({myfam:responseJson})
            console.log(responseJson);
            });
        }
    }

    async allowCabOnce(){
      if(this.state.vnumber=="" || this.state.vnumber.length<4){alert("Please Enter Last 4 Digit of Vehicle Number");return false;}
      if(this.state.cabtime=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowCabOnce/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              cabtime: this.state.cabtime,vnumber: this.state.vnumber,vcomp: this.state.vcomp,start_time:this.state.start_time
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false});
          alert(responseJson.msg);
        });
    }
    async allowDeliveryOnce(){
      if(this.state.validity=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowDeliveryOnce/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              start_time: this.state.start_time,validity: this.state.validity,vcomp: this.state.vcomp,help_type:this.state.help_type
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({delivery_tabs:false});
          //alert(responseJson.msg);
        });
    }

    async allowDeliveryFreq(){
      if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){
      alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowDeliveryFreq/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
              vnumber: this.state.vnumber,start_time:this.state.start_time,end_time:this.state.end_time,help_type:this.state.help_type
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({delivery_tabs:false});
          alert(responseJson.msg);
        });
    }

    async allowCabFreq(){
      ///alert(this.state.selectedItems);return false;
      if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){alert("Please Select All Fields.");
      return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowCabFreq/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
              vnumber: this.state.vnumber,start_time:this.state.start_time,end_time:this.state.end_time
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false});
          alert(responseJson.msg);
        });
    }

    render() {
      return (
          <Container style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
          <View style={{shadowColor: '#1d2839',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,shadowRadius: 2,elevation: 5,
           backgroundColor:'#5D6D7E',borderRadius:5,padding:5,margin:5, flexDirection:'row'}}>
            <Text onPress = {() => this.changeFlat()} style={{color:'#fff',fontSize:16}}>{this.state.flat_no}</Text>
            <Ionicons name="md-arrow-dropdown" color='#fff' style={{marginLeft:20}} size={25}/>
           </View>
           <Modal isVisible={this.state.isVisible2}>
           <Card>
            <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
              <Text style={{color:'#fff'}}>Select Flats</Text>
            </CardItem>
            <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View style={item.req_status=="Approved"?{shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#76D7C4',margin:5,borderRadius:5}:
            {shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#F1948A',margin:5,borderRadius:5}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/myflat.png')}/>
              </View>
              <View style={{flex:7,paddingLeft:10}}>
              <Text  onPress={()=>this.gotoSociety(item.soc_id,item.block_id,item.flat_id,item.flat_no,item.city_id,item.block_name,
                item.soc_name,item.req_status)} style={{fontSize:18,color:'#1e2939'}}>{item.soc_name}</Text>
                <Text style={{fontSize:16,color:'#fff'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              <View style={{flex:1,alignContent:'center'}}><Ionicons color="#fff" name="md-arrow-dropright" size={55}/></View>
            </View>}/>
            <CardItem footer bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Button title="Add New Flat" onPress={()=>this.addNewFlat()} color="#117A65"/>
              <Button title="Cancel" color="grey" style={{marginLeft:10}} onPress = {() => { this.setState({ isVisible2:!this.state.isVisible2})}}/>
            </CardItem>
          </Card>
           </Modal>          
           <Modal isVisible={this.state.cab_tabs}>
           <View style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10}}>Allow Cab Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({cab_tabs:false})}} size={22}/>
              </View>
           </View>
           <View style={{flex:1}}>
                    <Tabs>
                      <Tab heading="Once" style={{padding:10,height:200}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Allow My Cab to Enter The Society</Text>
                      
                      <DatePicker style={{borderRadius:10,width:'100%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.start_time} mode="datetime" format="DD-MM-YYYY LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Validity</Text>
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:10,padding:4}}>
                      <Picker
                      selectedValue={this.state.cabtime}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ cabtime: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                    <View style={{flexDirection:'row',alignContent:'space-around',marginTop:10}}>
                      <View style={{flex:6,marginRight:5}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Last 4 Digit of Vehicle No</Text>
                      <TextInput keyboardType = "number-pad" style={styles.input2} placeholder="1234"
    maxLength={10} onChangeText={(vnumber)=>this.setState({vnumber})}/>
                      </View>
                      <View style={{flex:4}}>
                        <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                        <View style={{borderWidth:1,borderColor:'grey',borderRadius:10,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                        </View>
                      </View>
                      </View>
                  <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabOnce()}>
                  <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                      <Tab heading="Frequent" style={{padding:10,height:200}}>
                      <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                    <View style={{flexDirection:'row',marginTop:5}}>
                      <View style={{flex:3}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:10,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Day"/>
                          <Picker.Item value="30 Days" label="30 Day"  />
                        </Picker>
                      </View>
                     </View>
                     <View style={{flex:2}}>
                     <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                     <View style={{borderWidth:1,borderColor:'grey',borderRadius:10,padding:4}}>
                      <Picker
                      selectedValue={this.state.vcomp}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                        <Picker.Item label="Select" value="" />
                        <Picker.Item label="Ola" value="Ola" />
                        <Picker.Item label="Uber" value="Uber" />
                        <Picker.Item label="Jugnoo" value="Jugnoo" />
                        <Picker.Item label="Other" value="Other" />
                      </Picker></View></View>
                    </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                    <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                    <DatePicker style={{borderRadius:10}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                    <DatePicker style={{borderRadius:10}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.end_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                    </View>
                    <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                    </Tabs>
                  </View>
                <Card>
                  
                  
                  
                </Card>
           </Modal>
           <Modal isVisible={this.state.delivery_tabs}>
           <View style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10}}>Allow {this.state.help_type} Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({delivery_tabs:false})}} size={22}/>
              </View>
           </View>
           <View style={{flex:1}}>
                    <Tabs>
                      <Tab heading="Once" style={{padding:10,height:200}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Allow {this.state.help_type} to Enter Society</Text>
                      <DatePicker style={{borderRadius:10,width:'100%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.start_time} mode="datetime" format="DD-MM-YYYY LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                    <View style={{flexDirection:'row',alignContent:'space-around',marginTop:10}}>
                      <View style={{flex:3,marginRight:5}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Validity</Text>
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:10,padding:4}}>
                      <Picker
                      selectedValue={this.state.validity}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ validity: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      </View>
                      <View style={{flex:2}}>
                        <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                        <TextInput  style={styles.input2} placeholder="Company" onChangeText={(vcomp)=>this.setState({vcomp})}/>
                      </View>
                      </View>
                  <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryOnce()}>
                  <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                      <Tab heading="Frequent" style={{padding:10,height:200}}>
                      <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:10,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Day"/>
                          <Picker.Item value="30 Days" label="30 Day"  />
                        </Picker>
                      </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                    <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                    <DatePicker style={{borderRadius:10}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                    <DatePicker style={{borderRadius:10}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 15}}}   date={this.state.end_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                    </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                    <TextInput  style={styles.input2} placeholder="Company" onChangeText={(vcomp)=>this.setState({vcomp})}/>
                    <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                    </Tabs>
                  </View>
                <Card>                  
                </Card>
           </Modal>
           
           <Modal isVisible={this.state.quick_actions} style={{borderRadius:50}}>
              <Card>
                <CardItem header bordered style={{backgroundColor:'#1d2839',flexDirection:'row'}}>
                  <Text style={{color:'#fff',fontSize:18}}>Allow Future Entry</Text>
                  <View style={{flex:1,alignItems:'flex-end'}}>
                  <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({quick_actions:false})}} size={22}/>
                  </View>
                </CardItem>
                <CardItem bordered>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.selectedAction('cab_tabs')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
                      <Text>Cab</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/delivery.jpg')}/>
                      <Text>Delivery</Text>
                    </TouchableOpacity>
                    <View style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5}} source={require('.././assets/guest.jpg')}/>
                      <Text>Guest</Text>
                    </View>
                    <TouchableOpacity onPress={()=>this.setState({service_list:true})} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/helper.jpg')}/>
                      <Text>Help</Text>
                    </TouchableOpacity>
                  </View>
                </CardItem>
                {this.state.service_list==true &&
                <List>
                    <ListItem onPress={() =>this.open_help('Home Repair')}>
                      <Left><Text>Home Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Appliances Repair')}>
                      <Left><Text>Appliances Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Internet Repair')}>
                      <Left><Text>Internet Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Beautician')}>
                      <Left><Text>Beautician</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Others')}>
                      <Left><Text>Others</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                  </List>}
              </Card>
           </Modal>
           
           <Modal isVisible={this.state.isVisible} style={{borderRadius:10}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff'}}>Add Family Member</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <TextInput onChangeText={(new_name)=>this.setState({new_name})} style={styles.input} placeholder="Full Name"/>
                  <View style={{flexDirection:'row',width:'80%',marginBottom:10}}>
                    <View style={{flex:8}}>
                      <Text style={{fontSize:12}}>Select Gender</Text> 
                      <View style={{borderWidth:1, borderColor:'grey'}}>
                      <Picker
                            selectedValue={this.state.gender}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ gender: itemValue})} >
                              <Picker.Item label="Select Gender" value="" />
                              <Picker.Item label="Male" value="Male" />
                              <Picker.Item label="Female" value="Female" />
                      </Picker> 
                      </View>
                      <Text style={{fontSize:12,marginTop:5}}>Select Age Group</Text> 
                      <View style={{borderWidth:1, borderColor:'grey'}}>
                      <Picker
                            selectedValue={this.state.age_group}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ age_group: itemValue})} >
                              <Picker.Item label="Age Group" value="" />
                              <Picker.Item label="18-40" value="18-35" />
                              <Picker.Item label="35-50" value="35-50" />
                              <Picker.Item label="above 50" value="above 50" />
                      </Picker> 
                      </View>
                    </View>
                    <View style={{flex:2,padding:10}}>
                    <TouchableOpacity onPress={()=>this._pickImage()}>
                    <Image style={{width:100,height:100}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.new_user_image }}/>
                    </TouchableOpacity>
                    </View>
                  </View>
                  <TextInput autoCapitalize = 'none' onChangeText={(new_email)=>this.setState({new_email})}  style={styles.input} placeholder="Enter Email"/>
            
              <TextInput keyboardType = "number-pad"
      maxLength={10} onChangeText={(new_mobile)=>this.setState({new_mobile})}  style={styles.input} placeholder="Enter Mobile Number"/>
              
                </Body>
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                <Button title="Add Member" onPress={()=>this.addnewMember()} color="#117A65"/>
                <Button title="Cancel" color="grey" style={{marginLeft:10}} onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}}/>
              </CardItem>
            </Card>
          </Modal>
          <Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
          
          <View style={{shadowColor: '#1d2839',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,shadowRadius:2,elevation:5,
          flexDirection:'row',padding:10,margin:5,borderRadius:5,backgroundColor:'#fff'}}>
            <View style={{flex:2}}>
            <Image style={{width:50, height:50,margin:5}} source={require('.././assets/user.png')}/>
            </View>
            <View style={{flex:6, margin:5}}>
            <Text style={{fontSize:18,color:'#0E6655',alignItems:'center',fontWeight:'600'}}>Hi, {this.state.name}</Text>
            <Text style={{fontSize:16,color:'#5D6D7E',alignItems:'center'}}>#000{this.state.userid}</Text>
           </View>
              
          </View>

          <View style={{flexDirection:'row',padding:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>My Family</Text>
            </View><View style={{flex:1}}>
              <Text onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}}  style={{fontSize:18,color:'#1ABC9C',textAlign:'right'}}>
              <Ionicons name="md-add" color='#1ABC9C' size={16}/> ADD</Text>
            </View>
          </View>
          {this.state.nofam &&
          <View style={{flexDirection:'row',marginTop:5,padding:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:1,borderRadius:4,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:90, height:90}} source={require('.././assets/addnew.png')}/> 
              <Text onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}} style={{fontSize:16,color:'#1ABC9C'}}>Add Member</Text>
            </View>
            <View style={{flex:2}}></View>
          </View>}
          {!this.state.nofam &&
            <View style={{flexDirection:'row',marginTop:5}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.myfam}
            renderItem={({item}) =>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            width:125,margin:5,borderRadius:5,padding:10,backgroundColor:'#fff'}}>
              <TouchableOpacity style={{alignContent:'center',alignItems:'center'}}>
                <Image style={{width:60, height:60,borderRadius:100}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
                <Text style={{fontSize:16,color:'#1ABC9C'}}>{item.name}</Text>
                <Text style={{fontSize:14,color:'grey'}}>#000{item.uid}</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row',borderTopColor:'grey',borderTopWidth:0.5,margin:5,padding:5}}>
                <View style={{flex:1,alignItems:'center'}}><Ionicons onPress={()=>{Linking.openURL('tel:'+item.mobile);}} name="md-call" color='#117A65' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-share" color='grey' size={22}/></View>
              </View>
            </View>}/></View>}

          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Daily Help</Text>
            </View><View style={{flex:1}}>
              <Text onPress={() => this.props.navigation.navigate('ListServices')}  style={{fontSize:18,color:'#F39C12',textAlign:'right'}}>
              <Ionicons name="md-add" color='orange' size={18}/> ADD</Text>
            </View>
          </View>
          {this.state.nohelp &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:10,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/help.png')}/> 
              <Text onPress={() => this.props.navigation.navigate('ListServices')}  style={{fontSize:16,color:'#1ABC9C'}}> Add Daily Help</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.nohelp &&
            <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.helps}
            renderItem={({item}) =>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            width:125,margin:5,borderRadius:5,padding:10,backgroundColor:'#FEF9E7'}}>
              <TouchableOpacity style={{alignContent:'center',alignItems:'center'}} onPress={() => this.props.navigation.navigate('WorkProfile',{hid:item.hid,htype:item.ser_type,serid:item.serid})} >
                <Image style={{width:80, height:80,borderRadius:100}} source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
                <Text style={{fontSize:16,color:'#1ABC9C'}}>{item.hname}</Text>
                <Text style={{fontSize:14,color:'grey'}}>{item.ser_type}</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row',justifyContent:'space-between',borderTopColor:'grey',borderTopWidth:0.5,margin:5,padding:5}}>
                <View style={{flex:1,alignItems:'center'}}><Ionicons onPress={()=>{Linking.openURL('tel:'+item.hphone);}} name="md-call" color='#E74C3C' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-notifications" color='grey' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-star" color='grey' size={22}/></View>
              </View>
          </View>}/></View>}
         <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Frequent Guests</Text>
            </View><View style={{flex:1}}>
              <Text style={{fontSize:18,color:'#E74C3C',textAlign:'right'}}>
              <Ionicons name="md-add" color='#E74C3C' size={18}/> ADD</Text>
            </View>
          </View>
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/addnew.png')}/> 
              <Text style={{fontSize:16,color:'#1ABC9C'}}> Add Entry</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>

          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Frequent Entries</Text>
            </View><View style={{flex:1}}>
              <Text style={{fontSize:18,color:'#5DADE2',textAlign:'right',fontWeight:'600'}}>
              <Ionicons name="md-add" color='#5DADE2' size={18}/> ADD</Text>
            </View>
          </View>
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/calendar.png')}/> 
              <Text style={{fontSize:16,color:'#1ABC9C'}}> Add Entry</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>
        </ScrollView>
        <ActionButton buttonColor="rgba(231,76,60,1)">
              <ActionButton.Item buttonColor='#9b59b6' title="Quick Actions" onPress={() =>this.setState({quick_actions:true})}>
                <Icon name="md-create" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#3498db' title="Security Alert" onPress={() => {}}>
                <Icon name="md-notifications" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#1abc9c' title="Message to Guard" onPress={() => {}}>
                <Icon name="md-mail" style={styles.actionButtonIcon} />
              </ActionButton.Item>
        </ActionButton>
          </Container>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:10},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},btnText2:{fontSize:16, textAlign:'center', color:'#000'},
  container: {
    flex: 1,
    backgroundColor: '#E8F8F5'
  },input:{width:'100%',borderRadius:2, borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
  input2:{borderRadius:10,width:'100%', borderWidth:0.5, height:40,padding:10,fontSize:16,backgroundColor:'#fff'},
});
