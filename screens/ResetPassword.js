import React from 'react';
import { StyleSheet,StatusBar,AsyncStorage,KeyboardAvoidingView,ScrollView,Button,TextInput,FlatList, Text, View,Image,TouchableOpacity  } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';

export default class ResetPassword extends React.Component {

    static navigationOptions =  ({ navigation }) => ({
        headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle: 'Update Password',
      })
 
  constructor(props){
    super(props);
     this.state={
            votp:'',mobile:'',
     }
  }

  async componentDidMount(){
    const { navigation } = this.props;
    var rec_otp=navigation.getParam('votp', '');
    var rec_mob=navigation.getParam('mobile', '');
    //alert(rec_otp);
    this.setState({votp:rec_otp})
    this.setState({mobile:rec_mob})
}


  UpdatePassword=()=>{
      if(this.state.password=="" || this.state.cpassword=="" || this.state.password!=this.state.cpassword || this.state.password.length<6){
          alert("Please Enter Valid Min 6 Character Password and Both Password must match.");
         // this.setState({errorMsg:'Please Enter Username & Password'});
        }else{
            const { navigation } = this.props;
            
          fetch('http://iglobal.in/home/update_password_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  password: this.state.password,
                  cpassword: this.state.cpassword,
                  mobile:this.state.mobile,
                  otp:this.state.votp
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
             console.log(responseJson);
            if(responseJson.status==0){
              alert(responseJson.msg);
              this.props.navigation.navigate("Login");
            }else{
              alert(responseJson.msg);
              this.props.navigation.navigate("ForgotPassword");
            }
            });
        }
  }


  

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <Image style={{width:200, height:50,margin:10}}  source={require('.././assets/logo.png')}/>
            <Text style={{fontSize:16,marginBottom:5}}>Reset to New Password</Text>
            <TextInput onChangeText={(password)=>this.setState({password})}  style={styles.input} placeholder="Enter New Password" secureTextEntry/>
            <TextInput onChangeText={(cpassword)=>this.setState({cpassword})}  style={styles.input} placeholder="Confirm New Password" secureTextEntry/>
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>this.UpdatePassword()}><Text style={styles.btnText}>Update Password</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn2}  
                onPress={()=>this.props.navigation.navigate('Login')}><Text style={styles.btnText}>Login</Text></TouchableOpacity>
                
            </View>
            </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8fafc',
    alignItems: 'center',
    justifyContent:'center'
  },
  input:{width:'80%', borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16},
  btn:{width:'60%', padding:10, backgroundColor:'#FF8E0F'},
  btn2:{width:'35%', padding:10, backgroundColor:'#FFBE0F'},
  btn3:{width:'80%', padding:10, marginTop:10, backgroundColor:'#FF3C1D'},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  points:{color:'#6C3483',fontSize:20},
  points2:{color:'#F39C12',fontSize:18,marginBottom:5,borderBottomWidth:0.5,borderBottomColor:'grey'},
  points3:{color:'#34495E',fontSize:16,marginBottom:5,fontWeight:'600'},
})
