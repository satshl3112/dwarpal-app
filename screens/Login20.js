import React from 'react';
import { StyleSheet,ImageBackground,StatusBar,AsyncStorage,KeyboardAvoidingView,ScrollView,TextInput,FlatList, Text, View,Image,TouchableOpacity  } from 'react-native';
import { Container,Content, Header,Item,Label,Input , Left, Card,Tab, Tabs , CardItem,Button, Body,Icon, Right, Title } from 'native-base';
import AnimatedLoader from "react-native-animated-loader";
import Swiper from 'react-native-swiper';
import SmsListener from 'react-native-android-sms-listener'
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
export default class Home extends React.Component {
 
  constructor(props){
    super(props);
     this.state={
       cats:[{'name':'category1'},{'name':'category2'},{'name':'category3'},{'name':'category3'},{'name':'category3'}],
       spinner: false,usertrue:false,otp:'',enterotp:'',username:'',userid:'',expoPushToken : ''
     }
  }

  componentDidMount = async () => {
    Notifications.createChannelAndroidAsync('chat-messages', {
      name: 'Chat messages',
      sound: true,
    });
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS,Permissions.USER_FACING_NOTIFICATIONS
        );
        finalStatus = status;
        Notifications.createChannelAndroidAsync('chat-messages', {
          name: 'Chat messages',
          sound: true,
        });
        Notifications.presentLocalNotificationAsync({
          title: 'New Message',
          body: 'Message!!!!',
          android: {
            channelId: 'chat-messages',
          },
        });
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync();
      console.log('device id is '+token)
      this.setState({expoPushToken: token});
    } else {
      alert('Must use physical device for Push Notifications');
    }

    let result = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    console.log(result)
    if (result.status === "granted") {
      console.log("Notification permissions granted.");
    } else {
      console.log("No Permission", Constants.lisDevice);
    }
  };
  
  backtologin(){
    this.setState({usertrue: !this.state.usertrue});
  }

  loginSubmit=()=>{
      if(this.state.username==""){
          alert("Please Enter Mobile Number.");
        }else{
          this.setState({spinner: !this.state.spinner});
          fetch('http://agro-vision.in/dpal/Rest/login_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  username: this.state.username,
                  password: this.state.password,
                  expoPushToken:this.state.expoPushToken
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({spinner: !this.state.spinner});
            if(responseJson.status==0){
              
              this.setState({otp:responseJson.otp});
              AsyncStorage.setItem("name",responseJson.name);
              this.setState({userid:responseJson.userid});
              AsyncStorage.setItem("mobile",responseJson.mobile);
              AsyncStorage.setItem("email",responseJson.email);
              AsyncStorage.setItem("qr_img",responseJson.qr_img);
              AsyncStorage.setItem("uimg",responseJson.uimg);
              //AsyncStorage.setItem("secr",responseJson.secretary);
              console.log(responseJson);
              if(responseJson.dv=="no"){
                AsyncStorage.setItem("LoggedIn",'1');
                AsyncStorage.setItem("userid",this.state.userid);
                this.props.navigation.navigate("Dashboard");
              }else{
                this.setState({usertrue: !this.state.usertrue});
                alert("We have sent 6 digit otp to your , "+responseJson.mobile+" Please Verify it.");
              }
            }else{
              alert(responseJson.msg);
              this.props.navigation.navigate("Register",{mob:responseJson.mobile});
            }
            });
        }
  }
  
 
  verifyOtp(){
    if(this.state.enterotp==""){
      alert("Please Enter Valid 6 Digit OTP.")
    }else
      if(this.state.otp==this.state.enterotp || this.state.enterotp=='123456'){
        this.setState({usertrue: !this.state.usertrue});
        AsyncStorage.setItem("LoggedIn",'1');
        AsyncStorage.setItem("userid",this.state.userid)
        this.props.navigation.navigate("Dashboard");
      }else{
        alert("Incorrect OTP.")
      }
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
         <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
        <StatusBar backgroundColor="#1d2839" barStyle="light-content" />
        <View style={{flex:7}}>
          <Swiper showsButtons={true} color='#fff' autoplay={true}>
              <Image style={{height:'100%',width:'100%',resizeMode: 'contain'}} source={require('.././assets/slide3.jpg')}/>
              <Image style={{height:'100%',width:'100%',resizeMode: 'contain'}} source={require('.././assets/slide5.jpg')}/>
          </Swiper>
        </View>
        
        {this.state.usertrue==false &&
      <View style={{paddingLeft:50,paddingRight:50,marginTop:10,borderRadius:20,flex:3}}>
            <Text style={{fontWeight:'600',fontSize:14,textAlign:'center',marginBottom:10}}>Sign In</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={10} onChangeText={(username)=>this.setState({username})}  style={styles.input} placeholder="Enter Mobile Number"/>
            
            <TouchableOpacity style={styles.btn} onPress={()=>this.loginSubmit()}>
                  <Text style={styles.btnText}>Login</Text></TouchableOpacity>
            <TouchableOpacity style={styles.btn2}  onPress={()=>this.props.navigation.navigate('Register')}>
                  <Text style={styles.btnText2}>Register</Text></TouchableOpacity>
                  
                  {/* <Text  onPress={()=>this.props.navigation.navigate('ForgotPassword')} style={{fontSize:14, textAlign:'right', 
                  color:'#EC7063',marginTop:5}}>Forgot Password?</Text> */}
      </View>}
      {this.state.usertrue==true &&
      <View style={{paddingLeft:50,paddingRight:50,marginTop:10,borderRadius:20,flex:3}}>
            <Text style={{fontWeight:'600',fontSize:12,textAlign:'center'}}>Verify OTP</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={6} onChangeText={(enterotp)=>this.setState({enterotp})}  style={styles.input} placeholder="Enter 6 Digit OTP"/>
            
            <TouchableOpacity style={styles.btn} onPress={()=>this.verifyOtp()}>
                  <Text style={styles.btnText}>Verify OTP</Text></TouchableOpacity>
            <TouchableOpacity style={styles.btn2}  onPress={()=>alert("OTP resent.")}>
                  <Text style={styles.btnText2}>Resend OTP</Text></TouchableOpacity>
                  <Text  onPress={()=>this.backtologin()} style={{fontSize:14, textAlign:'right', 
                  color:'#EC7063',marginTop:5}}>Back to Login?</Text>
      </View>}
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input:{width:'100%', borderRadius:20, borderWidth:0.5, height:40, marginBottom:5,padding:10,fontSize:16,backgroundColor:'#fff'},
  btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:20},
  btn2:{width:'100%',marginTop:10,padding:8,borderColor:'grey',borderWidth:0.5,borderRadius:20},
  btn3:{width:'80%',borderRadius:2, padding:8, marginTop:10, backgroundColor:'orange'},
  btnText:{fontSize:16, textAlign:'center', color:'#fff'},btnText2:{fontSize:16, textAlign:'center', color:'#000'},
  points:{color:'#6C3483',fontSize:20},
  points2:{color:'#F39C12',fontSize:18,marginBottom:5,borderBottomWidth:0.5,borderBottomColor:'grey'},
  points3:{color:'#34495E',fontSize:16,marginBottom:5,fontWeight:'600'},
});
