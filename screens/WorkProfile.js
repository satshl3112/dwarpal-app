
import React from 'react';
import { StyleSheet,RefreshControl,Text,Picker,Alert,ScrollView,Linking,AsyncStorage,Image,ActivityIndicator,TouchableOpacity,View,TextInput,
  FlatList,Dimensions,Button } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Header, Content,Card,CardItem, List, ListItem,Thumbnail,Left,Right,Body,Textarea } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import * as Font from 'expo-font';
import DatePicker from 'react-native-datepicker';
import Modal from "react-native-modal";
import StarRating from 'react-native-star-rating';


export default class WorkProfile extends React.Component {
    static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Profile Details',
    })

    constructor(props){
        super(props);
        this.state={ spinner:false,himg:'',name:'',htype:'',helperid:'',btn_title:'Add Help',serid:'',time_slots:[],flat_id:'',
        start_time:'',end_time:'',vModal:false,reviews:[],noRev:false,rModal:false,starCount: 0,review:'',
        punctual:1,regular:1,excep_ser:1,attitute:1,avrg:'',totr:'',qrmodal:false,full_present:'',flat_present:''
        }
    }

    onStarRatingPress(rating) {
      this.setState({
        starCount: rating
      });
    }
    regular(){
      if(this.state.regular==1){
        this.setState({regular:0});
      }else{
        this.setState({regular:1});
      }
    }
    attitute(){
      if(this.state.attitute==1){
        this.setState({attitute:0});
      }else{
        this.setState({attitute:1});
      }
    }
    excep_ser(){
      if(this.state.excep_ser==1){
        this.setState({excep_ser:0});
      }else{
        this.setState({excep_ser:1});
      }
    }
    punctual(){
      if(this.state.punctual==1){
        this.setState({punctual:0});
      }else{
        this.setState({punctual:1});
      }
    }
    reverseDate(d){
      var d=d.split("-").reverse().join("-");
      return d;
    }
    
    async pageRefresh(){
        const { navigation } = this.props;
        const hid = navigation.getParam('hid', '0');
        var socid=await AsyncStorage.getItem('society');
        const urlt='http://agro-vision.in/dpal/Rest/time_slots_api/'+hid+'/'+socid;
        fetch(urlt).then((response)=>response.json())
        .then((responseJson)=>{
          //console.log(responseJson)
          this.setState({time_slots:responseJson});
        }).catch((error)=>{ console.log(error)});

        const urltt='http://agro-vision.in/dpal/Rest/helper_review/'+hid;
        fetch(urltt).then((response)=>response.json())
        .then((responseJson)=>{
          //console.log(responseJson)
          if(responseJson.status==1){
            //this.setState({noRev:false})
            this.setState({reviews:responseJson.result})
          }//else{this.setState({noRev:true})}
        }).catch((error)=>{ console.log(error)} );

        var flatid = await AsyncStorage.getItem('flat');
        this.setState({flat_id:flatid});
        this.setState({htype:navigation.getParam('htype', '')});
        this.setState({helperid:navigation.getParam('hid', '0')});
        this.setState({serid:navigation.getParam('serid', '0')});
        const url2='http://agro-vision.in/dpal/Rest/workprofile_api/'+hid+'/'+flatid+'/'+navigation.getParam('serid', '0')+'/'+socid;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          //this.setState({spinner: !this.state.spinner});  
          this.setState({himg:responseJson.himg});
          this.setState({name:responseJson.hname});
          this.setState({hphone:responseJson.hphone});
          this.setState({avrg:responseJson.avrg});
          this.setState({totr:responseJson.totr});
          this.setState({full_present:responseJson.full_present});
          this.setState({flat_present:responseJson.flat_present});
          if(responseJson.rev>0){
            this.setState({noRev:false})
          }
          if(responseJson.is_work>0){
            this.setState({noRev:false})
          }if(responseJson.is_work>0 && responseJson.rev==0){
            this.setState({noRev:true})
          }else{this.setState({noRev:true})}
          //this.setState({btn_title:responseJson.btn_title});
        }).catch((error)=>{ console.log(error)} );
    }

    async componentDidMount(){
        await Font.loadAsync({
          'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
        });
        this.setState({ assetsLoaded: true });
        this.pageRefresh();
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    removeMyhelp(id){
      Alert.alert(
        'Confirm Action',
        'Sure you want to Cancel leave request?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
              const url='http://agro-vision.in/dpal/Rest/removeMyhelp/'+id;
              fetch(url).then((response)=>response.json())
              .then((responseJson)=>{
                  console.log(responseJson);
                  alert(responseJson.msg);
                  this.pageRefresh();
              }).catch((error)=>{
                console.log(error)
              })
            }
          },
        ],
        {cancelable: false},
      );
    }

    async addTomyHelp(){
      var flatid = await AsyncStorage.getItem('flat');
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/addhelp_toMyFlat_api/', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              flat: flatid,userid: await AsyncStorage.getItem('userid'),
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              hid: this.state.helperid,from_time:this.state.start_time,to_time:this.state.end_time,serid:this.state.serid
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({vModal:false})
          alert(responseJson.msg);
          this.pageRefresh();
        });
    }

    async addtoMyFlat(htid){
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/addhelp_toMyFlat_api/'+htid, {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              start_time:this.state.start_date+' '+this.state.start_time,
              ocomp:this.state.ocomp
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false});
          alert(responseJson.msg);
        });
    }
    
    async addReview(){
      if(this.state.review=="" || this.state.starCount==0){alert("Please write your review and give rating.");return false;}
      const { navigation } = this.props;
      const helperid = navigation.getParam('hid', '0');
      var flatid = await AsyncStorage.getItem('flat');
      //alert( this.state.starCount)
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/add_review_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  stars: this.state.starCount,flatid:flatid,helperid:helperid,
                  punctual: this.state.punctual,regular: this.state.regular,excep_ser: this.state.excep_ser,
                  attitute: this.state.attitute,review: this.state.review,
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              console.log(responseJson)
              this.setState({spinner: !this.state.spinner});
              alert(responseJson.msg);this.setState({rModal:false}); 
              this.pageRefresh();             
            });
    }

    starshow(rating){
      var star=''
      for(i=0;i<rating;i++){
      star+='<Ionicons name="md-star" size={20} color="orange"/>';
      }return star;
    }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
          <Container>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
          <Modal style={{padding:10}} isVisible={this.state.qrmodal}>
            <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({qrmodal:false})}} size={26}/>
            
            <Image style={{width:'100%', height:300}}  source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+this.state.himg}}/>
            
           </Modal>
          <Modal isVisible={this.state.vModal} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Add to Your Flat</Text>
              </CardItem>
              <Text style={{fontSize:14,color:'grey',margin:10}}>Select Allowed TimeSlot</Text>
                      <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between',margin:10}}>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
              </View>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
              <TouchableOpacity style={{backgroundColor:'grey',padding:8,borderRadius:5}} onPress = {() => { this.setState({ vModal:!this.state.vModal})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:'#117A65',padding:8,borderRadius:5}} onPress={()=>this.addTomyHelp()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Add</Text></TouchableOpacity>
                
              </CardItem>
            </Card>
          </Modal>
          <Modal isVisible={this.state.rModal} style={{borderRadius:20}}>
            <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>Rate & Review {this.state.name}</Text>
              </CardItem>
              <CardItem style={{justifyContent:'center'}}>
                <StarRating fullStarColor="orange" emptyStarColor="orange" disabled={false} maxStars={5} rating={this.state.starCount} 
                  selectedStar={(rating) => this.onStarRatingPress(rating)}/>
              </CardItem>
              <View style={{padding:10,}}>
              <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                <TouchableOpacity onPress={()=>this.punctual()} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  {this.state.punctual==1 &&
                  <Image style={{height:80,width:80,borderRadius:50}}  source={require('.././assets/panctual-0.jpg')}/>}
                  {this.state.punctual==0 &&
                  <Image style={{height:80,width:80}}  source={require('.././assets/panctual-1.jpg')}/>}
                  <Text>Very Punctual</Text>
                </TouchableOpacity>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={()=>this.regular()} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  {this.state.regular==1 &&
                  <Image style={{height:80,width:80,borderRadius:50}}  source={require('.././assets/regular-0.jpg')}/>}
                  {this.state.regular==0 &&
                  <Image style={{height:80,width:80}}  source={require('.././assets/regular-1.jpg')}/>}
                  <Text>Quite Regular</Text>
                </TouchableOpacity>
                </View>
              </View>
              <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                <TouchableOpacity onPress={()=>this.excep_ser()} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  {this.state.excep_ser==1 &&
                  <Image style={{height:80,width:80,borderRadius:50}}  source={require('.././assets/excep_ser-0.png')}/>}
                  {this.state.excep_ser==0 &&
                  <Image style={{height:80,width:80}}  source={require('.././assets/excep_ser-1.jpg')}/>}
                  <Text>Exceptional Service</Text>
                </TouchableOpacity>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity onPress={()=>this.attitute()} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  {this.state.attitute==1 &&
                  <Image style={{height:80,width:80,borderRadius:50}}  source={require('.././assets/attitute-0.jpg')}/>}
                  {this.state.attitute==0 &&
                  <Image style={{height:80,width:80}}  source={require('.././assets/attitute-1.jpg')}/>}
                  <Text>Great Attitude</Text>
                </TouchableOpacity>
                </View>
              </View>
              
              <Textarea   onChangeText={(review)=>this.setState({review})}  
                style={{width:'100%', marginBottom:5,padding:3,marginTop:10}} rowSpan={3} bordered placeholder="Type Your Review Here" />
              </View>
             <CardItem footer bordered style={{justifyContent:'space-between'}}>
              <TouchableOpacity style={{backgroundColor:'grey',padding:8,borderRadius:5}} onPress = {() => { this.setState({ rModal:!this.state.rModal})}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Cancel</Text></TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:'#117A65',padding:8,borderRadius:5}} onPress={()=>this.addReview()}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:16}}>Add Review</Text></TouchableOpacity>
              </CardItem>
            </Card>
          </Modal>
          <View style={styles.header}></View>
              <Image style={styles.avatar}   source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+this.state.himg}}/>
              <View style={styles.body}>
                <View style={styles.bodyContent}>
                  <Text style={styles.name}>{this.state.name}</Text>
                  <Text style={styles.info}>{this.state.htype}</Text>
              </View>
            </View>
            <View style={{flexDirection:'row',padding:10,borderTopWidth:0.5,borderTopColor:'grey',
            borderBottomWidth:0.5,borderBottomColor:'grey',backgroundColor:'#E8F8F5'}}>
              <View style={{flex:1,alignItems:'center'}}>
                <Text style={{fontSize:20}}><Ionicons name="md-star" size={20} color='#F1C40F'/> {this.state.avrg}</Text>
                <Text style={{fontSize:14,color:'grey',fontFamily:'custom-fonts',}}>{this.state.totr} Review </Text>
              </View>
              <View style={{flex:1,alignItems:'center',borderLeftColor:'grey',borderLeftWidth:0.5}}>
                <Text style={{fontSize:20}}><Ionicons name="md-calendar" size={20} color='#F39C12'/> {this.state.flat_present}/{this.state.full_present}</Text>
                <Text onPress={()=>this.props.navigation.navigate('HelperAttend',{helperid:this.state.helperid})} style={{fontSize:14,color:'grey',fontFamily:'custom-fonts',}}>Attendance </Text>
              </View>
              <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+this.state.hphone)}} style={{flex:1,alignItems:'center',borderLeftColor:'grey',borderLeftWidth:0.5,marginTop:4}}>
                <Text style={{fontSize:20}}><Ionicons name="md-call" size={20} color='#73C6B6'/></Text>
                <Text style={{fontSize:14,color:'grey',fontFamily:'custom-fonts',}}>Call</Text>
              </TouchableOpacity>
            </View>
            <Content style={{padding:5}}>
            <Card>
              <CardItem style={{flexDirection:'row',justifyContent:'space-between'}}>
                
                <TouchableOpacity style={{backgroundColor:'#117A65',padding:8,borderRadius:20}} 
                onPress={()=>this.setState({ vModal:!this.state.vModal})}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:14}}>Add to My Flat</Text></TouchableOpacity>
                
                  <TouchableOpacity style={{backgroundColor:'#27AE60',padding:8,borderRadius:20}}>
                  <Text style={{fontFamily:'custom-fonts',color:'#fff',fontSize:14}}>Pay Salary</Text></TouchableOpacity>
              </CardItem>
            </Card>
            <FlatList data={this.state.time_slots} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
                <CardItem  style={{flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flex:1}}><Text>{item.from_time} - {item.to_time}</Text></View>
                {item.wflat_id=="" &&
                <TouchableOpacity onPress={()=>this.addtoMyFlat(item.htid)} 
                style={{backgroundColor:'#27AE60',padding:5,borderRadius:5}}>
                  <Text style={{color:'#fff'}}>Add to My Flat</Text>
                </TouchableOpacity>}
                {item.wflat_id==this.state.flat_id && 
                <View style={{flex:1,alignItems:'flex-end'}}>
                  {item.req_status=="Requested" &&
                  <Text style={{color:'#EC7063'}}>Pending Approval</Text>}
                  {item.req_status=="Approved" &&
                  <TouchableOpacity onPress={()=>this.removeMyhelp(item.htid)} 
                  style={{backgroundColor:'#EC7063',padding:5,borderRadius:5}}>
                    <Text style={{color:'#fff'}}>Remove</Text>
                  </TouchableOpacity>}
                </View>}
                {item.wflat_id!=this.state.flat_id && item.wflat_id!="" && 
                <View style={{flex:1,alignItems:'flex-end'}}>
                  <Text style={{color:'orange'}}>Flat No. {item.flat_no} {item.block_name}</Text>
                </View>}
                </CardItem>
            </Card>}/>
            <FlatList  keyExtractor={(item, index) => index.toString()} 
            data={this.state.reviews}
            renderItem={({item}) =>
            <Card style={{padding:5,marginRight:5}}>
              <CardItem style={{flexDirection:'row',justifyContent:'space-between'}}>
                <View style={{flexDirection:'row'}}>
                  {item.rating>4 && <Ionicons name="md-star" size={20} color="orange"/>}
                  {item.rating>3 && <Ionicons name="md-star" size={20} color="orange"/>}
                  {item.rating>2 && <Ionicons name="md-star" size={20} color="orange"/>}
                  {item.rating>1 && <Ionicons name="md-star" size={20} color="orange"/>}
                  {item.rating>0 && <Ionicons name="md-star" size={20} color="orange"/>}
                </View>
                <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                  {item.panctual==0 &&
                  <Image style={{height:20,width:20,borderRadius:50}}  source={require('.././assets/panctual-0.jpg')}/>}
                  {item.regular==0 &&
                  <Image style={{height:20,width:20,borderRadius:50}}  source={require('.././assets/regular-0.jpg')}/>}
                  {item.excep_ser==0 &&
                  <Image style={{height:20,width:20,borderRadius:50}}  source={require('.././assets/excep_ser-0.png')}/>}
                  {item.attitute==0 &&
                  <Image style={{height:20,width:20,borderRadius:50}}  source={require('.././assets/attitute-0.jpg')}/>}
                </View>
              </CardItem>
              <Body>
              <Text style={{fontFamily:'custom-fonts'}}>{item.review}</Text>
              </Body>
              <CardItem style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={{fontFamily:'custom-fonts'}}>{this.reverseDate(item.rev_date)}</Text>
                  {/* <TouchableOpacity onPress = {() => { this.setState({ rModal:!this.state.rModal})}}>
                    <Text style={{color:'#1ABC9C',fontWeight:'600',fontFamily:'custom-fonts'}}>Edit</Text></TouchableOpacity> */}
              </CardItem>
            </Card>
            }/>
            {this.state.noRev==true &&
            <Card>
              {/* <CardItem header><Text>Review & Ratings</Text></CardItem> */}
              <Body style={{alignContent:'center',padding:10}}>
                <Image style={{height:100,width:100}}  source={require('.././assets/review.png')}/>
                {/* <Text>Be the first to review this.</Text> */}
                <TouchableOpacity style={styles.btn} onPress = {() => { this.setState({ rModal:!this.state.rModal})}}>
                  <Text style={styles.btnText}>Write Your Review</Text></TouchableOpacity>  
              </Body>
            </Card>}
            
           
            
        </Content></ScrollView>
        </Container>
      );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
      }
  }
}

const styles = StyleSheet.create({
    header:{
      backgroundColor: "#1d2839",
      height:100,
    },
    btn:{borderRadius:20,width:'100%', padding:8, backgroundColor:'#25be9f'},
    btnText:{fontSize:16, textAlign:'center', color:'#fff'},
    avatar: {
      width: 130,
      height: 130,
      borderRadius: 63,
      borderWidth: 4,
      borderColor: "white",
      marginBottom:10,
      alignSelf:'center',
      position: 'absolute',
      marginTop:30
    },
    name:{
      fontSize:20,fontFamily:'custom-fonts',
    },
    body:{
      marginTop:30,
    },
    bodyContent: {
      flex: 1,
      alignItems: 'center',
      padding:30,
    },
    info:{
      fontSize:14,fontFamily:'custom-fonts',
      color: "grey",
    },
    description:{
      fontSize:16,
      color: "#696969",fontFamily:'custom-fonts',
      marginTop:10,
      textAlign: 'center'
    },
    buttonContainer: {
      marginTop:10,
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
  });