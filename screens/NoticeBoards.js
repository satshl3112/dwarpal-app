import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,RefreshControl,KeyboardAvoidingView,CheckBox,ActivityIndicator,
  TextInput,Picker,Button,Alert,FlatList,TouchableOpacity,BackHandler } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import { Container,Content, Header, Left, Card,Textarea, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import { HeaderBackButton } from 'react-navigation-stack';
import MultiSelect from 'react-native-multiple-select';
import DatePicker from 'react-native-datepicker';
import * as Font from 'expo-font';

const day1 = new Date();
const day15=new Date(new Date().getTime()+(15*24*60*60*1000));
const day30=new Date(new Date().getTime()+(30*24*60*60*1000));
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = dd + '-' + mm + '-' + yyyy;

export default class NoticeBoards extends React.Component {
  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Notice Boards'})
  

    constructor(props){
        super(props);
        this.state={subject:'',rtype:'',notice:'',flats:[],start_date:'',notices:[],selected_flats:[],spinner:false,is_secr:null,
                    selected_flats:[],start_date:today,}
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    selected_flats(){
        
    }
   
    async componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
      this.setState({is_secr:await AsyncStorage.getItem('is_secr')})
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      this.pageRefresh();
    }

    handleBackPress = async () => {
        this.props.navigation.goBack()
        return true;
    }

    async pageRefresh(){
        var flatid = await AsyncStorage.getItem('flat');
        var society=await AsyncStorage.getItem('society');
        this.setState({spinner: true}); 
        const url2='http://agro-vision.in/dpal/Rest/notices_api/'+society;
        fetch(url2).then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: false}); 
            this.setState({notices:responseJson})
        }).catch((error)=>{
            console.log(error)
        });
    }

    async add_notice_api(){
      if(this.state.subject=="" || this.state.notice==""){alert("Please Enter Subject & Notice.");return false;}
        this.setState({spinner: !this.state.spinner});
        fetch('http://agro-vision.in/dpal/Rest/add_notice_api/', {
            method: 'POST',
            headers: {Accept: 'application/json','Content-Type': 'application/json'},
            body: JSON.stringify({
                userid: await AsyncStorage.getItem('userid'),start_date: this.state.start_date,rtype:this.state.rtype,
                subject: this.state.subject,notice: this.state.notice,society:await AsyncStorage.getItem('society'),
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            console.log(responseJson)
            this.setState({spinner: !this.state.spinner});
            this.pageRefresh();
            alert(responseJson.msg);
        });
    }

    async get_residents_list(rtype){
        this.setState({rtype:rtype})
        this.setState({spinner: !this.state.spinner});
        var socid=await AsyncStorage.getItem('society');
        const url3='http://agro-vision.in/dpal/Rest/get_residents_list_api/'+socid+'/'+this.state.rtype;
        console.log(url3)
        fetch(url3).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({flats:responseJson});
        }).catch((error)=>{ console.log(error)} );
    }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <Container>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} 
          onRefresh={this._onRefresh.bind(this)}/>}>
              <Content>
              {this.state.is_secr!=null &&
              <View>
                <Card>
                  <CardItem header bordered style={{backgroundColor:'orange'}}>
                    <Text>Add New Notice</Text></CardItem>
                  <View style={{padding:10}}>
                    <TextInput style={styles.input}  
                    onChangeText={(subject)=>this.setState({subject})} placeholder="Enter Subject"/>
                    <Textarea value={this.state.notice}  onChangeText={(notice)=>this.setState({notice})}  
                        style={{width:'100%', borderRadius:20,marginBottom:10,marginTop:10,padding:3,borderWidth:0.5}}
                        rowSpan={3} bordered placeholder="Type Your Notice Here" />
                    <View style={{flexDirection:'row'}}>
                      <View style={{flex:1}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Notice Expire Date</Text>
                    <DatePicker  style={{borderRadius:20,marginBottom:5,width:'95%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm" cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              
                      </View>
                      <View style={{flex:1}}>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Users</Text>
                      <View style={{borderWidth:1, borderColor:'grey',borderRadius:20}}>
                        <Picker selectedValue={this.state.rtype}
                                style={{ width: '100%',height:38 }}
                                onValueChange={(itemValue) => this.get_residents_list(itemValue)} >
                                <Picker.Item label="Resident Type" value="" />
                                <Picker.Item label="My Assigned" value="My Assigned" />
                                <Picker.Item label="All Residents" value="All Residents" />
                        </Picker> 
                    </View>
                      </View>
                    </View>
                    
                    
                  </View>
                </Card>
                <FlatList data={this.state.flats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                    <Card>
                    <CardItem style={{flexDirection:'row'}}>
                        <View style={{flex:1,marginRight:10,}}>
                        <Image style={{width:50,height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
                        </View>
                        <View style={{flex:4}}>
                        <Text style={{fontSize:18,color:'#17A589'}}>{item.name}</Text>
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                        <Text style={styles.badge}>{item.block_name} Flat No {item.flat_no}</Text>
                        {item.is_secr!="" &&
                        <Text style={{color:'orange'}}>Secretary</Text>}
                        </View>
                        </View>
                    </CardItem>{this.state.is_secr!=null &&
                    <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                        <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.mobile);}}>
                            <Text style={{fontSize:16,color:'#1E8449'}}>
                                <Ionicons name="md-call" color='#1E8449'size={18}/> Call</Text></TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={{fontSize:16,color:'#EC7063'}}>
                                <Ionicons name="md-trash" color='#EC7063'size={18}/> Remove</Text></TouchableOpacity>
                                <TouchableOpacity>
                            <Text style={{fontSize:16,color:'green'}}>
                                <Ionicons name="md-eye" color='green'size={18}/> View</Text></TouchableOpacity>
                    </CardItem>}
                    </Card>
                }/>
                <Card>
                    <CardItem>
                    <TouchableOpacity style={{backgroundColor:'#17A589',padding:10,borderRadius:20}} onPress={()=>this.add_notice_api()}>
                        <Text style={{color:'#fff',fontSize:16,textAlign:'center',fontFamily:'custom-fonts'}}>Add Notice</Text>
                    </TouchableOpacity>
                    </CardItem>
                </Card>
              </View>}
                <FlatList data={this.state.notices} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                    <Card>
                        <CardItem bordered style={{backgroundColor:'#EC7063'}}>
                          <Text style={{color:'#fff',fontFamily:'custom-fonts'}}>{item.notice_subj}</Text></CardItem>
                        <CardItem>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.notice}</Text>
                        </CardItem>
                        <CardItem style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#F2F3F4'}}>
                          <Text style={{fontFamily:'custom-fonts'}}>Posted on {item.nfrom_date}</Text>
                          <Text style={{fontFamily:'custom-fonts'}}>By {item.name}</Text>
                        </CardItem>
                    </Card>
                }/>
              </Content>
            </ScrollView>
          </Container>
        </KeyboardAvoidingView>
        );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',justifyContent:'center'
  },input:{width:'100%',borderRadius:20,
  borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
})
