import React from 'react';
import { StyleSheet, Text,AsyncStorage, TouchableOpacity,View,FlatList,Dimensions,Button,Textinput } from 'react-native';
import { Container,Content,Header, Left, Label,Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

export default class Withdraw extends React.Component {
  constructor(props){
    super(props);
    this.state={ wallet:'0',dataSource:[],withdrawAmt:''
    }
  }

  async withdrawSubmit(){
    if(this.state.withdrawAmt<1000){
      alert("Please Enter Min Amount 1000 rs.");return false;
    }
    var userid = await AsyncStorage.getItem('userid');
    fetch('http://iglobal.in/home/withdraw_submit_api/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                withdrawAmt:this.state.withdrawAmt
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              if(responseJson.wid==0){
                alert(responseJson.msg);
              }else{
                alert("WithDrawl Requested Successfully.");
                this.setState({dataSource:responseJson});
                this.setState({withdrawAmt:''})
              }
              //console.log(responseJson);
              ///alert(responseJson.msg);

            //if(responseJson.status==0){
              //alert(responseJson.msg);
              //this.props.navigation.navigate("LiveTrades");
              //this.setState({ isVisible:!this.state.isVisible});
            //}
    });
  }
  
  async componentDidMount(){
        var userid = await AsyncStorage.getItem('userid');
        const wurl='http://iglobal.in/home/wallet_api/'+userid;
        fetch(wurl).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({wallet:responseJson.wallet})
        }).catch((error)=>{
          console.log(error)
        });
        const wurl2='http://iglobal.in/home/withdrawls_api/'+userid;
        fetch(wurl2).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({dataSource:responseJson})
          
        }).catch((error)=>{
          console.log(error)
        })
  }

    render() {
      return (
          <Container>
            <Header style={{backgroundColor:'#F39C12',marginTop:-10}}>
                <Text style={{color:'#fff',fontSize:18,marginTop:20}}>Available Balance - <Ionicons name="md-wallet" size={18}/> {this.state.wallet}/-</Text>        
            </Header>
            <Content>
              <Card>
                <CardItem><Text style={{textAlign:'justify'}}>
                Please withdraw minimum amount of 1000 rs. Below 1000 rs withdrawl will not be accepted. Withdrawl amount will be transferred to customer bank account which is linked with us. Once we receive withdrawl request your WITHDRAWL 
                order will be processed before 48 hours. Our processing hours is between 10 AM to 7 PM on weekdays (Mon-Fri).</Text>
                </CardItem>
              </Card>
              <Card>
                <Body style={{ flexDirection: "row",padding:10 }}>
                  <Text style={{fontSize:18,color:'orange'}}>Withdraw Amount</Text>
                  <Item regular style={{ flex: 1,height:35,width:100 }}>
                    <Input  keyboardType = "number-pad"
    maxLength={5} onChangeText={(amt)=>this.setState({withdrawAmt:amt})} />
                  </Item>
                  <Button style={{ flex: 1}} onPress={()=>this.withdrawSubmit()} title="Withdraw"/>
                </Body>
              </Card>
              <Card>
                <CardItem header>
                  <Text style={{fontSize:16,color:'orange'}}>Previous WithDrawls</Text>
                </CardItem>
                <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderBottomColor:'orange',borderBottomWidth:0.5}}>
                  <Text>Date</Text>
                  <Text>Amount</Text>
                  <Text>Status/Remarks</Text>
                </CardItem>
                <FlatList data={this.state.dataSource}    keyExtractor={(item, index) => index.toString()} 
					renderItem={({item})=>
					<CardItem style={{flexDirection:'row',justifyContent:'space-between',borderBottomColor:'orange',borderBottomWidth:0.5}}>
                  <Text>{item.req_date}</Text>
                  <Text>{item.wamt}/-</Text>
                  <Text>{item.req_status} {item.remarks}</Text>
                </CardItem>}/> 
              </Card>
            </Content>
          </Container>
    );
  }
}
