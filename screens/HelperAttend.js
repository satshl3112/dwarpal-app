import React from 'react';
import { StyleSheet,ScrollView, Text,View,Image,AsyncStorage,RefreshControl,KeyboardAvoidingView,CheckBox,
  TextInput,Picker,Button,Alert,FlatList,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import { Container,Content, Header, Left, Card,Textarea, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';
import { HeaderBackButton } from 'react-navigation-stack';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';


var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();


export default class HelperAttend extends React.Component {
  static navigationOptions =  ({ navigation }) => ({
    headerTitleStyle: {color:'white'},
    headerStyle: {backgroundColor:'#25be9f'},
    headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:'Attendance'})
  
    constructor(props){
        super(props);
        this.state={helperid:'',marked_dates:'',marked: null,selected_dates:[]
      }
    }

    async componentDidMount(){
        const { navigation } = this.props;
        const hid = navigation.getParam('helperid', '0');
        this.setState({helperid:hid});
        var socid=await AsyncStorage.getItem('society');
        const urlt='http://agro-vision.in/dpal/Rest/get_attendance_api/1';
        fetch(urlt).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({selected_dates:responseJson})
          this.atten_recheck();
        }).catch((error)=>{ console.log(error)} );
        
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    async atten_recheck(){
        var flat_id=await AsyncStorage.getItem('flat');
        var society=await AsyncStorage.getItem('society');
        //const urlt11='http://agro-vision.in/dpal/Rest/get_attendance_api/1';//+this.state.helperid;
        fetch('http://agro-vision.in/dpal/Rest/get_attendance_api/1', {
                          method: 'POST',
                          headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                              flat_id: flat_id,society:society,
                              hid: this.state.helperid,
                          }),
                        }).then((response)=>response.json()).then((responseJson)=>{
                          console.log(responseJson)
                          
                          this.setState({selected_dates:responseJson});
                          var obj = this.state.selected_dates.reduce((c, v) => Object.assign(c, {[v]: {selected: true}}), {});
                          this.setState({ marked : obj});
        }).catch((error)=>{ console.log(error)} );
    }

    async mark_calendar(ndate=false){
      if(ndate){
        var flat_id=await AsyncStorage.getItem('flat');
        var society=await AsyncStorage.getItem('society');
        Alert.alert(
          'Confirm Action',
          'Sure you want to mark the staff absent for '+ndate+'?',
          [
            {
              text: 'No',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'Yes', 
              onPress:()=>{
                        //const url='http://agro-vision.in/dpal/Rest/hflat_absent/'+this.state.helperid+'/'+;
                        fetch('http://agro-vision.in/dpal/Rest/hflat_absent', {
                          method: 'POST',
                          headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                              flat_id: flat_id,society:society,
                              hid:this.state.helperid,
                              absent_date:ndate
                          }),
                        }).then((response)=>response.json()).then((responseJson)=>{
                        console.log(responseJson);alert(responseJson.msg)
                        this.atten_recheck();
                    }).catch((error)=>{
                      console.log(error)
                })
              }
            },
          ],
          {cancelable: false},
        );
        // if(this.state.selected_dates.includes(ndate)==true){
        //   var index = this.state.selected_dates.indexOf(ndate);
        //   if (index !== -1) this.state.selected_dates.splice(index, 1);
        // }else{
        //   this.state.selected_dates.push(ndate)
        //   console.log(this.state.selected_dates)
        // }
      }
      
    }

    render() {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <Container>
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Content>
                <Card>
                  <CardItem><Text>Mark Days</Text></CardItem>
                  <Calendar markingType={'custom'} onDayPress={(day) =>this.mark_calendar(day.dateString)} 
                   minDate={'2012-05-10'} maxDate={today} style={[styles.calendar, {height: 500}]} 
                   markedDates={this.state.marked} 
                   theme={{textDayFontSize:18,textMonthFontSize: 18,textDayHeaderFontSize: 16,selectedDayBackgroundColor: 'green',}}/>
                </Card>
              </Content>
            </ScrollView>
          </Container>
        </KeyboardAvoidingView>
        );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',justifyContent:'center'
  }
})
