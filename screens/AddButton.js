import React from 'react';
import { Linking,StyleSheet,Share,BackHandler,ActivityIndicator,WebView,Text,Alert,ScrollView,RefreshControl,CheckBox,Image, View,Picker,KeyboardAvoidingView,TextInput,FlatList,Button,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container,Content, Header, Left, Card,Form,CardItem,List,ListItem, Body,Icon, Right, Title,Fab,Tabs,Tab } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import DatePicker from 'react-native-datepicker';
import AnimatedLoader from "react-native-animated-loader";
import Modal from "react-native-modal";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import MultiSelect from 'react-native-multiple-select';
import * as Font from 'expo-font';

const day1 = new Date();
const day15=new Date(new Date().getTime()+(15*24*60*60*1000));
const day30=new Date(new Date().getTime()+(30*24*60*60*1000));
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = dd + '-' + mm + '-' + yyyy;
var cabtypes=[
  {label:'Once', value:'Once'},
  {label:'Frequent', value:'Frequent'}
]
var items = [{id: 'Sunday',name: 'Sunday'}, {id: 'Monday',name: 'Monday'},{id: 'Tuesday',name: 'Tuesday'},
{id: 'Wednesday',name: 'Wednesday',},{id: 'Thursday',name: 'Thursday'},{id: 'Friday',name: 'Friday'},
{id: 'Saturday',name: 'Saturday'}];

notifyUser = async () => {
  const message = {
    to: 'ExponentPushToken[ioMiWuIjK__hUUJ3H_2Js7]',
    sound: 'default',
    title: 'Test Title',
    body: 'Test Body',
    data: {},
    android: {
      "channelId": "chat-messages" //and this
    }

  };
  const response = await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });
  const data = response._bodyInit;
  console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
};

export default class AddButton extends React.Component {

  constructor(props){
    super(props);
    this.state={myfam:[],assetsLoaded: false,new_name:'',gender:'',new_email:'',nofam:true,new_mobile:'',name:'',mobile:'',flat_no:'',myflats:[],
    email:'',userid:'',age_group:'',active:false,helps:[],nohelp:true,isVisible: false,new_user_image:'add_family.jpg',usercode:'',
    isVisible2:false,token: '',error: '',quick_actions:false,cab_tabs:false,cabtype:'Once',cabtime:'',vcomp:'',vnumber:'',
    selectedItems : [],validity:'',start_time:'',end_time:'',delivery_tabs:false,service_list:false,help_type:'',cab1advance:false,
    cab2advance:false,del1advance:false,del2advance:false,ocomp:'',start_date:today,optionname:'Advance',qr_img:'',qrmodal:false,
    vModal:false,noveh:true,vehicles:[],veh_type:'',veh_number:'',vmake:'',saddress:'',is_secr:'',freq_ent:[],noentry:true,
    freq_guest:[],noguest:true,is_admin:'',loc_url:'',uimg:''}
  }

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };
 
  _pickImage = async () => {
    this.setState({spinner: !this.state.spinner});
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 4],
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
      let localUri = result.uri;
      let filename = localUri.split('/').pop();

      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;

      let formData = new FormData();
      formData.append('photo', { uri: localUri, name: filename, type });

      return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
        method: 'POST',
        body: formData,
        header: {
          'content-type': 'multipart/form-data',
        },
      }).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({new_user_image:responseJson.img});
      });
    }else{
      this.setState({spinner: !this.state.spinner});
    }
  };

  async componentDidMount(){
    await Font.loadAsync({
      'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
    });
    this.setState({ assetsLoaded: true });
  }

  selectedAction(type){
    if(type=="cab_tabs"){this.setState({cab_tabs:true});}
    if(type=="delivery_tabs"){this.setState({delivery_tabs:true});this.setState({help_type:'Delivery'});}
    this.setState({quick_actions:false});
  }

  open_help(type){
    this.setState({delivery_tabs:true});
    this.setState({service_list:false});
    this.setState({help_type:type});
    this.setState({quick_actions:false});
  }

  async addVehicle(){
    if(this.state.veh_type=="" || this.state.veh_number==""  || this.state.vmake==""){
      alert("Please Enter All The Fields.");
    }else{
      var userid = await AsyncStorage.getItem('userid');
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/add_vehicle/'+userid, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
              veh_type: this.state.veh_type,
              veh_number: this.state.veh_number,
              veh_name: this.state.vmake,
              flat:await AsyncStorage.getItem('flat'),
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({vModal: !this.state.vModal});
          alert(responseJson.msg)
        });
    }
  }

  async addnewMember(){
    if(this.state.new_name=="" || this.state.age_group==""  || this.state.gender=="" || this.state.new_mobile==""){
        alert("Please Enter All The Fields.");
      }else{
        var userid = await AsyncStorage.getItem('userid');
        this.setState({spinner: !this.state.spinner});
        fetch('http://agro-vision.in/dpal/Rest/add_new_member/'+userid, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.new_name,gender:this.state.gender,age_group:this.state.age_group,
                mobile: this.state.new_mobile,
                email: this.state.new_email,img:this.state.new_user_image,
                society:await AsyncStorage.getItem('society'),
                block:await AsyncStorage.getItem('block'),
                flat:await AsyncStorage.getItem('flat'),
                city:await AsyncStorage.getItem('city'),
            }),
          }).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            if(responseJson.status==1){
              alert(responseJson.msg)
            }else{
              this.setState({ isVisible:!this.state.isVisible})
              this.pageRefresh();
            }
          });
      }
  }

  async allowCabOnce(){
     this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowCabOnce/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            cabtime: this.state.cabtime,vnumber: this.state.vnumber,vcomp: this.state.vcomp,
            society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
            flat:await AsyncStorage.getItem('flat'),start_time:this.state.start_date+' '+this.state.start_time,
            ocomp:this.state.ocomp
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({cab_tabs:false});
        alert(responseJson.msg);
        this.props.navigation.navigate("Activity")
      });
  }
  async allowDeliveryOnce(){
    if(this.state.validity=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowDeliveryOnce/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            start_time: this.state.start_date+' '+this.state.start_time,society:await AsyncStorage.getItem('society'),
            block_id:await AsyncStorage.getItem('block'),
            validity: this.state.validity,vcomp: this.state.vcomp,help_type:this.state.help_type
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({delivery_tabs:false});
        alert(responseJson.msg);
        this.props.navigation.navigate("Activity")
      });
  }

  async allowDeliveryFreq(){
    if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){
    alert("Please Select All Fields.");return false;}
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowDeliveryFreq/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
            selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
            vnumber: this.state.vnumber,start_time:this.state.start_time,end_time:this.state.end_time,help_type:this.state.help_type
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({delivery_tabs:false});
        alert(responseJson.msg);
      });
  }

  advanceOptions(){
    this.setState({cab1advance:!this.state.cab1advance});
    if(this.state.optionname=="Advance"){
      this.setState({optionname:"Less"});
    }else{
      this.setState({optionname:"Advance"});
    }
  }

  async allowCabFreq(){
    ///alert(this.state.selectedItems);return false;
    if(this.state.selectedItems=="" || this.state.vcomp=="" || this.state.validity==""){alert("Please Select All Fields.");
    return false;}
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowCabFreq/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
            selectedItems: this.state.selectedItems,validity:this.state.validity,vcomp: this.state.vcomp,
            start_time:this.state.start_time,end_time:this.state.end_time,ocomp:this.state.ocomp
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson);
        this.setState({spinner: !this.state.spinner});
        this.setState({cab_tabs:false});
        alert(responseJson.msg);
      });
  }

  async goGuest(){
    this.setState({quick_actions:false})
    this.props.navigation.navigate("UserContacts",{gtype:1})
  }

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <View>
          <Container>
          <Modal isVisible={this.state.cab_tabs}>
           
           <View style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10,fontFamily:'custom-fonts'}}>Allow Cab Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({cab_tabs:false})}} size={22}/>
              </View>
           </View>
           <View style={{flex:1}}>
                    <Tabs>
                      <Tab heading="Once" style={{padding:10,height:200}}>
                      {this.state.cab1advance==false &&
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Allow My Cab to Enter Today Once in Next</Text>}
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5}}>Select Date & Time</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"   cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}} is24Hour={false}  date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      </View>
                      
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Valid for Next</Text>
                      </View>}
                      
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
                      <Picker
                      selectedValue={this.state.cabtime}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ cabtime: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,alignSelf:'center',fontFamily:'custom-fonts'}}>Last 4 Digit of Vehicle No</Text>
                      <TextInput keyboardType = "number-pad" style={{borderWidth:0.5,borderColor:'grey',
                      height:45,borderRadius:20,fontSize:30,textAlign:'center',padding:5,alignSelf:'center',width:'50%'}} placeholder="1234"
    maxLength={4} onChangeText={(vnumber)=>this.setState({vnumber})}/>
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      </View>}
                      <TouchableOpacity onPress={()=>this.advanceOptions()}>
                        <Text  style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,textAlign:'center',color:'#E74C3C'}}>{this.state.optionname} Options >></Text>
                      </TouchableOpacity>
                  <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabOnce()}>
                  <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                      <Tab heading="Frequent" style={{padding:10,height:200}}>
                      <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Days"/>
                          <Picker.Item value="30 Days" label="30 Days"  />
                        </Picker>
                      </View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                      <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
                confirmBtnText="Confirm" is24Hour={false} cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_time} mode="time" format="LT"
                confirmBtnText="Confirm" is24Hour={false} cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                      </View>
                    
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                    </Tabs>
                  </View>
                
           </Modal>
          <Modal isVisible={this.state.delivery_tabs}>
           <View style={{backgroundColor:'#1d2839',flexDirection:'row',justifyContent:'space-between',padding:10}}>
              <Image style={{width:30, height:30,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
              <Text style={{color:'#fff',fontSize:18,marginLeft:10,fontFamily:'custom-fonts'}}>Allow {this.state.help_type} Entry</Text>
              <View style={{flex:1,alignItems:'flex-end'}}>
              <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({delivery_tabs:false})}} size={22}/>
              </View>
           </View>
           <View style={{flex:1}}>
                    <Tabs>
                    <Tab heading="Once" style={{padding:10,height:200}}>
                      {this.state.del1advance==false &&
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Allow {this.state.help_type} to Enter Today Once in Next</Text>}
                      {this.state.del1advance==true &&
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Select Date & Time</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}  is24Hour={false} date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm" cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      </View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Valid for Next</Text>
                      </View>}
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
                      <Picker
                      selectedValue={this.state.validity}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ validity: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      
                      <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(vcomp)=>this.setState({vcomp})}/>
                      <TouchableOpacity onPress={()=>this.setState({del1advance:!this.state.del1advance})}><Text  style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,textAlign:'center',color:'#E74C3C'}}>Advance Options >></Text></TouchableOpacity>
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryOnce()}>
                      <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                      
                      <Tab heading="Frequent" style={{padding:10,height:200}}>
                      <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Day"/>
                          <Picker.Item value="30 Days" label="30 Day"  />
                        </Picker>
                      </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5}}>Select Allowed TimeSlot</Text>
                    <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                    <DatePicker style={{borderRadius:20}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}  is24Hour={false} date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                    <DatePicker style={{borderRadius:20}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}} is24Hour={false}  date={this.state.end_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                    </View>
                    <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company</Text>
                    <TextInput  style={styles.input2} placeholder="Company" onChangeText={(vcomp)=>this.setState({vcomp})}/>
                    <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Tab>
                    </Tabs>
                  </View>
                <Card>                  
                </Card>
           </Modal>
           
          <Modal isVisible={this.state.quick_actions} style={{borderRadius:50}}>
              <Card>
                <CardItem header bordered style={{backgroundColor:'#1d2839',flexDirection:'row'}}>
                  <Text style={{color:'#fff',fontSize:18,fontFamily:'custom-fonts'}}>Allow Future Entry</Text>
                  <View style={{flex:1,alignItems:'flex-end'}}>
                  <Ionicons name="md-close" color='#fff' onPress = {() => {this.setState({quick_actions:false})}} size={22}/>
                  </View>
                </CardItem>
                <CardItem bordered>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.selectedAction('cab_tabs')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/cab.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Cab</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/delivery.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Delivery</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.goGuest()} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={require('.././assets/guest.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Guest</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({ service_list:!this.state.service_list})} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/helper.jpg')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Staff</Text>
                    </TouchableOpacity>
                  </View>
                </CardItem>

                {this.state.service_list==true &&
                <List>
                    <ListItem onPress={() =>this.open_help('Home Repair')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Home Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Appliances Repair')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Appliances Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Internet Repair')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Internet Repair</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Beautician')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Beautician</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                    <ListItem onPress={() =>this.open_help('Others')}>
                      <Left><Text style={{fontFamily:'custom-fonts'}}>Others</Text></Left>
                      <Right><Ionicons name="md-arrow-dropright" size={18} /></Right>
                    </ListItem>
                  </List>}
              
                <CardItem bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.notifyUser()} style={{flex:1,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/kid_exit.png')}/>
                      <Text style={{fontFamily:'custom-fonts'}}>Kid Exit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:2,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/alert.png')}/>
                      <Text style={{textAlign:'center',fontFamily:'custom-fonts'}}>Security Alert</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selectedAction('delivery_tabs')} style={{flex:2,alignContent:'center',alignItems:'center'}}>
                      <Image style={{width:50, height:50,borderRadius:50}} source={require('.././assets/message.jpg')}/>
                      <Text style={{textAlign:'center',fontFamily:'custom-fonts'}}>Message to Guard</Text>
                    </TouchableOpacity>
                </CardItem>
              </Card>
           </Modal>
           
        </Container>
        <TouchableOpacity onPress={() =>this.setState({quick_actions:true})}>
              <Image style={{width:60, height:60,borderRadius:50}} source={require('.././assets/addbtn.jpg')}/></TouchableOpacity>
        </View>
        );
        }else {
          return (
              <View style={styles.container}>
                  <ActivityIndicator />
              </View>
          );
      }
    }
  }

  const styles = StyleSheet.create({
    cfont:{fontFamily: 'roboto-medium'},
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:20},
    
    btnText:{fontSize:18, textAlign:'center', color:'#fff',fontFamily:'custom-fonts'},btnText2:{fontSize:16, textAlign:'center', color:'#000'},
    container: {
      flex: 1,padding:5,
    },input:{width:'100%',borderRadius:20, borderWidth:0.5,fontFamily:'custom-fonts', height:40, marginBottom:10,padding:10,fontSize:16,backgroundColor:'#fff'},
    input2:{borderRadius:20,width:'100%', borderWidth:0.5,fontFamily:'custom-fonts', height:40,padding:10,fontSize:16,backgroundColor:'#fff'},
  });

