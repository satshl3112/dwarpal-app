import React from 'react';
import { StyleSheet,BackHandler, Text,Picker,AsyncStorage, TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import { Container, Header, Content, List, ListItem,Thumbnail,Left,Right,Body,Card } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import * as Font from 'expo-font';

export default class ListWorkers extends React.Component {
    static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle:navigation.getParam('ser_title', '0'),
    })
    
    constructor(props){
        super(props);
        this.state={ workers:[],spinner:true,service_type:'',serid:''
        }
    }
    handleBackPress = async () => {
        this.props.navigation.goBack()
        return true;
    }
    async componentDidMount(){
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
      const { navigation } = this.props;
      const serid = navigation.getParam('serid', '0');
      this.setState({service_type:navigation.getParam('ser_title', '')});
      this.setState({serid:navigation.getParam('serid', '')});
      var socid=await AsyncStorage.getItem('society');
      const url2='http://agro-vision.in/dpal/Rest/workers_api/'+serid+'/'+socid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner}); 
         this.setState({workers:responseJson})
         this.setState({allworkers:responseJson})
      }).catch((error)=>{
        console.log(error)
      });
    }
  
    listWorkers(id){
        
    }

    searchContacts = value => {
      if(value!=""){
      const filteredContacts = this.state.workers.filter(contact => {
        let contactLowercase = (
          contact.hname 
        ).toLowerCase();
  
        let searchTermLowercase = value.toLowerCase();
        return contactLowercase.indexOf(searchTermLowercase) > -1;
      });
      this.setState({ workers: filteredContacts });
      }else{
          this.setState({workers:this.state.allworkers})
      }
    };
  

    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <Container>
            <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <Content>
            <TextInput
          placeholder="Search Staff with name"
          placeholderTextColor="#dddddd"
          style={{backgroundColor: '#f3f3f3',height: 40,fontFamily:'custom-fonts',fontSize: 18,padding: 10,borderRadius:20,
          borderWidth:0.5,borderColor:'grey',margin:5}} 
            onChangeText={value => this.searchContacts(value)}/>
            <List>
            <FlatList data={this.state.workers} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                <ListItem avatar onPress={() => this.props.navigation.navigate('WorkProfile',{hid:item.hid,htype:this.state.service_type,serid:this.state.serid})}>
                <Left>
                  <Thumbnail style={{borderWidth:0.5,color:'grey'}} source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
                </Left>
                <Body>
                  <Text style={{fontSize:18,marginTop:5,fontFamily:'custom-fonts'}}>{item.hname}</Text>
                  <Text style={{color:'grey'}}>{item.totl} Flat(s)</Text>
                </Body>
                <Right>
                <Ionicons name="md-arrow-dropright" color="#E5E8E8" size={50} />
                </Right>
              </ListItem>}/>
            </List>
            </Content>
        </Container>    
    );}else {
        return (
            <View>
                <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            </View>
        );
    }
  }
}

