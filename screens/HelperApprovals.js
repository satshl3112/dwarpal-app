import React from 'react';
import {ScrollView, Text, View, Button,Image,StyleSheet,AsyncStorage,TouchableOpacity,FlatList,Alert,RefreshControl } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container,Content, Header, Left, Card,Form,CardItem,Badge} from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { HeaderBackButton } from 'react-navigation-stack';
import AnimatedLoader from "react-native-animated-loader";

export default class HelperApprovals extends React.Component {

    static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle: 'Helper Approvals',
      })
  constructor(props){
    super(props);
    this.state={pending_apprs:[],is_secr:'',spinner:false}
  }

  _onRefresh(){
    this.setState({refreshing:true})
    this.pageRefresh().then(()=>{
      this.setState({refreshing:false})
    })
  }
  
  async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      const url5='http://agro-vision.in/dpal/Rest/helper_approvals_api/'+userid
      fetch(url5).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({pending_apprs:responseJson})
      }).catch((error)=>{ console.log(error)});
  }

  async componentDidMount(){
    console.log(await AsyncStorage.getItem('secr'))
    this.setState({is_secr:await AsyncStorage.getItem('secr')})
    this.pageRefresh();
  }

  approveFlat(id,st,fltid,name,service){
    var nmsg='Staff '+name+' for '+service+' has been approve for your flat.';
    //alert(nmsg);return false;
    Alert.alert(
        'Confirm Action',
        'Sure you want to Update Status to '+st,
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed.'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
                this.setState({spinner: !this.state.spinner});
                fetch('http://agro-vision.in/dpal/Rest/approvehFlat/'+id+'/'+st+'/'+fltid, {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                  body: JSON.stringify({
                      nmsg:nmsg,
                  }),
                }).then((response)=>response.json())
                .then((responseJson)=>{
                  console.log(responseJson)
                  this.setState({spinner: !this.state.spinner});
                  alert(responseJson.msg)
                  this.pageRefresh();
                });
            }
          },
        ],
        {cancelable: false},
      );
  }

  render() {
    return (
      <Container style={{margin:5}}>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} 
          onRefresh={this._onRefresh.bind(this)}/>}>
        <Content>
        <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <FlatList data={this.state.pending_apprs} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
              <CardItem  style={{flexDirection:'row'}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5,borderRadius:50}} source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
              </View>
              <View style={{flex:8,padding:5}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <Text style={{fontSize:16,color:'#5D6D7E',fontFamily:'custom-fonts'}}>{item.hname}</Text>
                <Text style={{fontSize:16,color:'#76D7C4',fontFamily:'custom-fonts'}}>{item.ser_type}</Text>
                </View>
                <Text style={{fontSize:16,color:'#117A65',fontFamily:'custom-fonts'}}>Flat No {item.flat_no} {item.block_name}</Text>
                <Text style={{fontSize:16,color:'#5D6D7E',fontFamily:'custom-fonts'}}>{item.from_time} - {item.to_time}</Text>
              </View>
              </CardItem>
              <CardItem style={{flexDirection:'row',fontFamily:'custom-fonts',justifyContent:'space-between',borderTopWidth:0.5,
              borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  {item.req_status=='Requested' &&
                  <TouchableOpacity onPress={()=>this.approveFlat(item.htid,'Approved',item.flat_id,item.hname,item.ser_type)}>
                    <Text style={{fontSize:18,color:'#1E8449',fontFamily:'custom-fonts'}}>
                        <Ionicons name="md-checkmark" color='#1E8449'size={18}/> Approve</Text></TouchableOpacity>}
                  {item.req_status=='Requested' &&
                  <TouchableOpacity onPress={()=>this.approveFlat(item.htid,'Rejected',item.flat_id,item.hname,item.ser_type)}>
                    <Text style={{fontSize:18,color:'orange',fontFamily:'custom-fonts'}}>
                        <Ionicons name="md-close" color='orange'size={18}/> Reject</Text></TouchableOpacity>}
                        {item.req_status=='Approved' &&
                        <Text style={{color:'#fff',backgroundColor:'#27AE60',padding:5,fontSize:16,borderRadius:5,
                        fontFamily:'custom-fonts'}}>{item.req_status}</Text>}
                        {item.req_status=='Rejected' &&
                        <Text style={{color:'#fff',backgroundColor:'#EC7063',padding:5,fontSize:16,borderRadius:5,
                        fontFamily:'custom-fonts'}}>{item.req_status}</Text>}
                  <TouchableOpacity onPress={()=>this.approveFlat(item.htid,'Deleted',item.flat_id,item.hname,item.ser_type)}>
                    <Text style={{fontSize:18,color:'#EC7063',fontFamily:'custom-fonts'}}>
                        <Ionicons name="md-trash" color='#EC7063'size={18}/> Delete</Text></TouchableOpacity>
              </CardItem>
            </Card>}/>
        </Content></ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image_style:{
    width:'100%',height:120
  }
});
