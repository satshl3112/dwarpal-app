
import React from 'react';
import { StyleSheet, Text,Picker,AsyncStorage, TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { Container,Content,Header, Left, Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';


export default class CompanyProfile extends React.Component {

    render() {
      return (
        <Container>
          <Content>
          <Card>
            <CardItem header style={{backgroundColor:'#F5B041'}}><Text style={{fontSize:18,color:'#fff',fontWeight:'600'}}>PLANET OF INVESTMENT</Text></CardItem>
            <CardItem>
            <Body><Text style={{textAlign:'justify'}}>Its an honour to tell our investors that our company started from 1st of January'2016 in market to facillitate services in Demat services successfully .
                In our platform you can trade various types of asset which is trading in india market i.e. Commodity.</Text>
                </Body></CardItem>
          </Card>
          <Card>
            <CardItem header style={{backgroundColor:'#D7DBDD'}}><Text style={{fontSize:18, color:'#566573',fontWeight:'600',textAlign:'center'}}>How It Works</Text></CardItem>
            <CardItem>
            <Body>
                <Text style={styles.points}>1. Create an Account</Text>
                <Text style={styles.points}>2. Add Money to Wallet</Text>
                <Text style={styles.points}>3. Buy or Sell Trades</Text>
            </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header style={{backgroundColor:'#5D6D7E'}}><Text style={{fontSize:18,color:'#fff',fontWeight:'600',textAlign:'center'}}>Latest News</Text></CardItem>
            <CardItem>
            <Body>
                <Text style={styles.points2}>From 1st of April company cuts its brokerage on demat plans up to 50%</Text>
                <Text style={styles.points2}>We are Serving more than 5000 Happy Clients</Text>
                <Text style={styles.points2}>iGlobal Commodities will start soon its advisory services in the month of April.</Text>
            </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header style={{backgroundColor:'#7DCEA0'}}>
              <Text style={{fontSize:18,color:'#fff',fontWeight:'600',textAlign:'center'}}>iGlobal Pvt Ltd.</Text></CardItem>
            <CardItem><Body>
                <Text>Head Office</Text>
                <Text style={styles.points3}>02, First Floor, Kadamba Complex, F-Block Gamma-1, Block F, Gamma 1, Greater Noid, Uttar Pradesh 201310</Text>
                <Text>Branch Office</Text>
                <Text style={styles.points3}>Manyata Embassy Business Park, Outer Ring Road Opp. BEL Corporate Office, MS Ramaiah North City, Manayata Tech Park, Nagavara, Bengaluru, Karnataka 560045</Text>
                </Body>
            </CardItem>
            <CardItem><Body>
                <Text style={styles.points3}><Icon name='md-at' /> info@iGlobal.com</Text>
                <Text style={styles.points3}><Icon name='md-at' /> iglobelinvestment@gmail.com</Text>
                <Text style={styles.points3}><Icon name='logo-whatsapp' /> +91 9179183271</Text></Body>
            </CardItem>
          </Card>
          </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
    points:{color:'#6C3483',fontSize:20},
    points2:{color:'#F39C12',fontSize:18,marginBottom:5,borderBottomWidth:0.5,borderBottomColor:'grey'},
    points3:{color:'#34495E',fontSize:16,marginBottom:5,fontWeight:'600'},
  });
