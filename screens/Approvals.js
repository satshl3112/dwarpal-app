import React from 'react';
import { Text, View, Button,Image,StyleSheet,AsyncStorage,TouchableOpacity,FlatList,Alert } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container,Content, Header, Left, Card,Form,CardItem,Badge} from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { HeaderBackButton } from 'react-navigation-stack';
import AnimatedLoader from "react-native-animated-loader";

export default class Approvals extends React.Component {

    static navigationOptions =  ({ navigation }) => ({
      headerTitleStyle: {color:'white'},
      headerStyle: {backgroundColor:'#25be9f'},
      headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle: 'Pending Requests',
      })
  constructor(props){
    super(props);
    this.state={pending_apprs:[],is_secr:'',spinner:false}
  }
  
  async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      const { navigation } = this.props;
      const aptype= navigation.getParam('type', '');
      var flatid='';
      var userid = await AsyncStorage.getItem('userid');
      if(aptype=='Flat'){flatid=await AsyncStorage.getItem('flat');}
      const url5='http://agro-vision.in/dpal/Rest/flat_pending_req/'+userid+'/'+flatid;
      fetch(url5).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({pending_apprs:responseJson})
      }).catch((error)=>{ console.log(error)});
  }

  async componentDidMount(){
    console.log(await AsyncStorage.getItem('secr'))
    this.setState({is_secr:await AsyncStorage.getItem('secr')})
    this.pageRefresh();
  }

  approveFlat(id,st,token){
    Alert.alert(
        'Confirm Action',
        'Sure you want to Update Status to '+st,
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed.'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{

                this.setState({spinner: !this.state.spinner});
                fetch('http://agro-vision.in/dpal/Rest/approveFlat/'+id+'/'+st, {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                  body: JSON.stringify({
                    token:token,
                  }),
                }).then((response)=>response.json())
                .then((responseJson)=>{
                  this.setState({spinner: !this.state.spinner});
                  alert(responseJson.msg)
                  this.pageRefresh();
                });

                //this.setState({spinner: !this.state.spinner});
                //const url6='http://agro-vision.in/dpal/Rest/approveFlat/'+id+'/'+st+'/'+token;
                //fetch(url6).then((response)=>response.json())
                //.then((responseJson)=>{
                //    this.setState({spinner: !this.state.spinner});
                //    alert(responseJson.msg)
                //    this.pageRefresh();
                //}).catch((error)=>{ console.log(error)});
            }
          },
        ],
        {cancelable: false},
      );
  }

  render() {
    return (
      <Container style={{margin:5}}>
        <Content>
        <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <FlatList data={this.state.pending_apprs} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <Card>
              <CardItem  style={{flexDirection:'row'}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
              </View>
              <View style={{flex:8,padding:5}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <Text style={{fontSize:16,color:'#5D6D7E',fontFamily:'custom-fonts'}}>{item.name}</Text>
                <Text style={{fontSize:14,color:'#76D7C4',fontFamily:'custom-fonts'}}>{item.owner_type}</Text>
                </View>
                <Text style={{fontSize:14,color:'#117A65',fontFamily:'custom-fonts'}}>{item.soc_name}</Text>
                <Text style={{fontSize:14,color:'#5D6D7E',fontFamily:'custom-fonts'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              </CardItem>
              <CardItem style={{flexDirection:'row',fontFamily:'custom-fonts',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  <TouchableOpacity onPress={()=>this.approveFlat(item.rid,'Approved',item.expo_token)}>
                    <Text style={{fontSize:18,color:'#1E8449',fontFamily:'custom-fonts'}}>
                        <Ionicons name="md-checkmark" color='#1E8449'size={18}/> Approve</Text></TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.approveFlat(item.rid,'Rejected',item.expo_token)}>
                    <Text style={{fontSize:18,color:'#EC7063',fontFamily:'custom-fonts'}}>
                        <Ionicons name="md-close" color='#EC7063'size={18}/> Reject</Text></TouchableOpacity>
              </CardItem>
            </Card>}/>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image_style:{
    width:'100%',height:120
  }
});
