import React from 'react';
import { StyleSheet,KeyboardAvoidingView,AsyncStorage, Text,Image, View, TextInput ,TouchableOpacity } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
export default class VerifyForgotOTP extends React.Component {
    static navigationOptions =  ({ navigation }) => ({
        headerLeft: <HeaderBackButton color='#fff' onPress={() => navigation.goBack(null)} />,headerRight:'', headerTitle: 'Verify OTP',
      })

    constructor(props){
        super(props);
        this.state={
            otp:'',votp:'',mobile:''
        }
    }

    async componentDidMount(){
        const { navigation } = this.props;
        var rec_otp=navigation.getParam('votp', '');
        var rec_mob=navigation.getParam('mobile', '');
        //alert(rec_otp);
        this.setState({votp:rec_otp})
        this.setState({mobile:rec_mob})
        //alert(this.state.votp);
       // alert(this.state.mobile);
    }

    otpSubmit=()=>{
        if(this.state.otp==this.state.votp){
            //alert(this.state.otp+'/'+this.state.votp);
            this.props.navigation.navigate("ResetPassword",{mobile:this.state.mobile,votp:this.state.votp});
          }else{
            alert("Please Enter Correct OTP.");
            //this.props.navigation.navigate("ResetPassword",{mobile:mobile,otp:votp});
          }
    }


  render() {
    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <Image style={{width:100, height:150,marginBottom:50}} source={require('.././assets/otp.png')}/>
            <Text style={{fontSize:16,marginBottom:10}}>Verify Your OTP</Text>
            <TextInput keyboardType = "number-pad"
    maxLength={6} onChangeText={(otp)=>this.setState({otp})}  style={styles.input} placeholder="Enter 6 Digit OTP"/>
            
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>this.otpSubmit()}><Text style={styles.btnText}>Verify OTP</Text></TouchableOpacity>
                <TouchableOpacity  style={styles.btn2}><Text style={styles.btnText}>Resend OTP</Text></TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input:{width:'80%', borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16},
    btn:{width:'45%', padding:10, backgroundColor:'#FF8E0F'},btn2:{width:'45%', padding:10, backgroundColor:'#FFBE0F'},
    btnText:{fontSize:18, textAlign:'center', color:'#fff'}
  });