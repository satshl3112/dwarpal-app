import React from 'react';
import { StyleSheet, WebView,Text,CheckBox,Image, View,Picker,KeyboardAvoidingView,TextInput,FlatList,Button,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title } from 'native-base';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Ionicons } from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';

var metals=[
  {label:'BUY', value:'BUY'},
  {label:'SELL', value:'SELL'}
]

export default class MyTrades extends React.Component {

    static navigationOptions = {
      title: 'Add New Trade',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#1473B2',
      },
    };

    constructor(props){
      super(props);
      this.state={trade_metals:[],buy_type:'BUY',asset:'',mega_lot:'',placev:'',spinner:false,
      lotsize:1,limit:'',market:false,target:'',stoploss:'',wpin:'',mxtrades:1,
      }
    }

    checkBoxTest(){
      this.setState({
        market:!this.state.market
      })
    }

    async componentDidMount(){
      var userid = await AsyncStorage.getItem('userid');
          const url='http://iglobal.in/home/max_trade/'+userid;
          fetch(url).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({mxtrades:responseJson.maxtr})
            this.setState({mega_lot:responseJson.mega_lot})
            this.setState({placev:'Max Lot - '+ responseJson.maxtr})
             //console.log(this.state.mxtrades)
          }).catch((error)=>{
            console.log(error)  
          })
          const wurl='http://iglobal.in/home/wallet_api/'+userid;
          fetch(wurl).then((response)=>response.json())
          .then((responseJson)=>{
             this.setState({isLoading: false,wallet:responseJson.wallet})
          }).catch((error)=>{
            console.log(error)
          })
          const wurl2='http://iglobal.in/home/lots_list_api/'+userid;
          fetch(wurl2).then((response)=>response.json())
          .then((responseJson)=>{
            this.setState({trade_metals:responseJson})
            //console.log(responseJson.lots)
            // this.setState({isLoading: false,wallet:responseJson.wallet})
          }).catch((error)=>{
            console.log(error)
          })

      //this.socket=io('http://eglobel.com:4003');
      //this.socket.on("tr_emit",data=>{        });
    }

   
    async addTrade(){
      
      if(this.state.asset=="" || this.state.lotsize=="" || this.state.lotsize==0 || this.state.wpin=="")
      {
          alert("Please Enter and Correct the Fields.");return false;
         // this.setState({errorMsg:'Please Enter Username & Password'});
        }else if(this.state.lotsize>this.state.mxtrades){
          alert("You can put max "+this.state.mxtrades+" Lotsize"); return false;
        }else{
         // return false;
          this.setState({spinner: !this.state.spinner});
          var userid = await AsyncStorage.getItem('userid');
          var name = await AsyncStorage.getItem('name');
          fetch('http://iglobal.in/home/add_trade_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  userid:userid,name:name,
                  asset: this.state.asset,
                  lotsize: this.state.lotsize,
                  buy_type: this.state.buy_type,
                  limit: this.state.limit,
                  market: this.state.market,
                  target:this.state.target,
                  stoploss:this.state.stoploss,wpin:this.state.wpin
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({asset:''})
              this.setState({lotsize:''})
              this.setState({market:false})
              this.setState({limit:''})
              this.setState({target:''})
              this.setState({stoploss:''})
              this.setState({wpin:''})
              this.setState({spinner: !this.state.spinner});
              console.log(responseJson);
              alert(responseJson.msg);
            if(responseJson.status==0){
              this.props.navigation.navigate("LiveTrades");
            }
            });
        }
  }

    render() {
      return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.container}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'} size="large"
          textStyle={styles.spinnerTextStyle}
        />
        <Container style={{flex:1,flexDirection:'row'}}>
          <Content>
            <Card>
              <CardItem>
              <RadioForm style={{marginRight:10}}
                radio_props={metals}
                initial={0}// formHorizontal={true}  labelHorizontal={true}
                onPress={(value)=>{this.setState({buy_type:value})}}
              /></CardItem>
              <CardItem>
              <View style={{borderWidth:0.5, borderColor:'grey'}}>
              <Picker
                    selectedValue={this.state.asset}
                    style={{width:150, height:40, borderWidth:0.5}}
                    onValueChange={(itemValue) => this.setState({ asset: itemValue})}>
                       <Picker.Item label="Select Assets" value="" />
                    {this.state.trade_metals.map((facility, i) => {
                        return <Picker.Item key={i} value={facility.metal+'-'+facility.lots} label={facility.metal} />
                    })}
                </Picker>
                </View>
              </CardItem>
              <CardItem>
                <TextInput maxLength={2} value={this.state.lotsize} keyboardType = "number-pad"  placeholder={this.state.placev} onChangeText={(lotsize)=>this.setState({lotsize})}  style={styles.input}/>
              </CardItem>
              <CardItem style={{marginTop:-10}}>
                <CheckBox value={this.state.market} onChange={()=>this.checkBoxTest()} />
                <Text style={{fontSize:18}}> Market Rate</Text>
              </CardItem>
              <CardItem>
                <TextInput placeholder="Limit" value={this.state.limit} keyboardType = "number-pad" onChangeText={(limit)=>this.setState({limit})}  style={styles.input}/>
              </CardItem>
              <CardItem>
                <TextInput placeholder="Target" value={this.state.target} keyboardType = "number-pad" onChangeText={(target)=>this.setState({target})}  style={styles.input}/>
              </CardItem>
              <CardItem>
                <TextInput placeholder="Stop Loss" value={this.state.stoploss} keyboardType = "number-pad" onChangeText={(stoploss)=>this.setState({stoploss})}  style={styles.input}/>
              </CardItem>
              <CardItem>
                <TextInput placeholder="Wallet Pin" value={this.state.wpin} keyboardType = "number-pad"  onChangeText={(wpin)=>this.setState({wpin})}  style={styles.input}/>
              </CardItem>
              <CardItem>
              <TouchableOpacity style={styles.btn} onPress={()=>this.addTrade()}><Text style={styles.btnText}>Add Trade</Text></TouchableOpacity>
              </CardItem>
            </Card>
          </Content>
          
          <WebView
            source={{uri: 'http://iglobal.in/home/dashboard_api'}}
            style={{margin:5}}
          />
        </Container>
        </View>
        </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },input:{width:'100%', borderWidth:0.5,marginTop:-10, height:40, marginBottom:2,padding:5,fontSize:16},
  btn:{width:'100%', marginTop:-20, padding:10, backgroundColor:'#fa961f'},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  metals:{backgroundColor: 'transparent'},
  metals2:{backgroundColor: 'transparent'},
});
