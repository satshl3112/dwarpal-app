
import React from 'react';
import { StyleSheet, Text,Picker,AsyncStorage, TouchableOpacity,View,TextInput,FlatList,Dimensions,Button } from 'react-native';
import { Container,Content,Header, Left, Card, Input, Item, CardItem, Body,Icon, Right, Title } from 'native-base';


export default class Withdraw extends React.Component {

    constructor(props){
        super(props);
        this.state={ bank_name:'',acc_name:'',acc_number:'',ifsc:'',actype:''
        }
    }

    async componentDidMount(){
        var userid = await AsyncStorage.getItem('userid');
        const wurl='http://iglobal.in/home/bank_details_api/'+userid;
        fetch(wurl).then((response)=>response.json())
        .then((responseJson)=>{
           this.setState({bank_name:responseJson.bank_name})
           this.setState({acc_name:responseJson.acc_holder})
           this.setState({acc_number:responseJson.acc_no})
           this.setState({ifsc:responseJson.ifsc})
           this.setState({actype:responseJson.acc_type})
        }).catch((error)=>{
          console.log(error)
        });
    }

    async updateBank(){
        if(this.state.bank_name=='' || this.state.acc_name=='' || this.state.acc_number=='' || this.state.ifsc=='' || 
        this.state.actype==''){
          alert("Please Enter All Bank Details.");return false;
        }
        var userid = await AsyncStorage.getItem('userid');
        fetch('http://iglobal.in/home/update_bank_api/'+userid, {
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    bank_name:this.state.bank_name,acc_name:this.state.acc_name,acc_number:this.state.acc_number,
                    ifsc:this.state.ifsc,actype:this.state.actype,
                  }),
                }).then((response)=>response.json())
                .then((responseJson)=>{
                  //this.setState({dataSource:responseJson});
                  //console.log(responseJson);
                  alert(responseJson.msg);
        });
      }

    render() {
      return (
        <Container>
          <Content>
          <Card>
            <CardItem header style={{backgroundColor:'#F5B712'}}><Text style={{fontSize:18,color:'#fff'}}>Update Your Bank Details</Text></CardItem>
            <Body style={{padding:5}}>
              <Text style={styles.label}>Bank Name</Text>
              <Item regular style={{marginBottom:5}}>
                <Input value={this.state.bank_name} onChangeText={(bank_name)=>this.setState({bank_name:bank_name})}  placeholder='Bank Name' />
              </Item>
              <Text style={styles.label}>Account Holder's Name</Text>
              <Item regular style={{marginBottom:5}}>
                <Input value={this.state.acc_name} onChangeText={(acc_name)=>this.setState({acc_name:acc_name})}  placeholder='Account Holder Name' />
              </Item>
              <Text style={styles.label}>Account Number</Text>
              <Item regular style={{marginBottom:5}}>
                <Input keyboardType = "number-pad"
    maxLength={20} value={this.state.acc_number} onChangeText={(acc_number)=>this.setState({acc_number:acc_number})}  placeholder='Account Number' />
              </Item>
              <View style={{flexDirection:'row'}}>
              <View  style={{width:150, flex:1, alignSelf:'flex-start', marginLeft:2, height:52, borderWidth:0.5,borderColor:'grey'}}>
                <Picker 
                selectedValue={this.state.actype}
                onValueChange={(itemValue, itemIndex) =>
                    this.setState({actype: itemValue})
                }>
                <Picker.Item label="Account Type" value='' />
                <Picker.Item label="Current Account" value="Current Account" />
                <Picker.Item label="Savings Account" value="Savings Account" />
                </Picker>
              </View>
              <View style={{flex:1,}}>
              <Item regular style={{marginBottom:5}}>
                <Input value={this.state.ifsc} onChangeText={(ifsc)=>this.setState({ifsc:ifsc})}  placeholder='IFSC Code' />
              </Item>
              </View>
              </View>
              <Button title="Update Bank Details" onPress={()=>this.updateBank()}/>
            </Body>
            </Card>
          </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
  title:{fontSize:16,color:'green'},btn:{width:'100%', padding:5, backgroundColor:'#1b65d3'},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  label:{alignSelf:'flex-start',marginLeft:2,color:'grey'}
});
