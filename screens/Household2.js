import React from 'react';
import { Linking,StyleSheet,BackHandler, WebView,Text,ScrollView,RefreshControl,CheckBox,Image, View,Picker,KeyboardAvoidingView,TextInput,FlatList,Button,TouchableOpacity,AsyncStorage } from 'react-native';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title,Fab } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from "react-native-modal";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import ActionButton from 'react-native-action-button';

export default class Household extends React.Component {
    constructor(props){
      super(props);
      this.state={myfam:[],new_name:'',gender:'',new_email:'',nofam:true,new_mobile:'',name:'',mobile:'',flat_no:'',myflats:[],
      email:'',userid:'',age_group:'',active:false,helps:[],nohelp:true,isVisible: false,new_user_image:'add_family.jpg',
      isVisible2:false,token: '',error: ''}
    }

   

    _pickImage = async () => {
      this.setState({spinner: !this.state.spinner});
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 4],
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
  
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
  
        let formData = new FormData();
        formData.append('photo', { uri: localUri, name: filename, type });
  
        return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
          method: 'POST',
          body: formData,
          header: {
            'content-type': 'multipart/form-data',
          },
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({new_user_image:responseJson.img});
        });
      }else{
        this.setState({spinner: !this.state.spinner});
      }
    };

    async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      this.setState({userid:await AsyncStorage.getItem('userid')});
      this.setState({name:await AsyncStorage.getItem('name')});
      this.setState({mobile:await AsyncStorage.getItem('mobile')});
      this.setState({email:await AsyncStorage.getItem('email')});
      this.setState({flat_no:await AsyncStorage.getItem('flat_no')+' '+await AsyncStorage.getItem('blockname')+' '+await AsyncStorage.getItem('societyname')});
      //Get Family Members
      const url2='http://agro-vision.in/dpal/Rest/myfamily_api/'+flatid+'/'+userid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        if(responseJson.status==1){
          this.setState({nofam:false})
          this.setState({myfam:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)} );
      //Get Daily Helps
      const url1='http://agro-vision.in/dpal/Rest/daily_helper_api/'+flatid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson);
        if(responseJson.status==1){
          this.setState({nohelp:false})
          this.setState({helps:responseJson.result})
        }
      }).catch((error)=>{ console.log(error)} );
    }

    async componentDidMount(){
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
      });
      this.pageRefresh();
    }

    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }

    async changeFlat(){
      var userid = await AsyncStorage.getItem('userid');
      this.setState({ isVisible2:!this.state.isVisible2})
      this.setState({spinner: !this.state.spinner});
      const url1='http://agro-vision.in/dpal/Rest/myflats/'+userid;
      fetch(url1).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({myflats:responseJson})
      }).catch((error)=>{ console.log(error)} );
    }

    gotoSociety(s,b,f,fno,c,blck,so,st){
      //alert(f)
      if(st!="Approved"){
        alert("Your Request is Pending");return false;
      }
      AsyncStorage.setItem("blockname",blck);
      AsyncStorage.setItem("society",s);
      AsyncStorage.setItem("societyname",so);
      AsyncStorage.setItem("block",b);
      AsyncStorage.setItem("flat",f);
      AsyncStorage.setItem("flat_no",fno);
      AsyncStorage.setItem("city",c);
      this.setState({ isVisible2:!this.state.isVisible2})
      this.setState({nohelp:true})
      this.setState({nofam:true})
      this.pageRefresh();
    }

    addNewFlat(){
      this.setState({ isVisible2:!this.state.isVisible2})
      this.props.navigation.navigate("Dashboard")
    }

    async addnewMember(){
      if(this.state.new_name=="" || this.state.age_group==""  || this.state.gender=="" || this.state.new_mobile==""){
          alert("Please Enter All The Fields.");
        }else{
          var userid = await AsyncStorage.getItem('userid');
          this.setState({spinner: !this.state.spinner});
          fetch('http://agro-vision.in/dpal/Rest/add_new_member/'+userid, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  name: this.state.new_name,gender:this.state.gender,age_group:this.state.age_group,
                  mobile: this.state.new_mobile,
                  email: this.state.new_email,img:this.state.new_user_image,
                  society:await AsyncStorage.getItem('society'),
                  block:await AsyncStorage.getItem('block'),
                  flat:await AsyncStorage.getItem('flat'),
                  city:await AsyncStorage.getItem('city'),
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
            this.setState({spinner: !this.state.spinner});
            this.setState({ isVisible:!this.state.isVisible})
            this.setState({myfam:responseJson})
            console.log(responseJson);
            });
        }
    }

    componentWillReceiveProps(nextProps){
      console.log("logged");
    }

    render() {
      return (
        
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
          
          <View style={{flex:1, backgroundColor: '#f3f3f3'}}>
            {/* Rest of the app comes ABOVE the action button component !*/}
            <ActionButton buttonColor="rgba(231,76,60,1)">
              <ActionButton.Item buttonColor='#9b59b6' title="New Task" onPress={() => console.log("notes tapped!")}>
                <Icon name="md-create" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() => {}}>
                <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => {}}>
                <Icon name="md-done-all" style={styles.actionButtonIcon} />
              </ActionButton.Item>
            </ActionButton>
          </View>
          <View style={{shadowColor: '#1d2839',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,shadowRadius: 2,elevation: 5,
           backgroundColor:'#5D6D7E',borderRadius:5,padding:5,margin:5, flexDirection:'row'}}>
            <Text onPress = {() => this.changeFlat()} style={{color:'#fff',fontSize:16}}>{this.state.flat_no}</Text>
            <Ionicons name="md-arrow-dropdown" color='#fff' style={{marginLeft:20}} size={25}/>
           </View>
           
           <Modal isVisible={this.state.isVisible2}>
           <Card>
            <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
              <Text style={{color:'#fff'}}>Select Flats</Text>
            </CardItem>
            <FlatList data={this.state.myflats} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View style={item.req_status=="Approved"?{shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#76D7C4',margin:5,borderRadius:5}:
            {shadowColor: '#000',
            shadowOffset: { width: 10, height:10 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 5,flexDirection:'row',padding:5,backgroundColor:'#F1948A',margin:5,borderRadius:5}}>
              <View style={{flex:2}}>
              <Image style={{width:50, height:50,margin:5}} source={require('.././assets/myflat.png')}/>
              </View>
              <View style={{flex:7,paddingLeft:10}}>
              <Text  onPress={()=>this.gotoSociety(item.soc_id,item.block_id,item.flat_id,item.flat_no,item.city_id,item.block_name,
                item.soc_name,item.req_status)} style={{fontSize:18,color:'#1e2939'}}>{item.soc_name}</Text>
                <Text style={{fontSize:16,color:'#fff'}}>Flat No. {item.flat_no} {item.block_name}</Text>
              </View>
              <View style={{flex:1,alignContent:'center'}}><Ionicons color="#fff" name="md-arrow-dropright" size={55}/></View>
            </View>}/>
            <CardItem footer bordered style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Button title="Add New Flat" onPress={()=>this.addNewFlat()} color="#117A65"/>
              <Button title="Cancel" color="grey" style={{marginLeft:10}} onPress = {() => { this.setState({ isVisible2:!this.state.isVisible2})}}/>
            </CardItem>
          </Card>
           </Modal>
           <Modal isVisible={this.state.isVisible}>
          <Card>
              <CardItem header bordered style={{backgroundColor:'#1d2839'}}>
                <Text style={{color:'#fff'}}>Add Family Member</Text>
              </CardItem>
              <CardItem bordered>
                <Body>
                  <TextInput onChangeText={(new_name)=>this.setState({new_name})} style={styles.input} placeholder="Full Name"/>
                  <View style={{flexDirection:'row',width:'80%',marginBottom:10}}>
                    <View style={{flex:8}}>
                      <Text style={{fontSize:12}}>Select Gender</Text> 
                      <View style={{borderWidth:1, borderColor:'grey'}}>
                      <Picker
                            selectedValue={this.state.gender}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ gender: itemValue})} >
                              <Picker.Item label="Select Gender" value="" />
                              <Picker.Item label="Male" value="Male" />
                              <Picker.Item label="Female" value="Female" />
                      </Picker> 
                      </View>
                      <Text style={{fontSize:12,marginTop:5}}>Select Age Group</Text> 
                      <View style={{borderWidth:1, borderColor:'grey'}}>
                      <Picker
                            selectedValue={this.state.age_group}
                            style={{ width: '100%',height:35 }}
                            onValueChange={(itemValue) => this.setState({ age_group: itemValue})} >
                              <Picker.Item label="Age Group" value="" />
                              <Picker.Item label="18-40" value="18-35" />
                              <Picker.Item label="35-50" value="35-50" />
                              <Picker.Item label="above 50" value="above 50" />
                      </Picker> 
                      </View>
                    </View>
                    <View style={{flex:2,padding:10}}>
                    <TouchableOpacity onPress={()=>this._pickImage()}>
                    <Image style={{width:100,height:100}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.new_user_image }}/>
                    </TouchableOpacity>
                    </View>
                  </View>
                  <TextInput autoCapitalize = 'none' onChangeText={(new_email)=>this.setState({new_email})}  style={styles.input} placeholder="Enter Email"/>
            
              <TextInput keyboardType = "number-pad"
      maxLength={10} onChangeText={(new_mobile)=>this.setState({new_mobile})}  style={styles.input} placeholder="Enter Mobile Number"/>
              
                </Body>
              </CardItem>
              <CardItem footer bordered style={{justifyContent:'space-between'}}>
                <Button title="Add Member" onPress={()=>this.addnewMember()} color="#117A65"/>
                <Button title="Cancel" color="grey" style={{marginLeft:10}} onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}}/>
              </CardItem>
            </Card>
          </Modal>
          <View style={{flex:1, backgroundColor: '#f3f3f3'}}>
            {/* Rest of the app comes ABOVE the action button component !*/}
            <ActionButton buttonColor="rgba(231,76,60,1)">
              <ActionButton.Item buttonColor='#9b59b6' title="New Task" onPress={() => console.log("notes tapped!")}>
                <Icon name="md-create" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() => {}}>
                <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => {}}>
                <Icon name="md-done-all" style={styles.actionButtonIcon} />
              </ActionButton.Item>
            </ActionButton>
          </View>
          <Spinner visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
          
          <View style={{shadowColor: '#1d2839',shadowOffset: { width: 10, height:10 },shadowOpacity: 1,shadowRadius:2,elevation:5,
          flexDirection:'row',padding:10,margin:5,borderRadius:5,backgroundColor:'#fff'}}>
            <View style={{flex:2}}>
            <Image style={{width:50, height:50,margin:5}} source={require('.././assets/user.png')}/>
            </View>
            <View style={{flex:6, margin:5}}>
            <Text style={{fontSize:18,color:'#0E6655',alignItems:'center',fontWeight:'600'}}>Hi, {this.state.name}</Text>
            <Text style={{fontSize:16,color:'#5D6D7E',alignItems:'center'}}>#000{this.state.userid}</Text>
           </View>
              
          </View>

          <View style={{flexDirection:'row',padding:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>My Family</Text>
            </View><View style={{flex:1}}>
              <Text onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}}  style={{fontSize:18,color:'#1ABC9C',textAlign:'right'}}>
              <Ionicons name="md-add" color='#1ABC9C' size={16}/> ADD</Text>
            </View>
          </View>
          {this.state.nofam &&
          <View style={{flexDirection:'row',marginTop:5,padding:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:1,borderRadius:4,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:90, height:90}} source={require('.././assets/addnew.png')}/> 
              <Text onPress = {() => { this.setState({ isVisible:!this.state.isVisible})}} style={{fontSize:16,color:'#1ABC9C'}}>Add Member</Text>
            </View>
            <View style={{flex:2}}></View>
          </View>}
          {!this.state.nofam &&
            <View style={{flexDirection:'row',marginTop:5}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.myfam}
            renderItem={({item}) =>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            width:125,margin:5,borderRadius:5,padding:10,backgroundColor:'#fff'}}>
              <TouchableOpacity style={{alignContent:'center',alignItems:'center'}}>
                <Image style={{width:60, height:60,borderRadius:100}} source={{uri: 'http://agro-vision.in/dpal/assets/images/users/'+item.uimg}}/>
                <Text style={{fontSize:16,color:'#1ABC9C'}}>{item.name}</Text>
                <Text style={{fontSize:14,color:'grey'}}>#000{item.uid}</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row',borderTopColor:'grey',borderTopWidth:0.5,margin:5,padding:5}}>
                <View style={{flex:1,alignItems:'center'}}><Ionicons onPress={()=>{Linking.openURL('tel:'+item.mobile);}} name="md-call" color='#117A65' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-share" color='grey' size={22}/></View>
              </View>
            </View>}/></View>}

          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Daily Help</Text>
            </View><View style={{flex:1}}>
              <Text onPress={() => this.props.navigation.navigate('ListServices')}  style={{fontSize:18,color:'#F39C12',textAlign:'right'}}>
              <Ionicons name="md-add" color='orange' size={18}/> ADD</Text>
            </View>
          </View>
          {this.state.nohelp &&
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:10,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/help.png')}/> 
              <Text onPress={() => this.props.navigation.navigate('ListServices')}  style={{fontSize:16,color:'#1ABC9C'}}> Add Daily Help</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>}
          {!this.state.nohelp &&
            <View style={{flexDirection:'row'}}>
            <FlatList horizontal={true} showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => index.toString()} 
            data={this.state.helps}
            renderItem={({item}) =>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            width:125,margin:5,borderRadius:5,padding:10,backgroundColor:'#FEF9E7'}}>
              <TouchableOpacity style={{alignContent:'center',alignItems:'center'}} onPress={() => this.props.navigation.navigate('WorkProfile',{hid:item.hid,htype:item.ser_type,serid:item.serid})} >
                <Image style={{width:80, height:80,borderRadius:100}} source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
                <Text style={{fontSize:16,color:'#1ABC9C'}}>{item.hname}</Text>
                <Text style={{fontSize:14,color:'grey'}}>{item.ser_type}</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row',justifyContent:'space-between',borderTopColor:'grey',borderTopWidth:0.5,margin:5,padding:5}}>
                <View style={{flex:1,alignItems:'center'}}><Ionicons onPress={()=>{Linking.openURL('tel:'+item.hphone);}} name="md-call" color='#E74C3C' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-notifications" color='grey' size={22}/></View>
                <View style={{flex:1,alignItems:'center'}}><Ionicons name="md-star" color='grey' size={22}/></View>
              </View>
          </View>}/></View>}
         <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Frequent Guests</Text>
            </View><View style={{flex:1}}>
              <Text style={{fontSize:18,color:'#E74C3C',textAlign:'right'}}>
              <Ionicons name="md-add" color='#E74C3C' size={18}/> ADD</Text>
            </View>
          </View>
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/addnew.png')}/> 
              <Text style={{fontSize:16,color:'#1ABC9C'}}> Add Entry</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>

          <View style={{flexDirection:'row',margin:5}}>
            <View style={{flex:1}}>
              <Text style={{fontSize:18}}>Frequent Entries</Text>
            </View><View style={{flex:1}}>
              <Text style={{fontSize:18,color:'#5DADE2',textAlign:'right',fontWeight:'600'}}>
              <Ionicons name="md-add" color='#5DADE2' size={18}/> ADD</Text>
            </View>
          </View>
          <View style={{flexDirection:'row',margin:5}}>
            <View style={{shadowColor:'#1d2839',shadowOffset:{width:10,height:10},shadowOpacity:1,shadowRadius:2,elevation:5,
            flex:2,borderRadius:5,alignContent:'center',padding:5,backgroundColor:'#fff',alignItems:'center'}}>
              <Image style={{width:100, height:100}} source={require('.././assets/calendar.png')}/> 
              <Text style={{fontSize:16,color:'#1ABC9C'}}> Add Entry</Text>
            </View>
            <View style={{flex:3,padding:20}}>
              <Text style={{color:'grey',textAlign:'justify'}}>Add the Domestic Staff which comes daily to help you. 
                Get Notified of their entry exit and easily track their Attendance.</Text>
            </View>
          </View>
        </ScrollView>
        
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: '#E8F8F5'
  },input:{width:'100%',borderRadius:2, borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16,backgroundColor:'#fff'},
});
