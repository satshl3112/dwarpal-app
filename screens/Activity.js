import React from 'react';
import { StyleSheet,ScrollView,Linking,WebView,Text,CheckBox,Image, View,Picker,KeyboardAvoidingView,ActivityIndicator,
  TextInput,FlatList,Button,TouchableOpacity,AsyncStorage,RefreshControl,Alert } from 'react-native';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title,Badge,Tab, Tabs } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
import * as Font from 'expo-font';
import MultiSelect from 'react-native-multiple-select';
import { Notifications } from 'expo';
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = dd + '-' + mm + '-' + yyyy;
var tday=yyyy + '-' + mm + '-' + dd;
var items = [{
  id: 'Sunday',
  name: 'Sunday',
}, {
  id: 'Monday',
  name: 'Monday',
}, {
  id: 'Tuesday',
  name: 'Tuesday',
}, {
  id: 'Wednesday',
  name: 'Wednesday',
}, {
  id: 'Thursday',
  name: 'Thursday',
}, {
  id: 'Friday',
  name: 'Friday',
}, {
  id: 'Saturday',
  name: 'Saturday',
}];

export default class Activity extends React.Component {
    constructor(props){
      super(props);
      this.state={entries:[],spinner:false,assetsLoaded: false,edit_type:'',isVisible3:false,cab1advance:false,cabtype:'',filterByTypes:'',
      filterBydate:'',filterBydates:'',filterByType:'',dates:[],name:'',entriess:[],helperlog:[],pending_apprs:[],
      optionname:'Advance',edit_id:'',delivery_tab:false}
    }

    onSelectedItemsChange = selectedItems => {
      this.setState({ selectedItems });
    };

    async pre_schedule(){
      this.setState({spinner: !this.state.spinner});
      var flatid = await AsyncStorage.getItem('flat');
      const url21='http://agro-vision.in/dpal/Rest/entries_api/'+flatid;
      fetch(url21).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      this.setState({filterBydate:''});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      const url23='http://agro-vision.in/dpal/Rest/waiting_entries/'+flatid;
      fetch(url23).then((response)=>response.json())
      .then((responseJson)=>{
          //console.log(responseJson);return false;
        this.setState({pending_apprs:responseJson});
      }).catch((error)=>{ console.log(error)} );
      const url234='http://agro-vision.in/dpal/Rest/entries_api/'+flatid;
      fetch(url234).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)});
      const url2l4='http://agro-vision.in/dpal/Rest/entry_logs/'+flatid;
      fetch(url2l4).then((response)=>response.json())
      .then((responseJson)=>{
        //this.setState({spinner: !this.state.spinner});
        this.setState({entriess:responseJson});
      }).catch((error)=>{ console.log(error)} );
      const url3l='http://agro-vision.in/dpal/Rest/myhelpers_log/'+flatid;
      fetch(url3l).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        //this.setState({spinner: !this.state.spinner});
        this.setState({helperlog:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    check_header(d){
      //console.log(d)
      if(this.state.dates.includes(d)==true){
        return true
      }else{
        this.state.dates.push(d);
        return false
      }
      //console.log(this.state.dates)
    }

    async allowDeliveryOnce(){
    if(this.state.validity=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
    this.setState({spinner: !this.state.spinner});
    fetch('http://agro-vision.in/dpal/Rest/allowDeliveryOnce/', {
        method: 'POST',
        headers: {Accept: 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify({
            flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
            start_time: this.state.start_date+' '+this.state.start_time,society:await AsyncStorage.getItem('society'),
            block_id:await AsyncStorage.getItem('block'),
            validity: this.state.validity,vcomp: this.state.vcomp,help_type:this.state.help_type
        }),
      }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({delivery_tabs:false});
        alert(responseJson.msg);
        this.props.navigation.navigate("Activity")
      });
  }

    getTotalDays(d){
      var dd=d.split(",");
      return dd.length+' Days';
    }

    async filterByDate1(d){
      this.setState({filterBydate:d});
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async componentDidMount(){
      Notifications.addListener(notification => {
          this.pageRefresh();
      });
      const { navigation } = this.props;
      this.focusListener = navigation.addListener('didFocus', () => {
        this.pageRefresh();
      });
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      this.setState({name:await AsyncStorage.getItem('name')});
      this.pageRefresh();
    }
    componentWillUnmount() {
      // Remove the event listener
      this.focusListener.remove();
    }
    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }
    
    checkuname(n){
        if(this.state.name==n){
        return "YOU";
        }else{return n;}
    }

    advanceOptions(){
      this.setState({cab1advance:!this.state.cab1advance});
      if(this.state.optionname=="Advance"){
        this.setState({optionname:"Less"});
      }else{
        this.setState({optionname:"Advance"});
      }
    }

    change_date(d1,d2,type,ftype){
      //console.log('dates '+d1+' '+d2+' '+today)
      var r='';
      var dd1=d1.split("-").reverse().join("-");
      var dd2=d2.split("-").reverse().join("-");
      if(ftype=="Frequent"){
        var g1=new Date(d1);
        var g2=new Date(tday);
        console.log(g1+' '+g2)
        if( g2.getTime()>g1.getTime() && type!='Frequent'){
          r+= 'Expired';
        }else
        if(dd1==today){r=r+' Valid Till Today ';}else{r=r+=' Valid Till '+dd1;}
        if(dd2==today){r=r+'  Today ';}else{r=r+=dd2;}
      }else{
          if(dd2==today){dd2=' Today ';}
          //if(dd1==today){
          var now = new Date();
          var dt = (now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + dd1;
          var userval = new Date(dt);
          var g1=new Date(d2);
          var g2=new Date(tday)
          if (dd2==' Today ' && now > userval || g2.getTime()>g1.getTime() && type!='Frequent'){
            r+= 'Expired';
          }else{r+= ' Valid Till '+dd1+', '+dd2;}
        
        //if(dd1==today){r=r+' Valid Till Today ';}else{r=r+=' Valid Till '+dd1;}
        //if(dd2==today){r=r+' Today ';}else{r=r+=dd2;}
      }
      return r;
    }

    async delEntry(eid){
      var userid = await AsyncStorage.getItem('userid');
      Alert.alert(
        'Confirm Action',
        'Sure you want to Cancel leave request?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
              
      const url='http://agro-vision.in/dpal/Rest/delentry_api/'+eid;
          fetch(url).then((response)=>response.json())
          .then((responseJson)=>{
              console.log(responseJson);
              alert(responseJson.msg);
              if(responseJson.status==0){
                this.pageRefresh();
              }
          }).catch((error)=>{
            console.log(error)
      })
            }
          },
        ],
        {cancelable: false},
      );
    }

    async filterByTypePre(type){
      this.setState({filterByTypes:type});
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)

      const url2l2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'/'+this.state.filterByTypes;
      fetch(url2l2).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async filterByType(type){
      this.setState({filterByType:type});
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)

      const url2l='http://agro-vision.in/dpal/Rest/entry_logs/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2l).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({entriess:responseJson});
      }).catch((error)=>{ console.log(error)} );

      //const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      //fetch(url2).then((response)=>response.json())
      //.then((responseJson)=>{
      //  this.setState({spinner: !this.state.spinner});
      //  this.setState({entries:responseJson});
      //}).catch((error)=>{ console.log(error)} );
    }

    reverseDate(d){
      var d=d.split("-").reverse().join("-");
      return d;
    }

    editEntry(id,type,etype){
      //alert(type)
      this.setState({cabtype:type});
      this.setState({edit_id:id})
      if(etype=="Cab" && type!="Frequent"){
      this.setState({isVisible3: !this.state.isVisible3});
      const urledit='http://agro-vision.in/dpal/Rest/editEntry_api/'+id;
      fetch(urledit).then((response)=>response.json())
      .then((responseJson)=>{
        //console.log(responseJson)
        if(responseJson.entry_date!=today){this.setState({cab1advance:true});}
        this.setState({edit_type:responseJson.entry_type+' '+responseJson.freq_type});
        var d=responseJson.entry_date.split("-").reverse().join("-");
        this.setState({start_date:d});
        this.setState({start_time:responseJson.start_time});
        this.setState({end_time:responseJson.end_time});
        this.setState({vnumber:responseJson.vnumber});
        this.setState({cabtime:responseJson.allow_dday});
      }).catch((error)=>{ console.log(error)} );
    }else if(type!="Frequent"){
      this.setState({delivery_tabs: !this.state.delivery_tabs});
      const urledit1='http://agro-vision.in/dpal/Rest/editEntry_api/'+id;
      fetch(urledit1).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({validity:responseJson.allow_dday});
        this.setState({vcomp:responseJson.comp});
        this.setState({edit_type:responseJson.entry_type+' '+responseJson.freq_type});
        var d=responseJson.entry_date.split("-").reverse().join("-");
        this.setState({start_date:d});
        this.setState({start_time:responseJson.start_time});
        this.setState({end_time:responseJson.end_time});
      }).catch((error)=>{ console.log(error)} );
    }
    }

    async fldate(dd){
      this.setState({filterBydate:dd});
      //alert(this.state.filterBydate)
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async fldates(dd){
      this.setState({filterBydates:dd});
      this.setState({spinner: !this.state.spinner});
      var d=dd.split("-").reverse().join("-");
      var flatid = await AsyncStorage.getItem('flat');
      const url3l3='http://agro-vision.in/dpal/Rest/myhelpers_log/'+flatid+'/'+d;
      fetch(url3l3).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({helperlog:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async allowDeliveryOnce(){
      if(this.state.validity=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowDeliveryOnceEdit/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              start_time: this.state.start_date+' '+this.state.start_time,society:await AsyncStorage.getItem('society'),
              block_id:await AsyncStorage.getItem('block'),edit_id:this.state.edit_id,
              validity: this.state.validity,vcomp: this.state.vcomp,help_type:this.state.help_type
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          this.setState({spinner: !this.state.spinner});
          alert(responseJson.msg);
          this.setState({delivery_tabs:false,start_time:'',vnumber:'',vcomp:'',ocomp:'',cabtime:''});
          this.pre_schedule();
        });
    }

    async allowCabOnce(){
      //if(this.state.cab1advance==true && this.state.vnumber=="" || this.state.vnumber.length<4){alert("Please Enter Last 4 Digit of Vehicle Number");return false;}
      //12if(this.state.cabtime=="" || this.state.vcomp==""){alert("Please Select All Fields.");return false;}
      //this.setState({spinner: !this.state.spinner});
      fetch('http://agro-vision.in/dpal/Rest/allowCabOnceEdit/', {
          method: 'POST',
          headers: {Accept: 'application/json','Content-Type': 'application/json'},
          body: JSON.stringify({edit_id:this.state.edit_id,
              flat: await AsyncStorage.getItem('flat'),userid: await AsyncStorage.getItem('userid'),
              cabtime: this.state.cabtime,vnumber: this.state.vnumber,vcomp: this.state.vcomp,
              society:await AsyncStorage.getItem('society'),block_id:await AsyncStorage.getItem('block'),
              flat:await AsyncStorage.getItem('flat'),start_time:this.state.start_date+' '+this.state.start_time,
              ocomp:this.state.ocomp
          }),
        }).then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          //this.setState({spinner: !this.state.spinner});
          this.setState({cab_tabs:false,start_time:'',isVisible3:false,vnumber:'',vcomp:'',ocomp:'',cabtime:''});
          alert(responseJson.msg);
          this.pre_schedule();
        });
    }
    
    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <Container style={{backgroundColor:'#fff'}}>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
         
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
          <Modal isVisible={this.state.isVisible3} style={{borderRadius:20}}>
            <Ionicons name="md-close" color='#fff' onPress = {() => this.setState({isVisible3: !this.state.isVisible3})} size={24}/>
            <Card style={{padding:20}}>
                      {this.state.cab1advance==false &&
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Allow My Cab to Enter Today Once in Next</Text>}
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5}}>Select Date & Time</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      </View>
                      
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Valid for Next</Text>
                      </View>}
                      
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
                      <Picker
                      selectedValue={this.state.cabtime}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ cabtime: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,alignSelf:'center',fontFamily:'custom-fonts'}}>Last 4 Digit of Vehicle No</Text>
                      <TextInput keyboardType = "number-pad" style={{borderWidth:0.5,borderColor:'grey',
                      height:45,borderRadius:20,fontSize:30,textAlign:'center',padding:5,alignSelf:'center',width:'50%'}} placeholder="1234"
    maxLength={4} onChangeText={(vnumber)=>this.setState({vnumber})} value={this.state.vnumber}/>
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      </View>}
                      {/* <TouchableOpacity onPress={()=>this.advanceOptions()}>
                        <Text  style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,textAlign:'center',color:'#E74C3C'}}>{this.state.optionname} Options >></Text>
                      </TouchableOpacity> */}
                  <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabOnce()}>
                  <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                  </Card>
          </Modal>
          <Modal isVisible={this.state.delivery_tabs}>
          <Ionicons name="md-close" color='#fff' onPress = {() => this.setState({delivery_tabs: !this.state.delivery_tabs})} size={24}/>
            <Card style={{padding:20}}>
            <View>
              <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Select Date & Time</Text>
              <View style={{flexDirection:'row'}}>
                <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
          dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
          dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
        confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
        <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
          dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
          dateInput: {marginLeft: 10,borderRadius:20}}}  is24Hour={false} date={this.state.start_time} mode="time" format="LT"
        confirmBtnText="Confirm" cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
              </View>
              <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Valid for Next</Text>
            </View>
            <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
              <Picker
              selectedValue={this.state.validity}
              style={{ width: '100%',height:30,color:'grey' }}
              onValueChange={(itemValue) => this.setState({ validity: itemValue})} >
                <Picker.Item label="Select" value="Select" />
                <Picker.Item label="1 Hour" value="1 Hour" />
                <Picker.Item label="2 Hour" value="2 Hour" />
                <Picker.Item label="4 Hour" value="4 Hour" />
                <Picker.Item label="8 Hour" value="8 Hour" />
                <Picker.Item label="12 Hour" value="12 Hour" />
                <Picker.Item label="24 Hour" value="24 Hour" />
              </Picker></View>
              
              <Text style={{fontSize:14,color:'grey',marginBottom:5}}>Company Name</Text>
              <TextInput style={styles.input2} value={this.state.vcomp}  onChangeText={(vcomp)=>this.setState({vcomp})}/>
              
              <TouchableOpacity style={styles.btn} onPress={()=>this.allowDeliveryOnce()}>
              <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
            </Card>
          </Modal>          
          <Content>
          <Tabs>
          <Tab heading="Visitor Log" tabStyle={{backgroundColor: '#1e2939'}} activeTabStyle={{backgroundColor: '#25be9f'}}>
                
                <View  style={{flexDirection:'row',justifyContent:'space-between',padding:5}}> 
                 <View visible={false} style={{borderWidth:1,flex:2, borderColor:'grey',borderRadius:10,padding:2}}>
                    <Picker
                          selectedValue={this.state.filterByType}
                          style={{ width: '100%',height:30 }} 
                          onValueChange={(itemValue) => this.filterByType(itemValue)}>
                            <Picker.Item label="All" value="" />
                            <Picker.Item label="Cab" value="Cab" />
                            <Picker.Item label="Delivery" value="Delivery" />
                            <Picker.Item label="Guest" value="Guest" />
                            <Picker.Item label="Others" value="Others" />
                    </Picker>
                  </View>
                  <DatePicker  customStyles={{dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0}, 
                    dateInput: {marginLeft: 10,borderRadius:10,height:40,flex:2}}}   date={this.state.filterBydate} placeholder='Select Date' mode="date" format="DD-MM-YYYY"
                  confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(filterBydate) =>this.fldate(filterBydate)}/>
                  <View style={{flex:1,marginLeft:5}}>
                    <TouchableOpacity onPress={()=>this.pageRefresh()} style={{padding:5,backgroundColor:'grey',borderRadius:10,alignItems:'center'}}>
                      <Ionicons name="md-refresh" color='#fff'size={26}/>
                    </TouchableOpacity> 
                  </View>
                </View>
                
                <FlatList data={this.state.pending_apprs} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                <Card>
                  <CardItem>
                    <TouchableOpacity  style={{flexDirection:'row'}} 
                    onPress={()=>this.props.navigation.navigate("AskApproval",{nid:item.nentry_id})}>
                      <Image style={{width:65, height:65,borderRadius:50,marginRight:10}} 
                      source={{uri: 'http://agro-vision.in/dpal/assets/images/guests/'+item.nentry_img}}/>
                    <View style={{flex:4}}>
                      <Text style={{fontSize:18,color:'#17A589'}}>{item.nentry_type} awaiting Entry</Text>
                      <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                      <Text style={styles.badge}>{item.block_name} Flat No {item.flat_no}</Text>
                      <Text>{item.save_time}</Text></View>
                      <View style={{flexDirection:'row'}}>
                      <Text style={{color:'orange',fontSize:16}}>{item.nentry_name} • {item.nentry_phone}</Text>
                      </View>
                    </View>
                    </TouchableOpacity>
                  </CardItem>
                  {item.nentry_status!="" &&
                  <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                      {item.nentry_status=="Approved" &&
                      <Text style={{fontSize:16,color:'#1E8449'}}>
                            <Ionicons name="md-log-in" color='#1E8449'size={18}/> Approved {item.nentry_time}</Text>}
                            {item.nentry_status=="Denied" &&
                        <Text style={{fontSize:16,color:'#EC7063'}}>
                            <Ionicons name="md-close" color='#EC7063'size={18}/> Denied {item.nentry_time}</Text>}
                            
                  </CardItem>}
                  {item.nentry_status=="" &&
                  <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                      <TouchableOpacity onPress={()=>this.approveEntry(item.nentry_id,'Approved')}>
                        <Text style={{fontSize:18,color:'#1E8449'}}>
                            <Ionicons name="md-checkmark" color='#1E8449'size={18}/> Approve</Text></TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.approveEntry(item.nentry_id,'Denied')}>
                        <Text style={{fontSize:18,color:'#EC7063'}}>
                            <Ionicons name="md-close" color='#EC7063'size={18}/> Deny</Text></TouchableOpacity>
                  </CardItem>}
                </Card>
              }/>
              <FlatList data={this.state.entriess} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                <View>
                  {this.check_header(item.entry_date)==true &&
                  <Text style={{alignSelf:'center',fontSize:12,textAlign:'center',width:100,padding:4,margin:5,
                  backgroundColor:'grey',color:'#fff',borderRadius:20}}>{this.reverseDate(item.entry_date)}</Text>}
                <Card>
                  <CardItem style={{flexDirection:'row'}}>
                    <View style={{flex:1,marginRight:10,}}>
                      <Image style={{width:65, height:65,borderRadius:50,borderWidth:1,borderColor:'grey'}} 
                      source={{ uri:'http://agro-vision.in/dpal/assets/images/guests/'+item.uentry_img }}/>
                    </View>
                    <View style={{flex:4}}>
                      <Text style={{fontSize:18,color:'#17A589'}}>{item.log_entry_type} • {item.vnumber} {item.comp}</Text>
                      <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                      {item.guard=='' &&
                      <Text style={styles.badge}>Preapproved by {this.checkuname(item.name)}</Text>}
                      {item.guard!='' &&
                      <Text style={styles.badge}>Allowed by Guard {item.guard}</Text>}
                      <Text style={{fontFamily:'custom-fonts',fontSize:12}}>{item.stime}</Text>
                      </View>
                      <View style={{flexDirection:'row',marginTop:2}}>
                      <Text style={{color:'orange',fontSize:16,marginLeft:5}}>{item.entry_name} • {item.entry_phone}</Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                        <Text style={{fontSize:16,color:'#1E8449'}}>
                            <Ionicons name="md-log-in" color='#1E8449'size={18}/> {item.in_time}</Text>
                            {item.out_time=="" &&
                        <Text style={{fontSize:16,color:'#EC7063'}}>Inside</Text>}
                            {item.out_time!="" &&
                             <Text style={{fontSize:16,color:'#EC7063'}}>
                             <Ionicons name="md-log-out" color='#EC7063'size={18}/> {item.out_time}</Text>}
                  </CardItem>
                </Card></View>}/>
                   </Tab>
                    
                <Tab heading="Pre Scheduled" tabStyle={{backgroundColor: '#1e2939'}} activeTabStyle={{backgroundColor: '#25be9f'}}>
                <View  style={{flexDirection:'row',justifyContent:'space-between',padding:5}}> 
             <View visible={false} style={{borderWidth:1,flex:2, borderColor:'grey',borderRadius:10,padding:2}}>
                <Picker
                      selectedValue={this.state.filterByTypes}
                      style={{ width: '100%',height:30 }} 
                      onValueChange={(itemValue) => this.filterByTypePre(itemValue)}>
                        <Picker.Item label="All" value="" />
                        <Picker.Item label="Cab" value="Cab" />
                        <Picker.Item label="Delivery" value="Delivery" />
                        <Picker.Item label="Guest" value="Guest" />
                        <Picker.Item label="Others" value="Others" />
                </Picker>
              </View>
              <View style={{flex:1,marginLeft:5}}>
                <TouchableOpacity onPress={()=>this.pageRefresh()} style={{padding:5,backgroundColor:'grey',borderRadius:10,alignItems:'center'}}>
                  <Ionicons name="md-refresh" color='#fff'size={26}/>
                </TouchableOpacity> 
              </View>
            </View>
                <FlatList data={this.state.entries} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View>
            {this.check_header(item.save_date)==true &&
            <Text style={{alignSelf:'center',fontSize:12,textAlign:'center',width:100,padding:4,margin:5,
            backgroundColor:'grey',color:'#fff',borderRadius:20}}>{this.reverseDate(item.save_date)}</Text>}
            <Card>
              <CardItem style={{flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={{uri: 'http://agro-vision.in/dpal/assets/'+item.entry_img}}/>
                </View>
                <View style={{flex:4}}>
                  {item.entry_type=="Guest" &&
                  <View>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts'}}>{item.gname}</Text>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts',color:'orange'}}> {item.freq_type}</Text>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>{item.gphone}</Text>
                  <Text style={{fontFamily:'custom-fonts'}}>Guest</Text>
                  </View></View>}
                  {item.entry_type!="Guest" &&
                  <View>
                  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts'}}>{item.entry_type} {item.vnumber} </Text>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts',color:'orange'}}> {item.freq_type}</Text>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>Preapproved by {this.checkuname(item.name)}</Text>
                  <Text style={{fontFamily:'custom-fonts'}}>{item.save_time}</Text>
                  </View></View>}
                  
                </View>
              </CardItem>
              <CardItem>
                <Body>
                <Text style={{fontSize:16,color:'#85929E',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-calendar" color='#0E6655'size={16}/> {this.change_date(item.valid_till,item.entry_date,item.entry_type,item.freq_type)}</Text>
                {item.freq_type=="Frequent" && item.entry_type!="Guest" &&
                <Text style={{fontSize:16,color:'#85929E',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-clock" color='#0E6655'size={16}/> {item.start_time} - {item.end_time} • {this.getTotalDays(item.freq_days)}</Text>}
                {item.comp!="" &&
                <Text style={{fontSize:18,color:'#0E6655',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-checkmark" color='#0E6655'size={18}/> {item.comp}</Text>}
                </Body>
              </CardItem>
              {this.change_date(item.valid_till,item.entry_date,item.entry_type,item.freq_type)!='Expired' &&
              <CardItem hide style={{flexDirection:'row',fontFamily:'custom-fonts',justifyContent:'space-between',
              borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  {item.entry_type=="Guest" &&  
                  <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.gphone);}}>
                    <Text style={{fontSize:18,color:'green',fontFamily:'custom-fonts'}}><Ionicons name="md-call" color='green'size={18}/> Call</Text></TouchableOpacity>}
                    {item.entry_type!="Guest" &&  
                    <TouchableOpacity onPress={()=>this.editEntry(item.entid,item.freq_type,item.entry_type)}>
                    <Text style={{fontSize:18,color:'orange',fontFamily:'custom-fonts'}}><Ionicons name="md-create" color='orange'size={18}/> Edit</Text></TouchableOpacity>}
                  <TouchableOpacity onPress={()=>this.delEntry(item.entid)}>
                    <Text style={{fontSize:18,color:'#EC7063',fontFamily:'custom-fonts'}}><Ionicons name="md-close" color='red'size={18}/> Cancel</Text></TouchableOpacity>
              </CardItem>}
            </Card></View>}/>
                </Tab>
                <Tab heading="Staff Log" tabStyle={{backgroundColor: '#1e2939'}} activeTabStyle={{backgroundColor: '#25be9f'}}>
                <View  style={{flexDirection:'row',justifyContent:'space-between',padding:5}}> 
              <DatePicker  customStyles={{dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0}, 
                dateInput: {marginLeft: 10,borderRadius:10,height:40,flex:2}}}   date={this.state.filterBydates} placeholder='Select Date' mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(filterBydates) =>this.fldates(filterBydates)}/>
              <View style={{flex:1,marginLeft:5}}>
                <TouchableOpacity onPress={()=>this.pageRefresh()} style={{padding:5,backgroundColor:'grey',borderRadius:10,alignItems:'center'}}>
                  <Ionicons name="md-refresh" color='#fff'size={26}/>
                </TouchableOpacity> 
              </View>
            </View>
                <FlatList data={this.state.helperlog} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
                  <Card>
                    <CardItem style={{flexDirection:'row'}}>
                      <View style={{flex:1}}>
                        <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} 
                        source={{uri: 'http://agro-vision.in/dpal/assets/images/helpers/'+item.himg}}/>
                      </View>
                      <View style={{flex:4}}>
                        <View>
                        <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts'}}>{item.hname}</Text>
                        {item.out_time=="" &&
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                          <Text style={styles.badge}>Entered</Text>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.in_time}</Text>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.ser_type}</Text>
                        </View>}
                        {item.out_time!="" &&
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                          <Text style={styles.badge}>Left</Text>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.out_time}</Text>
                          <Text style={{fontFamily:'custom-fonts'}}>{item.ser_type}</Text>
                        </View>}
                        </View>
                      </View>
                    </CardItem>
                    <CardItem bordered style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#F2F3F4'}}>
                      <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.hphone);}}>
                        <Text style={{fontSize:18,color:'green',fontFamily:'custom-fonts'}}><Ionicons name="md-call" color='green'size={18}/> Call</Text></TouchableOpacity>
                      <TouchableOpacity onPress={()=>alert("attend")}>
                        <Text style={{fontSize:18,color:'orange',fontFamily:'custom-fonts'}}><Ionicons name="md-calendar" color='orange'size={18}/> Attendance</Text></TouchableOpacity>
                    </CardItem>
                  </Card>}/>  
                </Tab>
          </Tabs>
          </Content>
          </ScrollView>
        </Container>
      );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, marginLeft:5,marginRight:5,
    backgroundColor: '#fff'
  },
  badge:{paddingTop:2,paddingBottom:2,paddingLeft:5,paddingRight:5,borderRadius:5,borderWidth:0.5,borderColor:'#4AAE9A',
  backgroundColor:'#1ABC9C',color:'#fff',fontSize:12},
  btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  btnText2:{fontSize:16,fontFamily:'custom-fonts', textAlign:'center', color:'#000'},
  input2:{borderRadius:20,width:'100%', borderWidth:0.5,fontFamily:'custom-fonts', height:40,padding:10,fontSize:16,backgroundColor:'#fff'},
});
