
import React from 'react';
import {Modal,TouchableOpacity, StatusBar,StyleSheet, Text,Image, View,TextInput,FlatList,AsyncStorage,Dimensions,Button } from 'react-native';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
const WIDTH=Dimensions.get('window').width;
import Spinner from 'react-native-loading-spinner-overlay';

export default class LiveTrades extends React.Component {

    constructor(props){
        super(props);
        this.state={ isLoading: true,isVisible: false,spinner:false,
          dataSource:[],dataSource2:[],wallet:'0',ptarget:'',pstoploss:'',trid:''
        }
      }

      edit_trade(trid){
       // alert(trid);return false;
        const wurl='http://iglobal.in/home/edit_trade_api/'+trid;
            fetch(wurl).then((response)=>response.json())
            .then((responseJson)=>{
               this.setState({ptarget:responseJson.target,pstoploss:responseJson.stoploss,trid:responseJson.bsid})
               this.setState({ isVisible: true})
            }).catch((error)=>{
              console.log(error)
            })
      }

      tradeEditSubmit(id){
        fetch('http://iglobal.in/home/update_trade_api/', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                  target:this.state.ptarget,
                  stoploss: this.state.pstoploss,
                  bsid: this.state.trid,
              }),
            }).then((response)=>response.json())
            .then((responseJson)=>{
              console.log(responseJson);
              alert(responseJson.msg);
            if(responseJson.status==0){
              alert(responseJson.msg);
              //this.props.navigation.navigate("LiveTrades");
              this.setState({ isVisible:!this.state.isVisible});
            }
            });
      }
      
      async componentDidMount(){
        this.setState({spinner: !this.state.spinner});
        var userid = await AsyncStorage.getItem('userid');
        //alert(userid);
        var timer=setInterval(()=>{
            const url='http://iglobal.in/home/my_trades_api/'+userid;
            fetch(url).then((response)=>response.json())
            .then((responseJson)=>{
              
               this.setState({isLoading: false,dataSource:responseJson})
            }).catch((error)=>{
              console.log(error)
            })
        },1000);

        const url='http://iglobal.in/home/exit_trades_api/'+userid;
            fetch(url).then((response)=>response.json())
            .then((responseJson)=>{
              this.setState({spinner: !this.state.spinner});
               this.setState({isLoading: false,dataSource2:responseJson})
            }).catch((error)=>{
              console.log(error)
            })
            const wurl='http://iglobal.in/home/wallet_api/'+userid;
            fetch(wurl).then((response)=>response.json())
            .then((responseJson)=>{
               this.setState({isLoading: false,wallet:responseJson.wallet})
            }).catch((error)=>{
              console.log(error)
            })

        //this.socket=io('http://eglobel.com:4003');
        //this.socket.on("tr_emit",data=>{        });
      }

      squareOff(id){
        //confirm("Press a button!");
        const url='http://iglobal.in/home/square_off_api/'+id;
        fetch(url).then((response)=>response.json())
        .then((responseJson)=>{
          alert(responseJson.msg);
          if(responseJson.status==0){
            //this.props.navigation.navigate("LiveTrades");
            this.componentDidMount();
          }
        }).catch((error)=>{
          console.log(error)
        })
      }
    
      showSqOff(st,bsid){
        if(st!="On"){
          return <Button style={styles.btns} onPress={()=>this.squareOff(bsid)} title="Square Off" color='orange'/>
        }
      }

    render() {
      return (
        <Container>
        <Spinner size='large' visible={this.state.spinner} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
        <Header style={{backgroundColor:'#F39C12',marginTop:-10}}>
            <Text style={{color:'#fff',fontSize:18,marginTop:20}}>Wallet Balance - <Ionicons name="md-wallet" size={18}/> {this.state.wallet}/-</Text>        
        </Header>
          <Content>
          <Modal animationType = {"slide"} transparent = {false}
            visible = {this.state.isVisible}
            onRequestClose = {() =>{ console.log("Modal has been closed.") } }>
            {/*All views of Modal*/}
            {/*Animation can be slide, slide, none*/}
            <View style = {styles.modal}>
              <Text style = {{fontSize:18,color:'orange',textAlign:'center'}}>EDIT TRADE</Text>
              <Text>Target</Text>
              <TextInput onChangeText={(ptarget)=>this.setState({ptarget})} keyboardType = "number-pad" value={this.state.ptarget}  maxLength={10} style={styles.input} placeholder=""/>
              <Text>Stoploss</Text>
              <TextInput onChangeText={(pstoploss)=>this.setState({pstoploss})}  keyboardType = "number-pad"  value={this.state.pstoploss} maxLength={10} style={styles.input} placeholder=""/>
            <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>this.tradeEditSubmit(this.state.trid)}><Text style={styles.btnText}>Update</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn}  onPress = {() => {
                  this.setState({ isVisible:!this.state.isVisible})}}><Text style={styles.btnText}>Cancel</Text></TouchableOpacity>
            </View>
            </View>
        </Modal>
          <View style={styles.container}>
            <Text style={{fontSize:18,color:'#FFA613'}}>LIVE TRADES</Text>
            <FlatList data={this.state.dataSource}    keyExtractor={(item, index) => index.toString()} 
                renderItem={({item})=>
                <View style={item.pro_loss>0?{ flexDirection :'row',flex:1,
                borderWidth:0.5,borderColor:'green',width:WIDTH*0.90,padding:5,marginBottom:5,backgroundColor:'#caffc9',backgroundColor:'#A2FF32'}:{
                  flexDirection :'row',flex:1,
      borderWidth:0.5,borderColor:'red',width:WIDTH*0.90,padding:5,marginBottom:5,backgroundColor:'#caffc9',backgroundColor:'#FF735F'}}>
                <View style={{flex:1}}>
                  <Text style={{fontSize:20}}><Ionicons name="md-cash" size={20}/> {item.pro_loss}</Text>
                  <Text style={styles.father}>{item.type} {item.lotsize} Lot</Text>
                  <Text style={{fontSize:18, marginTop:8,color:'#5838C4',fontWeight:'600'}}>{item.metal}</Text>
                </View>
                
                <View style={{flex:1}}>
                <Text style={styles.father}><Ionicons name="md-analytics" size={20}/> Live {item.last_live}</Text>
                <Text style={styles.father}><Ionicons name="md-analytics" size={20}/> Limit {item.setlimit}</Text>
                {this.showSqOff(item.sq_off,item.bsid)}
                
                </View>

                <View style={{flex:1}}>
					<Text style={styles.father}><Ionicons name="md-arrow-dropup-circle" size={20}/> {item.target}</Text>
					<Text style={styles.father}><Ionicons name="md-arrow-dropdown-circle" size={20}/> {item.stoploss}</Text>      
                	<Button style={styles.btns}  onPress = {() =>this.edit_trade(item.bsid)} title="Edit" color='grey'/>
                </View>

                </View>}/> 
                <Text style={{fontSize:18,color:'#FFA613'}}>EXIT TRADES</Text>
            	<FlatList data={this.state.dataSource2}    keyExtractor={(item, index) => index.toString()} 
					renderItem={({item})=>
					<View style={styles.userlist2}>
					<View style={{flex:1}}>
					<Text style={item.pro_loss>0?{ color:'green',fontSize:20}:{color:'red',fontSize:20}}><Ionicons name="md-cash" size={20}/> {item.pro_loss}</Text>
					<Text style={styles.father}>{item.type} {item.lotsize} Lot</Text>
          <Text style={{fontSize:18, marginTop:8,color:'#5838C4',fontWeight:'600'}}>{item.metal}</Text>
                </View>
                
                <View style={{flex:1}}>
                  <Text style={styles.father}><Ionicons name="md-analytics" size={20}/> Limit {item.setlimit}</Text>
                  <Text style={styles.father}><Ionicons name="md-exit" size={20}/> SqOff {item.last_live}</Text>
                </View>
                
                <View style={{flex:1}}>
                  <Text style={styles.father}><Ionicons name="md-arrow-dropup-circle" size={20}/> {item.target}</Text>
                  <Text style={styles.father}><Ionicons name="md-arrow-dropdown-circle" size={20}/> {item.stoploss}</Text>
                </View>
                
                </View>}/> 
        </View>
    
          </Content>
          </Container>
        );
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff', padding:10,
      //alignItems: 'center',
    },
    userlist:{ flexDirection :'row',flex:1,
      borderWidth:0.5,borderColor:'green',width:WIDTH*0.90,padding:5,marginBottom:5,backgroundColor:'#caffc9'
    },userlist2:{ flexDirection :'row',flex:1,
    borderWidth:0.5,borderColor:'green',width:WIDTH*0.90,padding:5,marginBottom:5,backgroundColor:'#caffc9'
  },title:{fontSize:16},father:{fontSize:14,textAlign:'justify'},
    btns:{width:50},
    modal: {
      flex: 1,
      padding: 50
   },
   input:{borderWidth:0.5, height:40, marginBottom:10,padding:5,fontSize:16},
    btn:{width:'45%', padding:10, backgroundColor:'#1b65d3'},
    btnText:{fontSize:18, textAlign:'center', color:'#fff'}
  });
