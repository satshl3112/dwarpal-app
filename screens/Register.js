import React from 'react';
import { StyleSheet,KeyboardAvoidingView,AsyncStorage, Text,Image,Picker, View, TextInput ,TouchableOpacity } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';

var genders=[
  {label:'Male', value:'Male'},
  {label:'Female', value:'Female'}
]

export default class Register extends React.Component {
    static navigationOptions = {
        header: null
    }

    _pickImage = async () => {
      this.setState({spinner: !this.state.spinner});
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 4],
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
  
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
  
        let formData = new FormData();
        formData.append('photo', { uri: localUri, name: filename, type });
  
        return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
          method: 'POST',
          body: formData,
          header: {
            'content-type': 'multipart/form-data',
          },
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({new_user_image:responseJson.img});
        });
      }else{
        this.setState({spinner: !this.state.spinner});
      }
    };

    constructor(props){
        super(props);
        this.state={ spinner: false,
            name:'',mobile:'',email:'',password:'',cpassword:'',gender:'',age_group:'',expoPushToken:'',new_user_image:'add_family.jpg'
        }
    }

    _pickImage = async () => {
      this.setState({spinner: !this.state.spinner});
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 4],
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
  
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
  
        let formData = new FormData();
        formData.append('photo', { uri: localUri, name: filename, type });
  
        return await fetch('http://agro-vision.in/dpal/Rest/upload_image/', {
          method: 'POST',
          body: formData,
          header: {
            'content-type': 'multipart/form-data',
          },
        }).then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({spinner: !this.state.spinner});
          this.setState({new_user_image:responseJson.img});
        });
      }else{
        this.setState({spinner: !this.state.spinner});
      }
    };

    checkemail(mail) {
          const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
          if (reg.test(this.state.email) === true){
              alert( valid);
          }
          else{
              alert();
          }
    }

    componentDidMount = async () => {
      if (Constants.isDevice) {
        const { status: existingStatus } = await Permissions.getAsync(
          Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
          const { status } = await Permissions.askAsync(
            Permissions.NOTIFICATIONS
          );
          finalStatus = status;
        }
        if (finalStatus !== 'granted') {
          alert('Failed to get push token for push notification!');
          return;
        }
        let token = await Notifications.getExpoPushTokenAsync();
        console.log(token)
        this.setState({expoPushToken: token});
      } else {
        alert('Must use physical device for Push Notifications');
      }
    };

    registerSubmit=()=>{
      const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(this.state.name=="" || this.state.mobile=="" || this.state.age_group==""  || this.state.email==""){
            alert("Please Enter All The Fields.");
            //this.setState({errorMsg:'Please Enter Username & Password'});
          }else if (!this.state.name.match(/^[a-zA-Z_ ]+$/)){
              alert('Please Enter Valid Name.');
              return false;
          }else if (this.state.mobile.length!=10){
            alert('Please Enter Valid 10 Digit Mobile Number.');
            return false;
          }else if (reg.test(this.state.email) === false){
            alert('Please Enter a Valid Email Id.'); return false;
        }else{
            //this.setState({spinner: true}); 
            fetch('http://agro-vision.in/dpal/Rest/register_api/', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.name,gender:this.state.gender,age_group:this.state.age_group,
                    mobile: this.state.mobile,img:this.state.new_user_image,
                    email: this.state.email,expo_token:this.state.expoPushToken
                }),
              }).then((response)=>response.json())
              .then((responseJson)=>{
                console.log(responseJson)
                alert(responseJson.msg)
                if(responseJson.status==0){
                //this.setState({spinner:false})
                //alert(this.state.spinner)
                this.props.navigation.navigate("OtpVerify");
              }
              });
          }
    }


  render() {
    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
            <TouchableOpacity style={{alignSelf:'center',marginBottom:5}} onPress={()=>this._pickImage()}>
              <Image style={{width:100,height:100}} source={{ uri:'http://agro-vision.in/dpal/assets/images/users/'+this.state.new_user_image }}/>
            </TouchableOpacity>
            <Text style={{fontSize:16,marginBottom:10}}>New User Registration</Text>
            <TextInput  style={styles.input} onChangeText={(name)=>this.setState({name})} placeholder="Full Name"/>
            <View style={{flexDirection:'row',width:'80%',padding:5}}>
              <View style={{flex:3}}>
                <RadioForm radio_props={genders} buttonColor={'#25be9f'} selectedButtonColor={'#25be9f'} buttonSize={16}
              initial={0}  onPress={(value)=>{this.setState({gender:value})}}/>
              </View>
              <View style={{flex:4}}>
              <Text style={{fontSize:14,marginBottom:2}}>Select Age Group</Text> 
              <View style={{borderWidth:1, borderColor:'grey',borderRadius:20,padding:4}}>
              <Picker
                    selectedValue={this.state.age_group}
                    style={{ width: '100%',height:22,color:'grey' }}
                    onValueChange={(itemValue) => this.setState({ age_group: itemValue})} >
                      <Picker.Item label="Age Group" value="" />
                      <Picker.Item label="18-40" value="18-35" />
                      <Picker.Item label="35-50" value="35-50" />
                      <Picker.Item label="above 50" value="above 50" />
              </Picker> 
              </View>
              </View>
            </View>
            <TextInput autoCapitalize = 'none' onChangeText={(email)=>this.setState({email})}  style={styles.input} placeholder="Enter Email"/>
            <TextInput keyboardType = "number-pad"
    maxLength={10} onChangeText={(mobile)=>this.setState({mobile})}  style={styles.input} placeholder="Enter Mobile Number"/>
            
            <View style={{flexDirection:'row',width:'80%',justifyContent:'space-between'}}>
                <TouchableOpacity  
                onPress={()=>this.props.navigation.navigate('Login')} style={styles.btn2}><Text style={styles.btnText}>Login</Text></TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>this.registerSubmit()}><Text style={styles.btnText}>Register</Text></TouchableOpacity>
                
            </View>
        </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input:{borderRadius:20,width:'80%', borderWidth:0.5, height:40, marginBottom:10,padding:10,fontSize:16,backgroundColor:'#fff'},
    btn:{borderRadius:20,width:'45%', padding:10, backgroundColor:'#25be9f'},
    btn2:{width:'45%',borderRadius:2, padding:10, backgroundColor:'#1d2839',borderRadius:20},
    btnText:{fontSize:18, textAlign:'center', color:'#fff'}
  });