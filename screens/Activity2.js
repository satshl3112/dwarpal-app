import React from 'react';
import { StyleSheet,ScrollView,Linking,WebView,Text,CheckBox,Image, View,Picker,KeyboardAvoidingView,ActivityIndicator,
  TextInput,FlatList,Button,TouchableOpacity,AsyncStorage,RefreshControl,Alert } from 'react-native';
import { Container,Content, Header, Left, Card, CardItem, Body,Icon, Right, Title,Badge } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import AnimatedLoader from "react-native-animated-loader";
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
import * as Font from 'expo-font';
import MultiSelect from 'react-native-multiple-select';

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = dd + '-' + mm + '-' + yyyy;
var tday=yyyy + '-' + mm + '-' + dd;
var items = [{
  id: 'Sunday',
  name: 'Sunday',
}, {
  id: 'Monday',
  name: 'Monday',
}, {
  id: 'Tuesday',
  name: 'Tuesday',
}, {
  id: 'Wednesday',
  name: 'Wednesday',
}, {
  id: 'Thursday',
  name: 'Thursday',
}, {
  id: 'Friday',
  name: 'Friday',
}, {
  id: 'Saturday',
  name: 'Saturday',
}];
export default class Activity extends React.Component {
    constructor(props){
      super(props);
      this.state={entries:[],spinner:false,assetsLoaded: false,edit_type:'',isVisible3:false,cab1advance:false,cabtype:'',
      filterBydate:'',filterByType:'',dates:[],name:''}
    }

    onSelectedItemsChange = selectedItems => {
      this.setState({ selectedItems });
    };

    async pageRefresh(){
      this.setState({spinner: !this.state.spinner});
      this.setState({filterBydate:''});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
     
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    check_header(d){
      //console.log(d)
      if(this.state.dates.includes(d)==true){
        return true
      }else{
        this.state.dates.push(d);
        return false
      }
      //console.log(this.state.dates)
    }

    getTotalDays(d){
      var dd=d.split(",");
      return dd.length+' Days';
    }

    async filterByDate1(d){
      this.setState({filterBydate:d});
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async componentDidMount(){
      await Font.loadAsync({
        'custom-fonts': require('.././assets/fonts/Muli-VariableFont_wght.ttf')
      });
      this.setState({ assetsLoaded: true });
      this.setState({name:await AsyncStorage.getItem('name')});
      this.pageRefresh();
    }
    _onRefresh(){
      this.setState({refreshing:true})
      this.pageRefresh().then(()=>{
        this.setState({refreshing:false})
      })
    }
    
    checkuname(n){
        if(this.state.name==n){
        return "YOU";
        }else{return n;}
    }
    

    advanceOptions(){
      this.setState({cab1advance:!this.state.cab1advance});
      if(this.state.optionname=="Advance"){
        this.setState({optionname:"Less"});
      }else{
        this.setState({optionname:"Advance"});
      }
    }

    change_date(d1,d2,type){
      //console.log('dates '+d1+' '+d2+' '+today)
      var r='';
      var dd1=d1.split("-").reverse().join("-");
      var dd2=d2.split("-").reverse().join("-");
      if(type=="Frequent"){
        if(dd1==today){r=r+' Valid Till Today ';}else{r=r+=' Valid Till '+dd1;}
        if(dd2==today){r=r+'  Today ';}else{r=r+=dd2;}
      }else{
          if(dd2==today){dd2=' Today ';}
         // if(dd1==today){
          var now = new Date();
          var dt = (now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + dd1;
          var userval = new Date(dt);
         
          if (dd2==' Today ' && now > userval){
            r+= 'Expired';
          }else{r+= ' Valid Till '+dd1+', '+dd2;}
        
        //if(dd1==today){r=r+' Valid Till Today ';}else{r=r+=' Valid Till '+dd1;}
        //if(dd2==today){r=r+' Today ';}else{r=r+=dd2;}
      }
      return r;
    }

    async delEntry(eid){
      var userid = await AsyncStorage.getItem('userid');
      Alert.alert(
        'Confirm Action',
        'Sure you want to Cancel leave request?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', 
            onPress:()=>{
              
      const url='http://agro-vision.in/dpal/Rest/delentry_api/'+eid;
          fetch(url).then((response)=>response.json())
          .then((responseJson)=>{
              console.log(responseJson);
              alert(responseJson.msg);
              if(responseJson.status==0){
                this.pageRefresh();
              }
          }).catch((error)=>{
            console.log(error)
      })
            }
          },
        ],
        {cancelable: false},
      );
    }

    async filterByType(type){
      this.setState({filterByType:type});
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    reverseDate(d){
      var d=d.split("-").reverse().join("-");
      return d;
    }

    editEntry(id,type){
      this.setState({cabtype:type});
      this.setState({isVisible3: !this.state.isVisible3});
      const urledit='http://agro-vision.in/dpal/Rest/editEntry_api/'+id;
      fetch(urledit).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({edit_type:responseJson.entry_type+' '+responseJson.freq_type});
        this.setState({start_date:responseJson.start_time});
        this.setState({start_time:responseJson.start_time});
        this.setState({end_time:responseJson.end_time});
        this.setState({vnumber:responseJson.vnumber});
        this.setState({cabtime:responseJson.allow_dday});
        //this.setState({spinner: !this.state.spinner});
        //this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }

    async fldate(dd){
      this.setState({filterBydate:dd});
      //alert(this.state.filterBydate)
      this.setState({spinner: !this.state.spinner});
      var userid = await AsyncStorage.getItem('userid');
      var flatid = await AsyncStorage.getItem('flat');
      //alert(this.state.filterByType)
      const url2='http://agro-vision.in/dpal/Rest/entries_api/'+flatid+'='+this.state.filterBydate+'='+this.state.filterByType;
      fetch(url2).then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson)
        this.setState({spinner: !this.state.spinner});
        this.setState({entries:responseJson});
      }).catch((error)=>{ console.log(error)} );
    }
    
    render() {
      const {assetsLoaded} = this.state;
      if( assetsLoaded ) {
      return (
        <Container style={{backgroundColor:'#fff',padding:5}}>
          <ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)}/>}>
         
          <AnimatedLoader visible={this.state.spinner} overlayColor="rgba(255,255,255,0.75)" source={require(".././assets/loader.json")}
        animationStyle={{width: 100,height: 100}}speed={1}/>
          <Modal isVisible={this.state.isVisible3} style={{borderRadius:20}}>
            <Ionicons name="md-close" color='#fff' onPress = {() => this.setState({isVisible3: !this.state.isVisible3})} size={24}/>
            {this.state.cabtype=="" &&
            <Card style={{padding:20}}>
                      {this.state.cab1advance==false &&
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Allow My Cab to Enter Today Once in Next</Text>}
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:16,color:'grey',marginBottom:5,marginTop:5}}>Select Date & Time</Text>
                      <View style={{flexDirection:'row'}}>
                      <DatePicker style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_date} mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_date) => {this.setState({start_date: start_date})}}/>
              <DatePicker  style={{borderRadius:20,width:'50%'}} customStyles={{
                dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      </View>
                      
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Valid for Next</Text>
                      </View>}
                      
                      <View style={{borderWidth:1, marginRight:5, borderColor:'grey',borderRadius:20,padding:4}}>
                      <Picker
                      selectedValue={this.state.cabtime}
                      style={{ width: '100%',height:30,color:'grey' }}
                      onValueChange={(itemValue) => this.setState({ cabtime: itemValue})} >
                        <Picker.Item label="Select" value="Select" />
                        <Picker.Item label="1 Hour" value="1 Hour" />
                        <Picker.Item label="2 Hour" value="2 Hour" />
                        <Picker.Item label="4 Hour" value="4 Hour" />
                        <Picker.Item label="8 Hour" value="8 Hour" />
                        <Picker.Item label="12 Hour" value="12 Hour" />
                        <Picker.Item label="24 Hour" value="24 Hour" />
                      </Picker></View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,alignSelf:'center',fontFamily:'custom-fonts'}}>Last 4 Digit of Vehicle No</Text>
                      <TextInput keyboardType = "number-pad" style={{borderWidth:0.5,borderColor:'grey',
                      height:45,borderRadius:20,fontSize:30,textAlign:'center',padding:5,alignSelf:'center',width:'50%'}} placeholder="1234"
    maxLength={4} onChangeText={(vnumber)=>this.setState({vnumber})}/>
                      {this.state.cab1advance==true &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      </View>}
                      <TouchableOpacity onPress={()=>this.advanceOptions()}>
                        <Text  style={{fontSize:14,color:'grey',marginBottom:5,marginTop:10,textAlign:'center',color:'#E74C3C'}}>{this.state.optionname} Options >></Text>
                      </TouchableOpacity>
                  <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabOnce()}>
                  <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                  </Card>
            }{this.state.cabtype!="" &&
            <Card style={{padding:20}}>
                      <MultiSelect items={items} uniqueKey="id" ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange} selectedItems={this.state.selectedItems}
                      selectText="Select Weekdays" onChangeInput={(text)=> console.log(text)}
                      tagRemoveIconColor="#1ABC9C" tagBorderColor="#1ABC9C" tagTextColor="#1ABC9C" selectedItemTextColor="#1ABC9C"
                      selectedItemIconColor="#1ABC9C" itemTextColor="#1ABC9C" displayKey="name"searchInputStyle={{ color: '#1ABC9C' }}
                      submitButtonColor="#1ABC9C" submitButtonText="Done"/>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Validity</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4,marginRight:5}}>
                        <Picker
                        selectedValue={this.state.validity} onValueChange={(itemValue) => this.setState({ validity: itemValue})}
                        style={{ width: '100%',height:30,color:'grey' }} >
                          <Picker.Item label="Select Validity" value="" />
                          <Picker.Item value="1 Week" label="1 Week"/>
                          <Picker.Item value="15 Days" label="15 Days"/>
                          <Picker.Item value="30 Days" label="30 Days"  />
                        </Picker>
                      </View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,marginTop:5,fontFamily:'custom-fonts'}}>Select Allowed TimeSlot</Text>
                      <View style={{flexDirection:'row',alignContent:'space-around',justifyContent:'space-between'}}>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.start_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(start_time) => {this.setState({start_time: start_time})}}/>
                      <DatePicker style={{borderRadius:20}} customStyles={{
                  dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0},
                  dateInput: {marginLeft: 10,borderRadius:20}}}   date={this.state.end_time} mode="time" format="LT"
                confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(end_time) => {this.setState({end_time: end_time})}}/>
                      </View>
                    
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Company</Text>
                      <View style={{borderWidth:1,borderColor:'grey',borderRadius:20,padding:4}}>
                        <Picker
                        selectedValue={this.state.vcomp}
                        style={{ width: '100%',height:30,color:'grey' }}
                        onValueChange={(itemValue) => this.setState({ vcomp: itemValue})} >
                          <Picker.Item label="Select" value="" />
                          <Picker.Item label="Ola" value="Ola" />
                          <Picker.Item label="Uber" value="Uber" />
                          <Picker.Item label="Jugnoo" value="Jugnoo" />
                          <Picker.Item label="Other" value="Other" />
                        </Picker>
                      </View>
                      {this.state.vcomp=="Other" &&
                      <View>
                      <Text style={{fontSize:14,color:'grey',marginBottom:5,fontFamily:'custom-fonts'}}>Other Company</Text>
                      <TextInput style={styles.input2}  onChangeText={(ocomp)=>this.setState({ocomp})}/></View>}
                      <TouchableOpacity style={styles.btn} onPress={()=>this.allowCabFreq()}>
                          <Text style={styles.btnText}>Submit</Text></TouchableOpacity>
                      </Card>
            }
          </Modal>
          
          <Content>
          <View  style={{flexDirection:'row',justifyContent:'space-between',padding:5}}> 
              {/* <Text>Filter By Date</Text> */}
             <View style={{borderWidth:1,flex:2, borderColor:'grey',borderRadius:10,padding:2}}>
                <Picker
                      selectedValue={this.state.filterByType}
                      style={{ width: '100%',height:30 }} 
                      onValueChange={(itemValue) => this.filterByType(itemValue)}>
                        <Picker.Item label="All" value="" />
                        <Picker.Item label="Cab" value="Cab" />
                        <Picker.Item label="Delivery" value="Delivery" />
                        <Picker.Item label="Guest" value="Guest" />
                        <Picker.Item label="Others" value="Others" />
                </Picker>
              </View>
              <DatePicker  customStyles={{dateIcon: {position: 'absolute',left: 0, top: 4,marginLeft: 0}, 
                dateInput: {marginLeft: 10,borderRadius:10,height:40,flex:2}}}   date={this.state.filterBydate} placeholder='Select Date' mode="date" format="DD-MM-YYYY"
              confirmBtnText="Confirm"  cancelBtnText="Cancel" onDateChange={(filterBydate) =>this.fldate(filterBydate)}/>
              <View style={{flex:1,marginLeft:5}}>
                <TouchableOpacity onPress={()=>this.pageRefresh()} style={{padding:5,backgroundColor:'grey',borderRadius:10,alignItems:'center'}}>
                  <Ionicons name="md-refresh" color='#fff'size={26}/>
                </TouchableOpacity> 
              </View>
          </View>
          <FlatList data={this.state.entries} keyExtractor={(item, index) => index.toString()} renderItem={({item})=>
            <View>
            {this.check_header(item.save_date)==true &&
            <Text style={{alignSelf:'center',fontSize:12,textAlign:'center',width:100,padding:4,margin:5,
            backgroundColor:'grey',color:'#fff',borderRadius:20}}>{this.reverseDate(item.save_date)}</Text>}
            <Card>
              <CardItem style={{flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Image style={{width:50, height:50,borderRadius:50,borderWidth:0.5,borderColor:'grey'}} source={{uri: 'http://agro-vision.in/dpal/assets/'+item.entry_img}}/>
                </View>
                <View style={{flex:4}}>
                  {item.entry_type=="Guest" &&
                  <View>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts'}}>{item.gname} {item.freq_type}</Text>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>{item.gphone}</Text>
                  <Text style={{fontFamily:'custom-fonts'}}>Guest</Text>
                  </View></View>}
                  {item.entry_type!="Guest" &&
                  <View>
                  <Text style={{fontSize:18,color:'#17A589',fontFamily:'custom-fonts'}}>{item.entry_type} {item.freq_type}</Text>
                  <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:2}}>
                  <Text style={styles.badge}>PREAPPROVED BY {this.checkuname(item.name)}</Text>
                  <Text style={{fontFamily:'custom-fonts'}}>{item.save_time}</Text>
                  </View></View>}
                  
                </View>
              </CardItem>
              <CardItem>
                <Body>
                
                <Text style={{fontSize:16,color:'#85929E',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-calendar" color='#0E6655'size={16}/> {this.change_date(item.valid_till,item.entry_date,item.entry_type)}</Text>
                {item.freq_type=="Frequent" && item.entry_type!="Guest" &&
                <Text style={{fontSize:16,color:'#85929E',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-clock" color='#0E6655'size={16}/> {item.start_time} - {item.end_time} • {this.getTotalDays(item.freq_days)}</Text>}
                {item.comp!="" &&
                <Text style={{fontSize:18,color:'#0E6655',fontFamily:'custom-fonts'}}> 
                <Ionicons name="md-car" color='#0E6655'size={18}/> {item.comp}</Text>}
                </Body>
              </CardItem>
              {this.change_date(item.valid_till,item.entry_date,item.entry_type)!='Expired' &&
              <CardItem hide style={{flexDirection:'row',fontFamily:'custom-fonts',justifyContent:'space-between',
              borderTopWidth:0.5,borderColor:'#E5E7E9',backgroundColor:'#F8F9F9'}}>
                  {item.entry_type=="Guest" &&  
                  <TouchableOpacity onPress={()=>{Linking.openURL('tel:'+item.gphone);}}>
                    <Text style={{fontSize:18,color:'green',fontFamily:'custom-fonts'}}><Ionicons name="md-call" color='green'size={18}/> Call</Text></TouchableOpacity>}
                  <TouchableOpacity onPress={()=>this.editEntry(item.entid,item.freq_type)}>
                    <Text style={{fontSize:18,color:'orange',fontFamily:'custom-fonts'}}><Ionicons name="md-create" color='orange'size={18}/> Edit</Text></TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.delEntry(item.entid)}>
                    <Text style={{fontSize:18,color:'#EC7063',fontFamily:'custom-fonts'}}><Ionicons name="md-close" color='red'size={18}/> Cancel</Text></TouchableOpacity>
              </CardItem>}
            </Card></View>}/>
          </Content>
          </ScrollView>
        </Container>
      );
      }else {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, margin:5,
    backgroundColor: '#fff'
  },
  badge:{paddingTop:2,paddingBottom:2,paddingLeft:5,paddingRight:5,borderRadius:5,borderWidth:0.5,borderColor:'#4AAE9A',
  backgroundColor:'#1ABC9C',color:'#fff',fontSize:12},
  btn:{width:'100%',marginTop:10, padding:8, backgroundColor:'#25be9f',borderRadius:20},
  btnText:{fontSize:18, textAlign:'center', color:'#fff'},
  btnText2:{fontSize:16,fontFamily:'custom-fonts', textAlign:'center', color:'#000'},
});
