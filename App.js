import React from 'react';
import { StyleSheet,AsyncStorage,Image, Text, View,ActivityIndicator,StatusBar,Button } from 'react-native';
import { createSwitchNavigator,createDrawerNavigator, createAppContainer, DrawerItems } from "react-navigation";
import { Ionicons } from '@expo/vector-icons';
import { Container,Content, Header } from 'native-base';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack'
console.disableYellowBox = true;


import Login from './screens/Login';
import HelperAttend from './screens/HelperAttend';
import Complaints from './screens/Complaints';
import Events from './screens/Events';
import UserContacts from './screens/UserContacts';
import Approvals from './screens/Approvals';
import Register from './screens/Register';
import Dashboard from './screens/Dashboard';
import Withdraw from './screens/Withdraw';
import MyFlats from './screens/MyFlats';
import Community from './screens/Community';
import Activity from './screens/Activity';
import Household from './screens/Household';
import ChangeFlat from './screens/ChangeFlat';
import UserProfile from './screens/UserProfile';
import BankDetails from './screens/BankDetails';
import CompanyProfile from './screens/CompanyProfile';
import OtpVerify from './screens/OtpVerify';
import ForgotPassword from './screens/ForgotPassword';
import VerifyForgotOTP from './screens/VerifyForgotOTP';
import ResetPassword from './screens/ResetPassword';
import ListServices from './screens/ListServices';
import ListWorkers from './screens/ListWorkers';
import WorkProfile from './screens/WorkProfile';
import AddButton from './screens/AddButton';
import AddLocalService from './screens/AddLocalService';
import HelperApprovals from './screens/HelperApprovals';
import AddFlat from './screens/AddFlat';
import UserNotifications from './screens/UserNotifications'
import AskApproval from './screens/AskApproval';
import AddGuard from './screens/AddGuard';
import UNIcon from './screens/UNIcon';
import Residents from './screens/Residents';
import NoticeBoards from './screens/NoticeBoards';

const BAppNav=createBottomTabNavigator({
  Household: {screen: Household,navigationOptions:{  
    tabBarLabel:'',  
    tabBarIcon:({tintColor})=>(  
        <Ionicons name="md-cube" color={tintColor} size={25}/>  
    )  
  }},
  Activity: {screen: Activity,navigationOptions:{  
    tabBarLabel:'Actions',  
    tabBarIcon:({tintColor})=>(  
        <Ionicons name="md-analytics" color={tintColor} size={25}/>  
    )  
  }},
  Add: {
      screen: () => null, // Empty screen
      tabBarOptions: {showLabel: false,},
      navigationOptions: () => ({tabBarLabel:' ', 
          tabBarIcon: <AddButton /> // Plus button component
      })
  },
  Community: {screen: Community,navigationOptions:{  
    tabBarLabel:'Society',
    tabBarIcon:({tintColor})=>(  
        <Ionicons name="md-contacts" color={tintColor} size={25}/>  
    )  
  }},
  Settings: {screen: Dashboard,navigationOptions:{  
    tabBarLabel:'Settings',
    tabBarIcon:({tintColor})=>(  
        <Ionicons name="md-settings" color={tintColor} size={25}/>  
    )  
  }}
  },{
  tabBarOptions: {
    activeTintColor: '#25be9f',
    labelStyle: {
      color:'#fff'
    },
    style: {
      backgroundColor: '#1d2839',
    },
  }
});

class SignInScreen extends React.Component {
  static navigationOptions = {
    title: 'Please sign in',
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="Sign in!" onPress={this._signInAsync} />
      </View>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome to the app!',
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="Show me more of the app" onPress={this._showMoreApp} />
        <Button title="Actually, sign me out :)" onPress={this._signOutAsync} />
      </View>
    );
  }

  _showMoreApp = () => {
    this.props.navigation.navigate('Other');
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}

class OtherScreen extends React.Component {
  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="I'm done, sign me out" onPress={this._signOutAsync} />
        <StatusBar barStyle="default" />
      </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userid');
    if(await AsyncStorage.getItem('flat')!=null){
      this.props.navigation.navigate('Household');
    }else{
    this.props.navigation.navigate(userToken ? 'Dashboard' : 'Login');
    }
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const MainNavigator = createStackNavigator({
  Login:{screen:Login,navigationOptions: {
    header: null,
  }},
  Register:{screen:Register},
  OtpVerify:{screen:OtpVerify,navigationOptions: {
    header: null,
  }},
  Dashboard:{screen:Dashboard},
  Household:BAppNav,ListServices:{screen:ListServices},
  ForgotPassword:{screen:ForgotPassword},
  VerifyForgotOTP:{screen:VerifyForgotOTP},
  ResetPassword:{screen:ResetPassword},
  ListServices:{screen:ListServices},
  ListWorkers:{screen:ListWorkers},
  WorkProfile:{screen:WorkProfile},
  AddLocalService:{screen:AddLocalService},
  Approvals:{screen:Approvals},
  HelperApprovals:{screen:HelperApprovals},
  UserContacts:{screen:UserContacts},
  AddFlat:{screen:AddFlat},
  UserProfile:{screen:UserProfile},
  UserNotifications:{screen:UserNotifications},
  AskApproval:{screen:AskApproval},
  AddGuard:{screen:AddGuard},
  Residents:{screen:Residents},Complaints:{screen:Complaints},
  NoticeBoards:{screen:NoticeBoards},Events:{screen:Events},HelperAttend:{screen:HelperAttend},
},{
  defaultNavigationOptions:({navigation})=>{
    return{ 
    headerLeft:null,//<Ionicons style={{marginLeft:15,color:'#fff'}} onPress={()=>navigation.toggleDrawer()} name="md-menu" size={30}/>,
    headerTintColor:'#fff',
    headerStyle:{backgroundColor:'#1e2939'},
    headerTitle:'Dwarpal App ',
    headerRight:<View style={{flexDirection:'row'}}><View><Ionicons  onPress={()=>navigation.navigate("UserNotifications")} style={{color:'#fff'}}
     name="md-notifications" size={25}/></View><UNIcon/></View>
  }}
})

async function logout(){
	await AsyncStorage.clear();
}

//const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen });
const AuthStack = createSwitchNavigator({ SignIn: Login });

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: MainNavigator,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));
